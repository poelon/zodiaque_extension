//********************************** tableaux résumés ******************************************
function calcSigneChinois(){
    
    //signe chinois (1900=année du rat)
    var x=(anChinois-1900)%12;
    if (x<0) x+=12;
    abc=labelsChine[x]; //traduction signe
    
    //éléments : année finissant en 0-1 métal, 2-3 eau, 4-5 bois, 6-7 feu, 8-9 terre
    var x=anChinois%10; //dernier chiffre de l'année
    var abc;
    for (var i=0;i<=8;i+=2){
        if (x==i || x==i+1) {
           abc+= " - " + labelsChine[12+(i/2)]; //traduction élément
           break;
        }
    }
    return abc;
}

// ******************************* tableaux Uranien **************************

function tableauMP(ref,type){ //ref:planete, type 0=interne, 1,2=externe
    var message="",
        planetes,
        aspects,
        orbes;
    var row,cell,signeDegres,titre,abc;
    var couleur=["blue","green","orange"]; //interne,externes
    var tt=okTransits-(type&&1);
    //pour couleur 2eme transit
    var tt2=tt+okMix;
    if (tt==0) tt2=0;
    var ty2=type+okMix;
    if (type==0) ty2=0;

    //création tableau
    if (tabMP){garbage=divtabMP.removeChild(tabMP);}
    tabMP=document.createElement('table');
    tabMP.setAttribute('class','tabMP');
                    // tabMP.setAttribute('border-style', 'none'); //ne marche pas
    //titre+initialisation tableau
    titre=titreTableaux(okTransits);
    tableauxInit(tabMP,titre,5,divtabMP); 
  
    //1ère ligne d'infos : planètes de la formule,interne ou externe
    [cell,row]=ajout_ligne(tabMP,5);
    cell.setAttribute('class','tabcell');//pas de bordures, ne marche pas au niveau du tableau, seulement des cellules
    message=labelsUranien[4+(type&&1)];//interne ou externe
    if (MP==0) message+=" a";
    else if (MP==1) message+=" a,b";
    else if (MP==2) message+=" a,b,c";
    cell.style.color=couleur[type];
    cell.textContent=message;  
    
     //2ème ligne : planète(s) survolée ou calculée(s) en vert ou bleu
    [cell,row]=ajout_ligne(tabMP,5);
    cell.setAttribute('class','tabcell');
    message=labelsUranien[4+tt];//interne ou externe
    if (MP==0) message+=" b,c";
    else if (MP==1) message+=" c,d";
    else if (MP==2) message+=" d,e";
    cell.style.color=couleur[tt];
    cell.textContent=message;
    
    //3ème ligne d'infos : résumé type x=a+b-c=24.5
    [cell,row]=ajout_ligne(tabMP,5);
    cell.style.color="black";
    cell.setAttribute('class','tabcell');
    message=selectMP.item(MP).label +" : ";
    if (MP==0) message+="a = b/c "
    else if (MP==1) message+="a/b = c/d ";
    else if (MP==2)message+="a + b - c = d/e";
        //ajoute position planete ou calcul paire ou formule
        var count=0;
        var totalFormule=0;
        var pos=[];
        var t; 
            //position planete ou roue  libre
            if (MP==0){
                if (ref%1==0) totalFormule=posPlanete[type][ref];//planete
                else totalFormule=ref;//roue libre
            }
        else{
            //calcul paire ou formule
            for (var x=0;x<listeBlanche.length;x++){
                if (listeBlanche[x]==1){
                    if (count==1 && type && okMix && selectMP.value=="optPaire") t=2;//double transit+paire
                    else t=type;
                    pos[count]=posPlanete[t][x]%roueAngle;
                        //(a+b)/2
                        if (count==1 && MP==1){
                            totalFormule=(pos[0]+pos[1])/2;
                            //évite écart de 180deg. (ex 10 et 350 : MP=0 et non 180)
                            var a=Math.abs(pos[0]-pos[1]);
                            if (a>roueAngle/2) totalFormule-=roueAngle/2;
                            break;
                        }
                        //a+b-c
                        else if (count==2 && MP==2) {
                            totalFormule=pos[0]+pos[1]-pos[2];
                            break;
                        }
                    count+=1;
                }
            }
        }
        totalFormule=r360(totalFormule,roueAngle);
        totalFormule=cDdDm(totalFormule);
        message+=" = "+totalFormule;

    cell.textContent=message;
    
    //en-têtes colonnes
    titre=["a","b","c","d","e",labelsUranien[9]];
    row=document.createElement('tr');
    tabMP.appendChild(row); 
    for (var i=0;i<titre.length;i++){
        cell=document.createElement('td');
        cell.setAttribute('class','tabcell');
        row.appendChild(cell);
        cell.style.color="blue";
        if (MP==0 && i==3) continue;
        else if (MP<2 && i==4) continue;
        cell.textContent=titre[i];
    }
    //liste aspects
    planetes=listAspects[0].planete[ref].split(",");
    orbes=listAspects[0].orbe[ref].split(",");
    if (planetes.length<=1) return;
    
    //lignes tableau
    for (i=1;i<planetes.length;i+=5){
        row=document.createElement('tr');
        tabMP.appendChild(row);           
        //colonnes
        for (var j=0; j<=5; j++){
            cell=document.createElement('td');
            cell.setAttribute('class','tabcell');
            row.appendChild(cell);
            switch(j){
                 case 0:
                    //planete a
                    x=planetes[i];
                    cell.style.color=couleur[type];
                    cell.style.font=urFont(0.8,x)+"vw Zodiac";
                    if (i>1 && i<planetes.length-5 && MP<2) cell.textContent="-";
                    else cell.textContent=planetesFonts[x];
                    break;
                case 1:
                    //planete b
                    if (MP==0) x=planetes[i+3];
                    else x=planetes[i+1];
                    
                    if (MP==0) cell.style.color=couleur[tt];
                    else if (MP==1) cell.style.color=couleur[ty2];
                    else if (MP==2) cell.style.color=couleur[type];
                    if (MP==0 && x==planetes[i+4]) cell.style.color="red";//paire de 2 planetes identiques
                    
                    cell.style.font=urFont(0.8,x)+"vw Zodiac";
                    if (i>1 && i<planetes.length-5 && MP==1) cell.textContent="-";
                    else cell.textContent=planetesFonts[x];
                    break;
                case 2:
                    //planete c
                    if (MP==0) x=planetes[i+4];
                    else if (MP==1) x=planetes[i+3];
                    else x=planetes[i+2];
                    if (MP==0 && x==planetes[i+3] && !okMix) break;//paire de 2 planetes identiques
                    
                    if (MP==0) cell.style.color=couleur[tt2];
                    else if (MP==1) cell.style.color=couleur[tt];
                    else if (MP==2) cell.style.color=couleur[type];
                    if (MP==1 && x==planetes[i+4]) cell.style.color="red";//paire de 2 planetes identiques
                    
                    cell.style.font=urFont(0.8,x)+"vw Zodiac";
                    cell.textContent=planetesFonts[x];
                    break;
                case 3:
                    //planete d
                    if (MP==0) break;
                    if (MP==1) x=planetes[i+4];
                    else x=planetes[i+3];
                    if (MP==1 && x==planetes[i+3] && !okMix) break;//paire de 2 planetes identiques
                    
                    if (MP==1) cell.style.color=couleur[tt2];
                    else cell.style.color=couleur[tt];
                    if (MP==2 && x==planetes[i+4]) cell.style.color="red";//paire de 2 planetes identiques
                    
                    cell.style.font=urFont(0.8,x)+"vw Zodiac"; 
                    cell.textContent=planetesFonts[x];
                    break;
                case 4:
                    //planete e
                    if (MP<2) break;
                    x=planetes[i+4];
                    if (x==planetes[i+3] && !okMix) break;//paire de 2 planetes identiques
                    
                    cell.style.color=couleur[tt2];
                    
                    cell.style.font=urFont(0.8,x)+"vw Zodiac"; 
                    cell.textContent=planetesFonts[x];
                    break;
                case 5:
                    //orbe
                    signeDegres=convPos2DegSigne(orbes[i]);
                    cell.style.font=fontTableau;
                    cell.textContent=signeDegres.degres;
                    break;
             }        
        }
    }
    if (MP==2) triTable();//classe formules
}

//source : https://www.w3schools.com/howto/howto_js_sort_table.asp
function triTable() {
    var rows= tabMP.rows, switching, i,j, x, y, jx,jy,shouldSwitch,ref=[],count=[],ideb;
    
    //récup Nos des 3 planetes de la formule
    var f=[];
    for (j=0;j<=2;j++){
        for (var i=0;i<maxPl;i++){
            if (rows[5].cells[j].textContent==planetesFonts[i]) break;
        }
        f[j]=i;
    }
    //tri croissant
    f=f.sort(function (a,b) {return a-b});
    //pour classement des formules : a+b-c, a+c-b, b+c-a (pas besoin de tester la dernière)
    f=[f[0],f[1],f[2],f[0],f[2],f[1]];
    
    //2 passages
    for (j=0; j<f.length; j+=3){
        ref[j]=count[j]=0;
        ideb=5; //départ en haut du tableau pour 1er passage
        if (count[j-3]) ideb=count[j-3]+1;//départs suivants avec offset du nbre de lignes du précédent passage
        //formule 1ère ligne
        x=[planetesFonts[f[j]],planetesFonts[f[j+1]],planetesFonts[f[j+2]]];
        jx=JSON.stringify(x);//pour comparaison de 2 arrays
    
        /* Make a loop that will continue until
        no switching has been done: */
        switching = true;
        while (switching) {
            switching = false;
            
            //recherche lignes identiques à x
            for (i = ideb; i < (rows.length); i++) {
                
                shouldSwitch = false;
                //formule ligne i
                y=[rows[i].cells[0].textContent,rows[i].cells[1].textContent,rows[i].cells[2].textContent];
                jy=JSON.stringify(y);
                
                if (jx==jy && ref[j]){
                    shouldSwitch = true;
                    break;
                }else if (jx!=jy && !ref[j]) ref[j]=i;
            }
            //déplacement ligne pour regroupement des lignes identiques
            if (shouldSwitch) {
                tabMP.insertBefore(rows[i], rows[ref[j]]);
                count[j]=ref[j];
                ref[j]=0;
                switching = true;
            }
        }
    }
    //supprime répétition formules
    x=[4,count[0],count[3],rows.length-1];
    for (i=0;i<=2;i++){
        for (j=x[i]+2;j<x[i+1];j++){
            rows[j].cells[0].textContent="-";
            rows[j].cells[1].textContent="-";
            rows[j].cells[2].textContent="-";
        }
    }
    
    //ajoute 2 interlignes
    var row,cell;
    x=[count[0]+1,count[3]+2];
    x.forEach(function(item){
        [cell,row]=ajout_ligne(tabMP,5);
        row.style.backgroundColor="white";
        tabMP.insertBefore(row,rows[item]);
    });
}

function tableauUranien(type){ //type 0=interne, 1=externe
 
    tabResume.hidden=true;
    tabDominantes.hidden=true;
    tabChaine.hidden=true;
    //création tableau
    if (tabUranien){garbage=cadre2.removeChild(tabUranien);}
    tabUranien=document.createElement('table'); 
    //titre
    var titre=titreTableaux(type);
    tableauxInit(tabUranien,titre); 
    var row,cell,signeDegres;
    //case pour arc solaire
    row=document.createElement('tr');
    tabUranien.appendChild(row); 
    cell=document.createElement('td');
    cell.setAttribute('colspan', 6);
    row.appendChild(cell);
    //en-têtes
    var bulle="Retrograde";
    var titre=[,"22.5°","90°","360°"];
    row=document.createElement('tr');
    tabUranien.appendChild(row); 
    cell=document.createElement('td');
    row.appendChild(cell);
    for (i=0;i<titre.length;i++){
        cell=document.createElement('td');
        row.appendChild(cell);
        cell.textContent=titre[i];
        if (i==0) infobulle(cell,bulle);
    }
    //lignes
    for (var i=0; i<=maxPl; i++){
        if (i==11 || i==12) continue;//lilith,NS
        if (isNaN(posPlanete[type][i])) continue;
        row=document.createElement('tr');
        tabUranien.appendChild(row);           
        //colonnes
        for (var j=0; j<=5; j++){
            cell=document.createElement('td');
            row.appendChild(cell);
            switch(j){
                case 0:
                    //planetes
                    cell.style.font=1+"vw Zodiac";  
                    cell.textContent=planetesFonts[i];
                    break;
                case 1:
                    //rétrogradation
                     if (retro[type][i]){
                        cell.style.font = fontTableau;
                        cell.textContent="R";
                    }
                    break;
                case 2:
                    //position 22.5 deg.
                    signeDegres=cDdDm(posPlanete[type][i]%22.5);
                    cell.textContent=signeDegres;
                    break;
                case 3:
                    //position 90 deg.
                    signeDegres=cDdDm(posPlanete[type][i]%90);
                    cell.textContent=signeDegres;
                    break;
                case 4:
                    //position 360 deg
                    signeDegres=convPos2DegSigne(posPlanete[type][i]);//pas cdddm sinon pas de signe
                  //  cell.style.font=fontTableau;
                    cell.textContent=signeDegres.degres;
                    break;
                case 5:
                    //signes
                    cell.style.font=1+"vw Zodiac";
                    cell.textContent=signesFonts[signeDegres.signe];
             }        
        }
    }
}
//****************** fin tableaux Uranien ***************

// ************ routines communes ********

//a=0:individuelles, a=1:collectives, b=signe (0 à 11), c=planetes
function calcDominantes(a,b,c){   
    dominantes.binaire[2*a+b%2]+=1; //yang ou chaud,yin ou froid inviduelles (a=0),collectives (a=1)
    dominantes.ternaire[3*a+b%3]+=1; //cardinal,fixe,mutable
    dominantes.quaternaire[4*a+b%4]+=1; //feu,terre,air,eau
    dominantes.qualite[2*a+Math.trunc((b%4)/2)]+=1; //sec, humide (math.trunc=partie entière)
    var x=b%4%3;
    if (x>1){x=1};
    dominantes.primarite[2*a+x]+=1;  //primaire,secondaire
    x=0;
    if (b>=3 && b<=8){x=1};
    dominantes.hemicyclesAB[2*a+x]+=1; //A,B
    x=1;
    if (b>=6){x=0};
    dominantes.hemicyclesCD[2*a+x]+=1; //C,D
}


//message au survol souris
function infobulle(cell,info,titre){
    var lieu = new Image();
    lieu.src = '../images/bulle.png';
    cell.appendChild(lieu);
    lieu.onmouseover=function(e){
        displayDivInfo(info,e.pageX,e.pageY,titre);
    }
    lieu.onmouseout=function(){
        displayDivInfo();
    }
}

function titreTableaux(type){
    var titre="";
    if (okMix){//2 transits
        //theme,transit,arc,rs,progresse,synastrie (traduits)
        var m=["","Transit",labelsUranien[3],labelsCentre[20],labelsDroite[34],labelsDroite[30]];
        var a=selectTransits.selectedOptions;
        titre=m[a.item(0).index] + " / " + m[a.item(1).index];  
    }
    else{
        if (okArc) titre+=labelsUranien[3]+ " ("+StringArcSolaire()+") "+ dateMoyenne(dateMaisons); //arc solaire
        else if (okProgresse) titre+=labelsDroite[34]+" "+ dateMoyenne(dateMaisons);//progressé
        else if (okSynastrie) titre+=labelsDroite[30]+" "+ nomTheme2; //synastrie
        else if (okRS) titre+=labelsCentre[20]+" "+ dateMoyenne(dateRS) + " " + heureRS;//RS
        else if (okTransits) titre+="Transits "+ dateMoyenne(dateMaisons)+" "+choixHeure.value;
        if ((okProgresse || okArc || okRS) && heureNatal !=choixHeure.value){
            var texte=labelsDroite[16]; //heure de naissance;
            texte+=labelsDroite[17]; //" modifiée ";
            titre+=texte;
        }
    }
    if(titre==""){
        if (titreTheme.search(nomNatal)==-1) titre=labelsDroite[0]; //zodiaque
        else titre=nomNatal;
    }
    return titre;    
}

function StringArcSolaire(){
    var signeDegres=cDdDm(arcSolaire);
    return signeDegres;
}

function ajout_ligne(t,c,f,a){ //t:tableau, c:colspan, f:font, a:align (ex 'left')
    row=document.createElement('tr');
    if (a) row.align=a; //'left';
    if (f) row.style.font=f;
    t.appendChild(row);
    cell=document.createElement('td');
    row.appendChild(cell);
    cell.setAttribute('colspan', c);
    return [cell,row];
}

function tableauxInit(x,texte,span,lieu){
    if (!span) span=6;
    if (lieu)lieu.appendChild(x);
    else cadre2.appendChild(x);
   //en-tête
    var row=document.createElement('tr');
    x.appendChild(row);
    var cell=document.createElement('th');
    cell.setAttribute('colspan', span);
    cell.setAttribute('class','tabcell');//pas de bordure
    cell.textContent = texte;
    row.appendChild(cell);
}

function creeTableaux(){//tabResume,tabDominantes,tabChaine
    var row,cell;
    var a=[tabResume,tabDominantes,tabChaine];
    var l=[13,15,11];//lignes
    var c=[5,3,14];//colonnes
    for (var k=0; k<a.length; k++){
        tableauxInit(a[k]);
        a[k].hidden=true;
        a[k].setAttribute('class','cadrevertical');//bordure
        //lignes
        for (var i=0; i<=l[k]; i++){
            row=document.createElement('tr');
            a[k].appendChild(row);           
            //colonnes
            for (var j=0; j<=c[k]; j++){
                cell=document.createElement('td');
                //symboles pour tabresume
                cell.style.font= k==0 && (j==0 || j==5)  ? 1+"vw Zodiac" : fontTableau;
                //pour tabchaine
                if (k==2){
                    cell.style.font= 0.8+"vw Zodiac";
                    cell.setAttribute('class','tabcell');//pas de bordure
                }
                row.appendChild(cell);
            }
        }
    }
   //ajouts pour tabResume
    // sous-tableaux
    var tabCell;
    //maisons gouvernées : 4 cellules dans 4ème colonne
    for (i=1;i<=13;i++){
        tabCell=document.createElement('table');
        tabCell.setAttribute('class','tabcell'); //pas de bordures
        tabResume.rows[i].cells[3].appendChild(tabCell);
        row=document.createElement('tr');
        row.style.font=fontTableau;
        row.style.color="black";
        tabCell.appendChild(row);
            for (j=1;j<=4;j++){
                cell=document.createElement('td');
                cell.setAttribute('class','tabcell');
                row.appendChild(cell); 
            }
    }
    //ligne infobulles (Retrograde,Maisons habitées,Maisons gouvernées)
    var bulle=["Retrograde",labelsCentre[10],labelsCentre[11]];
    for (i=0;i<=2;i++){
        cell= tabResume.rows[14].cells[i+1];
        infobulle(cell,bulle[i]);
    }
    //lignes dignités
    for (j=1;j<=4;j++){
        [cell,row]=ajout_ligne(tabResume,3,fontTableau,"left");
        if (j==1){ //maison interceptée
            cell=document.createElement('td');
            cell.setAttribute('colspan', 3);
            row.appendChild(cell);
        }
    }
    //ligne zodiaque chinois
    row=document.createElement('tr');
    tabResume.appendChild(row);
    var cell=document.createElement('th');
    cell.setAttribute('colspan', 6);
    row.appendChild(cell);
}

function effaceDonnees(){
    for (var i=1;i<=13;i++){
        tabResume.rows[i].cells[4].textContent="";//colonne degrés
          for (var j=0;j<=3;j++){//colonne maisons gouvernées (4 cellules)
              tabResume.rows[i].cells[3].firstChild.rows[0].cells[j].textContent="";
          }
    }
    //lignes du bas :dignités + maison interceptée
    for (i=15;i<=18;i++){
        j=tabResume.rows[i].cells[0];
        j.setAttribute('class','tabcell');
        j.textContent="";
        if (i==15) {
            tabResume.rows[i].cells[1].textContent="";
        }
    }
}

// *************** fin routines communes*************

// ******************* tableaux Tropical *****************


function tableauResume(type){//type 0=interne, 1=externe
    if (!type) type=0;
    //remise à 0 dominantes + chaine
    dominantes.binaire=[0,0,0,0];//laisser les 0 sinon calcDominantes donne des NaN à cause de +=
    dominantes.ternaire=[0,0,0,0,0,0];
    dominantes.quaternaire=[0,0,0,0,0,0,0,0];
    dominantes.qualite=[0,0,0,0];
    dominantes.primarite=[0,0,0,0];
    dominantes.hemicyclesAB=[0,0,0,0];
    dominantes.hemicyclesCD=[0,0,0,0];
    signes_indiv=[0,0,0,0,0,0,0,0,0,0,0,0]; //signes individuels du thème
    chaine.racine=[];
    chaine.signe=[];
    chaine.dispositeur=[];
    chaine.dispose=[];
    var cell,cell2,cell3;
    
    effaceDonnees();
    cell=tabResume.rows[0].cells[0];
    var abc=titreTheme.split(labelsDroite[9])[0]; //supprime affichage lieu de naissance et suite
    cell.textContent=abc.split("(utc")[0]; //supprime utc et suite si pas de lieu de naissance

    var digne=[0,0,0,0,0]; //affiche (1) ou pas (0) les dignités ou la maison interceptée (digne[4]) 
    
    //lignes
    for (var i=0; i<=12; i++){
        //colonnes
        for (var j=0; j<=5; j++){
            cell=tabResume.rows[i+1].cells[j];
            switch(j){
                case 0:
                    cell.style.color="";
                    cell.textContent=planetesFonts[i];
                    //dignités
                    if (i<=9){
                       var abc=convPos2DegSigne(posPlanete[type][i]);
                       var l=["maitrise","exil","chute","exaltation"];
                       var c=["green","red","orange","blue"];
                       for (var x=0;x<l.length;x++){;
                           var max=dignites[l[x]][i].length;
                           for (var k=0;k<max;k++){
                               if (abc.signe==dignites[l[x]][i][k]){
                                   //double dignité
                                   if (cell.style.color>""){
                                        //ajout sous-tableau (1 ligne, 1 cellule)
                                        var tabCell = document.createElement('table');
                                        cell.appendChild(tabCell);
                                        tabCell.setAttribute('class','tabcell'); //pas de bordures
                                        var row2 = document.createElement('tr');
                                        tabCell.appendChild(row2);
                                        var cell2=document.createElement('td');
                                        row2.appendChild(cell2); 
                                        cell2.setAttribute('class','tabcell');
                                        cell2.style.font=1+"vw Zodiac"; 
                                        cell2.textContent=planetesFonts[i];
                                        cell2.style.color=c[x];
                                        digne[x]=1;
                                   }
                                   //simple dignité
                                   else {
                                       cell.style.color=c[x];
                                       digne[x]=1;
                                   }
                                  break;
                               }
                           }
                      } 
                    }
                    break;
                case 1:
                    cell.textContent=retro[type][i] ? "R" : "";
                    break;
                case 2:
                    //maisons habitées (100=non défini)
                    cell.textContent=planeteHabite[i]<100 && type==0 ? AtoR(planeteHabite[i]+1) : "";
                    break;
                case 3:
                    //maisons gouvernées
                    if (i<=9 && type==0){
                        for (var k=0;k<planeteGouverne[i].length;k++){ 
                            var cell2=tabResume.rows[i+1].cells[3].firstChild.rows[0].cells[k];
                            cell2.style.color="";
                            var car=planeteGouverne[i][k];
                                //maison interceptée ?
                                if (car>=100){
                                    car-=100;
                                    cell2.style.color="fuchsia";
                                    digne[4]=1;
                                }
                            cell2.textContent=String(car+1);
                        }
                    }
                    break;
                case 4:
                     //degrés
                    if (posPlanete[type][i]){ //NN Lilith en-dehors plage 1800-2039
                        var signeDegres=convPos2DegSigne(posPlanete[type][i]);
                        cell.textContent=signeDegres.degres;
                    }
                    break;
                case 5:
                     //signes + chaine + dominantes
                    if (posPlanete[type][i]){ //NN Lilith en-dehors plage 1800-2039
                        cell.textContent=signesFonts[signeDegres.signe];
                        //chaine planetaire : signe et dispositeur
                        chaine.signe[i]=signeDegres.signe;
                        chaine.dispositeur[i]=pMaitre[signeDegres.signe][0];
                        if (i<=6){
                            calcDominantes(0,signeDegres.signe,i);//individuelles
                            signes_indiv[signeDegres.signe]+=1; //signe du thème, à mettre en rouge dans infobulle
                        }else if (i>=7 && i<=9){
                            calcDominantes(1,signeDegres.signe,i);//collectives
                        }
                    }
             }        
        }
    }
    //pour chaine planetaire : recherche des planetes "disposées" (uniquement pour les planetes individuelles)
    for (i=0;i<=6;i++){
        car=0;
        chaine.dispose[i]=[];
        for (j=0;j<=12;j++){
            for (var k=0;k<=dignites.maitrise[i].length-1;k++){
                if (chaine.signe[j]==dignites.maitrise[i][k]){
                    chaine.dispose[i][car]=j;
                    car+=1;
                }
            }
        }
    }
    //ajout ASC,MC aux dominantes personnelles et FC, DS aux collectives
    var a=[0,9,3,6],
        b=[0,0,1,1],
        abc;
    for (i=0;i<=3;i++){
        abc= checkDom.checked ? convPos2DegSigne(posMaisonSave[a[i]]) : convPos2DegSigne(posMaison[a[i]]);
        calcDominantes(b[i],abc.signe);
        if (i<2){signes_indiv[abc.signe]+=1}; //signe du thème, à mettre en rouge dans infobulle
    }
    
    //légende bas du tableau
    var l=browser.i18n.getMessage("dignites").split(",");
    var c=["green","fuchsia","red","orange","blue"];
        //maitrise
        cell= tabResume.rows[15].cells[0];
        if (digne[0]==1){
            cell.style.color=c[0];
            cell.textContent=l[0];
        }
        //maison interceptée
        if (digne[4]==1){
            cell=tabResume.rows[15].cells[1];
            cell.setAttribute('class','tabcell');
            cell.style.color=c[1];
            cell.textContent=l[1];
        }
        //exil,chute,exaltation
        for (i=1;i<=3;i+=1){
            if (digne[i]==1){
                cell=tabResume.rows[15+i].cells[0];
                cell.style.color=c[i+1];
                cell.textContent=l[i+1];
            }
        }
        
    //signe chinois
    abc=labelsChine[17];
    abc+= calcSigneChinois();
    cell=tabResume.rows[19].cells[0];
    cell.textContent=abc;
}

//max en gras (2ème et 3ème cellules du tableau des dominantes)
function max_en_gras(tab,col,array){
    var max=Math.max(...array),
        pos=[];
    pos[0]=array.indexOf(max); 
    tab.rows[col[pos[0]]].cells[1].style.fontWeight="bold";
    tab.rows[col[pos[0]]].cells[2].style.fontWeight="bold";
    //cherche si plusieurs max
    var ind=1;
    if (pos<array.length-1){
        for (var i=pos[0]+1;i<array.length;i++){
            if (array[i]==max){
                pos[ind]=i;
                ind+=1;
                tab.rows[col[i]].cells[1].style.fontWeight="bold";
                tab.rows[col[i]].cells[2].style.fontWeight="bold"; 
            }
        }
    }
    return pos;
}
//tableau des dominantes élémentaires
function tableauDominantes(type){//type 0=interne, 1=externe
    if (!type) type=0;
    tableauResume(type);
    tableauChaine();
    
    var row,cell,bulle;
    cell=tabDominantes.rows[0].cells[0];
    cell.textContent=labelsDominantes[23] +" - type ";
    //lignes
    for (var i=0; i<=14; i++){
        bulle="";
        //colonnes
        for (var j=0; j<=3; j++){
            cell=tabDominantes.rows[i+1].cells[j];
            cell.style.font=1+'vw serif';
            cell.textContent="";
                    //infobulles
                    if (j==0){
                        for (var k=0;k<bulles[i].length;k++){
                            bulle+="_"+Signes[bulles[i][k]];
                            if (signes_indiv[bulles[i][k]]>=1){bulle+=" ("+signes_indiv[bulles[i][k]]+")"};
                        }
                        infobulle(cell,bulle,labelsDominantes[i]);
                    };
                    //nom des dominantes
                    if (j==1){
                        cell.textContent=labelsDominantes[i];
                    };
                    if (i>=2 && i<=4 || i>=9 && i<=10 || i>=13){
                        cell.style.backgroundColor="rgb(235,235,235)"; //gris
                    };
        }
    } 
    
//écriture dominantes personnelles et collectives
    var max,
        pos,
        sec_humide,
        qualite,
        couple;
        
    //yang/yin = chaud/froid
    var a=[1,2,1,2]; //lignes
    var b=[2,2,3,3]; //colonnes
    var c=[0,0,0,0]; //pour recherche max
    for (i=0;i<=3;i++){
        tabDominantes.rows[a[i]].cells[b[i]].textContent=dominantes.binaire[i];
        if (i<=1){c[i]=dominantes.binaire[i]};
    };
        var pos=max_en_gras(tabDominantes,a,c);
        var chaud_froid=pos[0]; //chaud=0, froid=1
    //cardinal,fixe,mutable    
    var x=[0,0];
    a=[3,4,5,3,4,5];
    b=[2,2,2,3,3,3];
    c=[];
    for (i=0;i<=5;i++){
        tabDominantes.rows[a[i]].cells[b[i]].textContent=dominantes.ternaire[i];
        if (i<=2){c[i]=dominantes.ternaire[i]};
    };
        x[0]=max_en_gras(tabDominantes,a,c);
    //feu,terre,air,eau    
    a=[6,7,8,9,6,7,8,9];
    b=[2,2,2,2,3,3,3,3];
    c=[];
    for (i=0;i<=7;i++){
        tabDominantes.rows[a[i]].cells[b[i]].textContent=dominantes.quaternaire[i];
        if (i<=3){c[i]=dominantes.quaternaire[i]};
    };
        x[1]=max_en_gras(tabDominantes,a,c);
        //si plusieurs éléments max (feu,terre,air,eau), détermination avec chaud_froid(=yang/yin) et sec_humide (=qualite)
        if (x[1].length>1) {
            for (i=0;i<=1;i++){
                c[i]=dominantes.qualite[i];
            };
            max=Math.max(...c);
            pos=c.indexOf(max);
            sec_humide=pos; //sec=0, humide=1
            qualite=[chaud_froid,sec_humide];
            //couple=[chaud_froid,air_humide] 0-0=feu, 1-0=terre, 0-1=air, 1-1=eau
            couple=[[0,0],[1,0],[0,1],[1,1]]; 
            for (i=0;i<=3;i++){
                if (couple[i][0]==chaud_froid && couple[i][1]==sec_humide){
                    x[1]=i;
                    qualite=[chaud_froid,sec_humide];
                }
            }
        }else{
            x[1]=x[1][0];
            qualite=[];
        }
    //primaire,secondaire    
    a=[10,11,10,11];
    b=[2,2,3,3];
    c=[];
    for (i=0;i<=3;i++){
        tabDominantes.rows[a[i]].cells[b[i]].textContent=dominantes.primarite[i];
        if (i<=1){c[i]=dominantes.primarite[i]};
            
    };
        pos=max_en_gras(tabDominantes,a,c);
    //hémicyles A,B    
    a=[12,13,12,13];
    b=[2,2,3,3];
    c=[];
    for (i=0;i<=3;i++){
        tabDominantes.rows[a[i]].cells[b[i]].textContent=dominantes.hemicyclesAB[i];
        if (i<=1){c[i]=dominantes.hemicyclesAB[i]};
    };
        pos=max_en_gras(tabDominantes,a,c);
    //hémicyles C,D    
    a=[14,15,14,15];
    b=[2,2,3,3];
    c=[];
    for (i=0;i<=3;i++){
        tabDominantes.rows[a[i]].cells[b[i]].textContent=dominantes.hemicyclesCD[i];
        if (i<=1){c[i]=dominantes.hemicyclesCD[i]};    
    };
        pos=max_en_gras(tabDominantes,a,c);
        
//recherche et affichage du signe "type" dans l'en-tête du tableau
    //couple=[ternaire,quaternaire] de Bélier à Poissons, ex [0,0]=[cardinal,feu]=Bélier ou [1,3]=[fixe,eau]=Scorpion
    couple=[[0,0],[1,1],[2,2],[0,3],[1,0],[2,1],[0,2],[1,3],[2,0],[0,1],[1,2],[2,3]]; 
    bulle="";
    for (j=0;j<x[0].length;j++){
        for (i=0;i<=11;i++){
            if (couple[i][0]==x[0][j] && couple[i][1]==x[1]){
                cell=tabDominantes.rows[0].cells[0];
                cell.textContent+="\n" + Signes[i] + "  "; //\n=LF ne marche pas dans une cellule de tableau
                //info bulle
                bulle+= labelsDominantes[2+x[0][j]]+" & "+labelsDominantes[5+x[1]]+ ", " //"\n";
                if (qualite.length>0 && j==x[0].length-1){
                    bulle+=" ["+labelsDominantes[15+qualite[0]]+"/"+labelsDominantes[17+qualite[1]]+"]";
                };
                break;
            };
        };
    };
        bulle=terminaison(bulle,", "); //supprime la virgule de fin
        infobulle(cell,bulle);
        
//ajout 1 ligne pour infobulles
    bulle=[labelsCentre[13],labelsCentre[14]];
    for (i=0;i<=3;i++){
        cell=tabDominantes.rows[16].cells[i];
        cell.textContent="";
        if (i>=2){infobulle(cell,bulle[i-2])};
    }
    
//sélection du tableau à afficher
    a=[tabResume,tabChaine,tabDominantes,divDynamique];//ordre important, doit correspondre à zodiaque.html
    for (i=0;i<=3;i++){
        a[i].hidden=!choixTypeDonnees.item(i).checked;
    }
}


//tableau de la chaîne planétaire
function tableauChaine(){
    var x=[],y,car,cellule,racines=[];
    //recherche des racines
    for (var i=0; i<=6; i++){
        if (chaine.racine[i]>=0) continue;
        x=[],y=i,car=0;
        
        do {
            x[car]=y;
            y=chaine.dispositeur[y];
            x[car+1]=y;
            car+=2;
            if (car>14) break;
        }  while (x[0]!=y);
        
        //trouvé racine
        if (car<=14){
            //élimination des doublons dans chaque racine
            y=[];
            for (var j=0;j<x.length;j++){
                car=0;
                for (var k=0;k<y.length;k++){
                    if (x[j]==y[k]) {car=1;break};
                }
                if (car==0) y[y.length]=x[j];
            }
            //classement (et sauvegarde) racine par ordre numérique croissant
            y.sort();
            for (j=0;j<y.length;j++){
                chaine.racine[y[j]]=y;
            }
        }
    }

    var row,cell,rac1,rac2,rac3;
    
    //traitement racines
    for (i=0; i<=6; i++){
        x=[],y=[];
        if (chaine.racine[i] && chaine.dispose[i]){
            for (k=0;k<chaine.dispose[i].length;k++){
                car=0;
                for (var l=0;l<chaine.racine[i].length;l++){
                    if (chaine.dispose[i][k]==chaine.racine[i][l]) {car=-1; break};
                }
                if (car==0) y.push(chaine.dispose[i][k]);
            }
            //sauvegarde de disposées-racine dans disposées
            chaine.dispose[i]=y;
        }
    }
    
    //suppression doublons racines, copie dans array "racines"
    for (i=0;i<chaine.racine.length;i++){
        if (typeof(chaine.racine[i])=="undefined") continue;
        car=0;
        for (j=0;j<racines.length;j++){
            if (chaine.racine[i]==racines[j]) {car=-1; break};
        }
        if (car==0) racines[j]=chaine.racine[i];
    }
    //détermination nombre et type de racines
    rac1=rac2=rac3=0;
    for (i=0; i<racines.length; i++){
        if (racines[i].length==1) rac1+=1;
        else if (racines[i].length==2) rac2+=1;
        else if (racines[i].length>=3) rac3+=1;
    }
    //effacement tableau
    for (i=0; i<=11; i++){//lignes
        for (j=0;j<=14;j++){//colonnes
            cell=tabChaine.rows[i].cells[j];
            if (cell){
                cell.textContent=cell.style.color="";
            }
        }
    }
    //remplissage tableau à partir du bas (racines)
    var rowMax=10,rowMin,ligne,colonne=1,retour;
    var a0,a1,a2,a3,a4,a5,a6,a7;
    var c=["green","red","blue","maroon","violet","orange","indigo"];
     //toutes les racines
    for (i=0;i<racines.length;i++){
        
        //une racine (simple, double ou boucle)
        for (j=0;j<racines[i].length;j++){
            //racine
            a0=racines[i][j];
            //écriture racine
            ligne=rowMax;
            cellule=tabChaine.rows[rowMax].cells[colonne];
            cellule.textContent=planetesFonts[a0] //a0;
            cellule.style.color=c[i];
            cellule=tabChaine.rows[ligne].cells[0];
            cellule.textContent="1";
            
            //niveaux 1-7 (avance de gauche à droite, colonne par colonne)
             for (var e1=0;e1<chaine.dispose[a0].length;e1++){
                [a1,colonne,retour]=niveauxChaine(a0,e1,rowMax,colonne,c[i],1)
                if (retour==1) continue;
                
                for (var e2=0;e2<chaine.dispose[a1].length;e2++){
                    [a2,colonne,retour]=niveauxChaine(a1,e2,rowMax,colonne,c[i],2)
                    if (retour==1) continue;
                     
                    for (var e3=0;e3<chaine.dispose[a2].length;e3++){
                        [a3,colonne,retour]=niveauxChaine(a2,e3,rowMax,colonne,c[i],3)
                        if (retour==1) continue;
                        
                        for (var e4=0;e4<chaine.dispose[a3].length;e4++){
                            [a4,colonne,retour]=niveauxChaine(a3,e4,rowMax,colonne,c[i],4)
                            if (retour==1) continue;
                            
                            for (var e5=0;e5<chaine.dispose[a4].length;e5++){
                                [a5,colonne,retour]=niveauxChaine(a4,e5,rowMax,colonne,c[i],5)
                                if (retour==1) continue;
                                
                                for (var e6=0;e6<chaine.dispose[a5].length;e6++){
                                    [a6,colonne,retour]=niveauxChaine(a5,e6,rowMax,colonne,c[i],6)
                                    if (retour==1) continue;
                                    
                                    for (var e7=0;e7<chaine.dispose[a6].length;e7++){
                                        [a7,colonne,retour]=niveauxChaine(a6,e7,rowMax,colonne,c[i],7)
                                        if (retour==1) continue;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        colonne+=1;
        }
    }
    
    //centrage titre tableau + ajout nombre de racines
    cellule=tabChaine.rows[0].cells[0];
    cellule.setAttribute('colSpan',colonne);
    l=browser.i18n.getMessage("dignites").split(",");//traductions
    //permet de faure un retour à la ligne dans la cellule d'un tableau ! (ligne suivante + "\r\n")
    cellule.setAttribute('style', 'white-space: pre;');
    cellule.textContent=labelsDominantes[27]+"\r\n"+"\r\n"+labelsDominantes[29] +" 1 ";//chaine planetaire + niveau 1
   
    if (rac1) cellule.textContent+="\r\n"+l[0]+" : "+ rac1;
    if (rac2) cellule.textContent+="\r\n"+l[5]+ " : "+ rac2;
    if (rac3) cellule.textContent+="\r\n"+l[6]+" : "+rac3;
    
    //*** ajout de "-" entre les planètes liées***
    //lignes
    colonne=1;
    for (i=rowMax;i>0;i--){
        //sortie si cellule de gauche est vide
        cellule=tabChaine.rows[i].cells[0];
        if (cellule.textContent=="") break;
        //contenu+couleur cellule
        cellule=tabChaine.rows[i].cells[colonne];
        x=cellule.style.color;
        
        //colonnes
        for (j=colonne+1;j<=12;j++){
            cellule=tabChaine.rows[i].cells[j];
            if (cellule.textContent=="") continue;
            y=cellule.style.color;
            //pour comparaison avec ligne suivante
            cellule=tabChaine.rows[i+1].cells[j];
            //même couleur et cellule en-dessous est vide ou contient "-"
            if (x==y && (cellule.textContent=="" || cellule.textContent=="-")){
                for (k=colonne+1;k<j;k++){
                    cellule=tabChaine.rows[i].cells[k];
                    cellule.textContent="-"; 
                }
                j=k;
                cellule=tabChaine.rows[i].cells[j];
                x=cellule.style.color;
            }else x=y;
            colonne=j;
        }
        colonne=1;
    }
}

function niveauxChaine(a0,e1,rowMax,colonne,c,n){
    var retour=0;
    var a1=chaine.dispose[a0][e1];
    var ligne=rowMax-n;
    var cellule=tabChaine.rows[ligne].cells[colonne];
    cellule.textContent=planetesFonts[a1];
    cellule.style.color=c;
    cellule=tabChaine.rows[ligne].cells[0];
    cellule.textContent=n+1;
    if (a1>6) {colonne+=1;retour=1} //erreur si ";" en fin de ligne !
    else if (chaine.dispose[a1].length==0) {colonne+=1;retour=1};
    return [a1,colonne,retour];
}
// ********************** fin tableaux **********************

//******* transits dynamiques + audio ************

//crée curseurs volume et vitesse
function creeRange(abc,zone){
    var a= document.createElement('input');
    a.setAttribute('type', 'range');
    a.setAttribute('id', abc);
    var b = document.createElement('label');
    b.setAttribute('for',abc)
    b.textContent=abc;
    
        zone.append(b,a);
    return [a,b];
}
//crée checkbox dièses et random
function creeCheck(abc,zone,x){
    var a = document.createElement('input');
    a.setAttribute('type', 'checkbox');
    a.setAttribute('id', abc);
    b = document.createElement('label');
    b.setAttribute('for',abc);//permet de cliquer sur le label
    b.textContent="dièses";
    if (x) zone.append(b,a);
    else zone.append(a,b);
    return [a,b];
}

function modeDynamique(){
    if (okDynamique==1) return;
   
    var a,b,c,d,e,f,i,j,x,jouer;
    efface(divDynamique);
    divDynamique.hidden=false;
    var [accords,octaves]=creeAccords();
    
    //en-tête
    a = document.createElement('p');
    a.setAttribute('id', 'dynamique');
    a.textContent=labelsDominantes[30]; //"transits dynamiques"
    a.style.fontStyle="italic";
        divDynamique.append(a);
    
    //date de début
    b= document.createElement('input');
    b.setAttribute('type', 'date');
    b.setAttribute('id', 'dateDebut');
    a = document.createElement('label');
    a.setAttribute('for','dateDebut')
    a.textContent=labelsCentre[21]+" ";//début
    c = document.createElement('br');
        divDynamique.append(a,b,c);
    dateDebut.value=dateMaintenant;
    
    //bouton "joue/pause"
    a = document.createElement('br');
    var dynBtn = document.createElement('input');
    dynBtn.setAttribute('type', 'image');
    dynBtn.setAttribute('alt', 'joue-play/pause');
    dynBtn.setAttribute('class', 'play');
    c = document.createElement('br');
    d = document.createElement('br');
        divDynamique.append(a,dynBtn,c,d);
    survol(dynBtn,labelsCentre[25]);

    //icones bouton
    var iconPlay="../images/play.png";//équivalent : dynBtn.textContent==String.fromCharCode(9654)
    var iconPause="../images/pause.png";//équivalent : dynBtn.textContent=String.fromCharCode(9689)
    dynBtn.src=iconPlay;
    
    //vitesse
    var vitesse;
    var [vitesse0,label0]=creeRange(labelsCentre[26],divDynamique);//vitesse sans son
    var [vitesse1,label1]=creeRange("tempo",divDynamique);//vitesse avec son
    vitesse0.min=15;
    vitesse0.max=55;
    vitesse0.value="35";
    vitesse1.min=6;
    vitesse1.max=14;
    vitesse1.value="10";
    vitesse0.step=vitesse1.step="any";
    vitesse1.hidden=true;
    label1.hidden=true;
    
    //checkaudio
    a = document.createElement('br');
    b = document.createElement('br');
        divDynamique.append(a,b);
    var [checkAudio,label]=creeCheck('checkAudio',divDynamique);
    label.textContent="audio";
    c = new Image();
    c.src="../images/bulle.png";
        divDynamique.append(" ",c);
    survol(c,labelsCentre[30]);//"peut être gourmand en ressources système !"
    a = document.createElement('br');
    b = document.createElement('br');
        divDynamique.append(a,b);
    checkAudio.checked=false;
    
    //*** partie audio  ***
    a = document.createElement('div');
    a.setAttribute('id','divAudio');
        divDynamique.append(a);

    //volume
    var [volume,label2]=creeRange("volume",divAudio);
    volume.min=0;
    volume.max=10;
    volume.value=2;
    volume.step="any";
    
    //choix instruments
    a = document.createElement('br');
        divAudio.append(a);
    x=["piano",labelsCentre[22],labelsCentre[23],"mix"];//orgue,guitare
    for (i=0;i<x.length;i++){
        a = document.createElement('br');
        b = document.createElement('input');
        b.setAttribute('type', 'radio');
        b.setAttribute('name', 'radioDyn');
        b.setAttribute('id', 'radioDyn'+i);
        c = document.createElement('label');
        c.setAttribute('for','radioDyn'+i);//permet de cliquer sur le label
        c.textContent=x[i];
            divAudio.append(a,b,c);
    }
    var radioDyn=document.getElementsByName("radioDyn");
    radioDyn.item(0).checked=true;//piano
    
    a=document.createElement('br');
    b=document.createElement('br');
        divAudio.append(a,b);
        
    //check accords
    c = new Image();
    c.src="../images/bulle.png";
        divAudio.append(c);
    survol(c,labelsCentre[32]);//"quelques accords musicaux"
    var [checkAccords,label3]=creeCheck('checkAccords',divAudio,1); 
    label3.textContent=labelsCentre[31];//dièses
    
    //check dieses
    var [checkDieses,label3]=creeCheck('checkDieses',divAudio);
    label3.textContent=labelsCentre[27];//dièses
    c = new Image();
    c.src="../images/bulle.png";
        divAudio.append(c);
    survol(c,labelsCentre[29]);//"si transits retrogrades"
    
     //tableau correspondance planètes/notes
    a=document.createElement('br');
    b=document.createElement('br');
    var table,row,cell;
    table=document.createElement('table');
    tableauxInit(table,labelsCentre[24],7); 
    c=[0.8+"vw Zodiac",planetesFonts,1+"vw serif",planetesNotes[0]];//'14px serif'ne marche pas avec android
    for (j=0;j<=3;j+=2){
        row=document.createElement('tr');
        table.appendChild(row); 
        for (i=0;i<=6;i++){
            cell=document.createElement('td');
            cell.setAttribute('class','tabcell');//pas de bordure
            cell.style.font=c[j];
            row.appendChild(cell);
            if (j==0) cell.textContent=c[j+1][i];//planètes
            else cell.textContent=labelsDominantes[32+i];//notes
        } 
    }
        divAudio.append(a,b,table);
    
    //départ
    dynBtn.addEventListener('click',() => {
            //décoche domitude sinon blocage programme (car pas d'aspects ?)
            checkDom.checked=false;
            //validité date ?
            if (dateDebut.valueAsDate==null) return;
            //ligne ajoutée sinon départ à date de naissance au lieu de date du jour, à cause du Promise.resolve plus bas !
            choixDate.value=dateDebut.value;
            //joue
            jouer=1;
            if (dynBtn.src.search("play")>0){
                
                dynBtn.src=iconPause;
                okDynamique=1;
                devalideTout(true);
                //sauf...
                a=[dynBtn,volume,vitesse0,vitesse1,checkAudio,checkDieses,checkAccords];
                for (i of a){i.disabled=false};
                [0,1,2,3].forEach((item)=>{radioDyn[item].disabled=false});
                //sélection transits
                a=selectTransits.selectedOptions.length;
                for (i=0;i<a;i++){
                    selectTransits.selectedOptions.item(0).selected=false;//0 : la position change après un effacement
                }
                selectTransits.item(1).selected=true;
                razAir();
                Promise.resolve().then(() => {//évite sur chrome message : [Violation] Forced reflow while executing JavaScript took 40ms
                    mix(dateDebut.value); //1er jour de transit (theme.js), charge "air"
                });
            }
            //pause
            else {
                console.log("Mode dynamique interrompu");
                jouer=0;
                razAir();
                dynBtn.src=iconPlay;
                dynBtn.disabled=true;
                return;
            }
            // Create a new 'change' event
            var event = new Event('change');
            
            //cycle des transits - synchronisation musique-affichage transits
            (async () => {
                do
                {
                    //instrument=x
                    for (x=0;x<radioDyn.length;x++){
                        if (radioDyn.item(x).checked==true) break;
                    }
                    //vitesse si audio coupé
                    if (checkAudio.checked==false){
                        cachecurseur(false,vitesse1,label1,vitesse0,label0);
                        vitesse=vitesse0.value;

                    }else{//tempo en audio
                        cachecurseur(true,vitesse1,label1,vitesse0,label0);
                        vitesse=vitesse1.value;
                    }
                    dateDebut.value=choixDate.value;
                    //console.log(dateDebut.value);
                    const res = await audioBoucle(x,volume,vitesse/10,accords,octaves); //joue notes du jour
                    //on revient ici après un clic sur "pause"
                    razAir();
                    if (jouer!=0) {
                        incJour.stepUp(); //jour suivant
                        // Dispatch it."await" en lien avec "await requetes" dans changedateheure (zodiaque.js)
                        await incJour.dispatchEvent(event); //affiche transits et charge "air"
                    }
                } while (jouer!=0);
                okDynamique=0;
                dynBtn.disabled=false;
                devalideTout(false);
                affiSave.style.fontWeight="bold";//remet en gras le nom sélectionné
            })();
    });
}

function razAir(){
    air[0]=[];//transitée
    air[1]=[];//transitante
    air[2]=[];//rétrograde (0 ou 1)    
}

function cachecurseur(x,a,b,c,d){
    a.hidden=!x;
    b.hidden=!x;
    c.hidden=x;
    d.hidden=x;   
}

function random(x){
    return Math.floor(Math.random()*x);
}

//*** attention, modifier zodiaque.js (fin de fichier) en cas de changement des paramètres : duree,octave,insrument !!!
async function audioBoucle(instrument,volume,vitesse,accords,octaves) {
    if (checkAudio.checked==true && checkAccords.checked==true && air[0].length>=2) await litAccords(instrument,volume,vitesse,accords,octaves);
    
    var i,j,k,octave,duree,tempo,x,x0,y,abc,bcd;
    x0=air[0].length;
    for (y=0;y<x0;y++){//ne pas remplacer x0 par air[0].length car air[0] change de taille au fur à mesure
        if (air[0].length==0) break; //sortie en mode stop
        //lecture aléatoire des notes du jour (mettre x=0 pour jouer dans l'ordre)
        x=0;//random(air[0].length);
        
        i=air[0][x];// transitée
        j=air[1][x]; //transitante
        k=air[2][x]; //transitante retrograde ? (0 ou 1)
        //transitante
        if (j<=1){//transitantes Soleil et Lune
            octave=4;
            duree=2; //sec
            tempo=150; //msec
            if (instrument==3) instrument=2;//mix : guitare
        }
        else if (j<=4){//...Mercure à Mars
            octave=4;
            duree=2;
            tempo=250;
            if (instrument==3) instrument=0; //mix : piano
        }
        else if (j<=6){//...jupiter et Saturne
            octave=4;
            duree=2.5;
            tempo=350;
            if (instrument==3) instrument=0;//mix : piano
        }
        else if (j>=7){//...Uranus à NN
            octave=3;
            duree=3;
            tempo=450;
            if (instrument==3) instrument=1;//mix : orgue
        }
        //corrections diverses
        if (checkDieses.checked==false) k=0; //pas de dièses
        if (i==6) octave=3; //Saturne- le Si octave 4 est trop aigu !
    
        //the audio element’s play() method returns a promise.
        //a promise can succeed or fail. 
        try {
            //result1 renvoie : resolve
            const result1 = await joue(i,j,k,instrument,octave,volume,duree,tempo/vitesse,air[0]);
        } catch (err) { //In the case it fails, you need to handle that failure.
            console.log (err);
        }
        //compression de air en supprimant la note jouée
        for (var z=0;z<=2;z++){
            abc=air[z].slice(0,x);//partie avant
            bcd=air[z].slice(x+1);//partie arrière
            air[z]=abc.concat(bcd);//regroupement des 2 parties
        }
    }
}

function creeAccords(){
    //mettre contenu de chaque accord en ordre croissant (en valeur absolue) pour la recherche ci-dessous !
    
    //accords sans dièses : DoM,Rém,Mim,FaM,SolM,Lam,Sidim
    var accords=[[0,3,5],[1,2,4],[0,3,6],[1,2,5],[3,4,6],[0,1,5],[2,4,6]];
    var octaves=[[3,3,3],[3,3,3],[3,3,3],[3,3,4],[3,4,3],[4,3,3],[4,4,3]];
    //ajoute accords avec dièses : Dom,RéM,MiM,Fam,Solm,LaM,SiM 
    accords.push([3,-4,5],[1,-2,4],[0,-3,6],[2,-3,5],[-1,3,4],[0,1,-5],[-2,-4,6]);
    octaves.push([3,3,3],[3,3,3],[3,3,3],[3,3,4],[3,3,4],[4,3,3],[4,4,3]);
    
    return [accords,octaves];
}

async function litAccords(instrument,volume,vitesse,accords,octaves) {
    var i,j,k,octave,duree,tempo,x,y,z,abc,bcd,cde,def,a,b,c=[],aire=[];
     //crée un array avec les numéros d'accords
    for (i=0;i<accords.length;i++){
        aire.push(i);
    }
    //inverse le signe des planètes si dièses est cochée et la planète transitante est rétrograde
    for (i=0;i<air[0].length;i++){
        if (checkDieses.checked==true && air[2][i]==1) air[0][i]=-air[0][i];
    }
    //recherche si accords dans la série du jour
    for (z=0;z<accords.length;z++){
        if (air[0].length==0) break; //sortie après un stop
        //lecture aléatoire d'1 accord (mettre x=0 pour jouer dans l'ordre)
        x=0;//random(aire.length);
            
        c[0]=air[0].findIndex(element => element==accords[x][0]);
        c[1]=air[0].findIndex(element => element==accords[x][1]);
        c[2]=air[0].findIndex(element => element==accords[x][2]);
            
        //accord trouvé
        if (c[0]!=-1 && c[1]!=-1 && c[2]!=-1){
            //console.log ("accord",accords[x]);
            for (y=0;y<=2;y++){
                i=Math.abs(air[0][c[y]]);//transitéé
                j=air[1][c[y]];//transitante
                k=air[2][c[y]];//transitante rétrograde (0 ou 1)
                if (checkDieses.checked==false) k=0; //pas de dièses (inutile...)
                octave=octaves[x][y];
                duree=3;
                if (y==2) tempo=500;//dernière note de l'accord
                else tempo=0;
                if (instrument==3) instrument=0;//mix
                try {
                    const result1 = await joue(i,j,k,instrument,octave,volume,duree,tempo/vitesse,air[0]);
                } catch (err) { //In the case it fails, you need to handle that failure.
                    console.log (err);
                }
            }
            //compression de air en supprimant les 3 notes de l'accord joué
            for (y=0;y<=2;y++){
                abc=air[y].slice(0,c[0]);//partie avant
                bcd=air[y].slice(c[0]+1,c[1]);//suite 1
                cde=air[y].slice(c[1]+1,c[2]);//suite 2
                def=air[y].slice(c[2]+1);//partie arrière
                air[y]=abc.concat(bcd,cde,def);//regroupement des 4 parties
            }
            //compression de aire (retrait du No d'accord joué)
            abc=aire.slice(0,x);//partie avant
            bcd=aire.slice(x+1);//partie arrière
            aire=abc.concat(bcd);//regroupement des 2 parties
        }
    }
    //enlève le signe des planètes
    for (var i=0;i<air[0].length;i++){
       air[0][i]=Math.abs(air[0][i]);
    }
}

//Audio : Synth.play(sound, note, octave, duration);
function joue(x,y,z,instrument,octave,volume,duree,tempo,jour){//x=transitée, y=transitante z= retrograde (pour les dièses #)
    //joue audio si option cochée
    if (checkAudio.checked==true){
        var note=planetesNotes[z][x];
        var url;
         //console.log(jour,Planetes[x],Planetes[y],planetesNotes[z][x],"instrument:",instrument,"octave:",octave," durée:",duree,"sec tempo:",tempo,"msec");
        if (tableNotes[instrument][z][x][octave][duree].length==0){//crée la note dans audiosynth et l'écrit dans tableNotes
            Synth.setVolume(1);//1=100%
            url = Synth.play(instrument,note,octave,duree);//note non jouée, juste récupérée
            tableNotes[instrument][z][x][octave][duree]=url;//21 notes possibles ou 42 avec les dièses (par instrument)
            totalNotes+=1;
            console.log ("note créée :",instrument,z,x,octave,duree,"total :",totalNotes);
        }else{//lit la note directement depuis tableNotes
            url=tableNotes[instrument][z][x][octave][duree];
           // console.log ("lit",instrument,z,x,octave,duree);
        }
        var audio = new Audio(url);
        audio.volume=volume.value/100;
        audio.play();
    }
   //tempo : intervalle entre 2 notes (en ms)
    return new Promise(resolve => {
        setTimeout(() => {
            resolve("resolve");
        }, tempo);
    });
}
