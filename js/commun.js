var tabWiki,
    tabUtc,
    tabMeteo,
    clefSave,
    page_options=0;

var natif = {
    date: document.getElementById("1"),
    heure:document.getElementById("2"),
    lieu: document.getElementById("3"),
    utc: document.getElementById("4"),
    latA: document.getElementById("5"),
    latB: document.getElementById("5b"),
    longA: document.getElementById("6"),
    longB: document.getElementById("6b"),
    nom: document.getElementById("0")
}

function ajoutZero(e){
    if (e.length==1){e="0"+e;}
    return e
}
//conv DegresDecimal to Degres.Minutes(avec symbole des degrés ou "." entre entier et décimale si "b" présent)
//par ex. si a=25.5, return si b absent : 25°30', si b=1 : 25.30, si b=2 : [25,30]
function cDdDm(a,b){
    if (!b) b=0;
    //partie entière
    var x=Math.trunc(a);//conserve le signe contrairement à floor(a);
    //partie décimale
    var y=Math.abs(arrondi(a-x));
    //conversion en minutes
    y=Math.round(y*60);
    if (y==60) {y=0;x+=1}
    var deg=String(x);
    if (a<0 && a>-1) deg="-"+deg;
    var deg0=deg; //partie entière
    if (b==0) deg+="°";//symbole des degrés
    else deg+=".";
    if (y) deg+=ajoutZero(String(y));
    if (b==0) deg+="'";
    if (b==2) return[deg0,y];//retourne 2 nombres (deg et min)
    return deg;//retourne 1 string (deg.min)
}

function arrondi(x,decimales) {//retourne 1 string
    if (!decimales)decimales=2;
  return Number.parseFloat(x).toFixed(decimales);
}

//bordure en rouge si changement
function verif(a,x,y){
    if (x==undefined) return;
    if (x!=y) a.style.borderColor="red"; 
}

//suppression bordures rouges
function clearBorders(){
    var inputs=cadreHaut.getElementsByTagName("input")
    for (var i = 0; i < inputs.length; i++) {
        inputs[i].style.borderColor="";
    }
}

//*** auto-configure latitude/longitude et utc ***

//source longitude : https://nominatim.org/release-docs/develop/api/Search/
//source timezones : https://github.com/darkskyapp/tz-lookup-oss
//source calcul utc : https://github.com/mobz/get-timezone-offset

autoSet.addEventListener("click", async() => {
    var dateA=natif.date.value,
        heureA=natif.heure.value  || "12:00",
        ville=natif.lieu.value,
        utcA=natif.utc.value,
        latA=natif.latA.value,
        latB=natif.latB.value,
        longA=natif.longA.value,
        longB=natif.longB.value;
        
    //sortie anticipée...
    if (ville=="" || ville=="undefined"){natif.lieu.required=true;return};
    if (page_options==0) clearBorders();
    //** recherche latitude/longitude **
        labelLieu.textContent=labelTimezone.textContent="";
        autoSet.hidden=true;
        //lecture fichier json sur site web
        var requestURL = 'https://nominatim.openstreetmap.org/search?q='+ville+'&format=json';
        var details=await litJson(requestURL);
        if (details.length && details!="erreur"){
            //localité trouvée
            labelLieu.style.color="green";
            labelLieu.textContent=details[0].display_name;
            //latitude
            var x=Number(details[0].lat);
            var [i,j]=cDdDm(x,2);
            if (clefSave && page_options==0){
                var [k,l]=cDdDm(latNatal,2);
                verif(natif.latA,k,i);
                verif(natif.latB,l,j);
            }
            [natif.latA.value,natif.latB.value]=[i,j];
            //longitude
            x=Number(details[0].lon);
            [i,j]=cDdDm(x,2);
            if (clefSave && page_options==0){
                var [k,l]=cDdDm(longNatal,2);
                verif(natif.longA,k,i);//comparaison de la valeur trouvée à celle du natal
                verif(natif.longB,l,j);
            }
            [natif.longA.value,natif.longB.value]=[i,j];
        }else{
            labelLieu.style.color="red";
            labelLieu.textContent=labelsGauche[21];//non trouvé
            autoSet.hidden=false;
            return;
        }
    
    //** recherche utc **
    
    //recherche timezone
    var zone=tzlookup(details[0].lat, details[0].lon);
    //calcul utc
    var date;
    if (dateA) date=new Date(dateA+" "+heureA);//ajout heureA pour précision sur passage heure été/hiver
    else date=new Date();
    var offset = getTimezoneOffset( zone, date );//en minutes
    offset=Math.round(-offset/60*100)/100;//*100/100=arrondi à 2 décimales (sans 0 si entier)
    if (clefSave && page_options==0) verif(natif.utc,utcNatal,offset);
    natif.utc.value=offset;
    labelTimezone.style.color="green";
    labelTimezone.textContent=zone+ " : utc "+offset;
    autoSet.hidden=false;
    //commenter le return pour utiliser API timezonedb ci-dessous
    return;                
    
                                //méthode API abandonnée (v. bas du fichier)
//                                 var refTime=new Date('1970-01-01'),
//                                     endTime=new Date(dateA),
//                                     diff=(endTime-refTime)/1000; //de msec à sec.
//                                     
//                                 requestURL=timezonedb(details[0].lat,details[0].lon,diff);
//                                 details=await litJson(requestURL);
//                                 if (details.status=="OK") {
//                                     //entre 1890 et 1920 environ gmtoffset=561, utc= 0.1599999
//                                     if (details.gmtOffset==561) details.gmtOffset=540;//donne utc=0.15
//                                     natif.utc.value=details.gmtOffset/60/60;//secondes en heures
//                                     labelLieu.textContent+="  (utc :" + natif.utc.value + ")";
//                                 }else natif.utc.value="";
                                
});

async function litJson(url){
    var response;
    const request = new Request(url);
    try {
        response = await fetch(request);//ok
    }
    catch (e) {
       // console.error(e); // erreur
        labelTimezone.style.color="red";
        labelTimezone.textContent=labelsGauche[25];//serveur inacessible
        return "erreur";
    }
    const details = await response.json();
    return details;
}

//calcul utc
var locale = 'en-US';
var us_re = /(\d+).(\d+).(\d+),?\s+(\d+).(\d+)(.(\d+))?/;

var format_options = {
	timeZone: "UTC",
	hourCycle: 'h23',
	year: 'numeric',
	month: 'numeric',
	day: 'numeric',
	hour: 'numeric',
	minute: 'numeric'
};

var utc_f = new Intl.DateTimeFormat(locale, format_options );

function parseDate( date_str ) {
	date_str = date_str.replace(/[\u200E\u200F]/g, '');
	var date_a = us_re.exec( date_str );
	return [].slice.call(us_re.exec( date_str ), 1)
		.map( Math.floor );
}

function diffMinutes( d1, d2 ) {
	var day = d1[1] - d2[1];
	var hour = d1[3] - d2[3];
	var min = d1[4] - d2[4];

	if( day > 15 ) day = -1;
	if( day < -15 ) day = 1;

	return 60 * ( 24 * day + hour ) + min;
}

function getTimezoneOffset( tz_str, date ) {

	format_options.timeZone = tz_str;

	var loc_f = new Intl.DateTimeFormat(locale, format_options );

	return diffMinutes(
		parseDate( utc_f.format( date )),
		parseDate( loc_f.format( date ))
	);
}//fin utc

//**** affiche wikipedia ****
infoWiki.addEventListener("click",async () => {
    tabWiki=await ouvreTab(tabWiki,"https://" + navigator.language.slice(0,2)+".wikipedia.org/wiki/");
});

//*** affiche time.is ***
infoUtc.addEventListener("click",async () => {
    tabUtc=await ouvreTab(tabUtc,"https://time.is/" + navigator.language.slice(0,2)+"/");
});

//*** météo ***
//source : https://www.infoclimat.fr/previsions-meteo-par-ville.html
infoLatitude.addEventListener("click",async () => {
    if (groupesNoms.includes("écrivains")==false) return;
    //conversion deg.min en degres.decimales
    let x=Number(natif.latA.value);
    let y=Number(natif.latB.value);
    let la=x+y/60;
    x=Number(natif.longA.value);
    y=Number(natif.longB.value);
    let lo=x+y/60;
    if (la==0 || lo==0) {la=latitude;lo=longitude;}
    let c= Math.abs(la-48.71)<0.01 && Math.abs(lo-2.25)<0.01 ? "QYWxh" : "iZWxv";//"QYWxh"=Pala(iseau) "CZWxv"="belo(iseau)"
    
    tabMeteo=await ouvreTab(tabMeteo,"https://www.infoclimat.fr/public-api/mixed/iframeSLIDE?_ll="+la+","+lo+"&_inc=WyJ"+c+"aXNlYXUiLCI4NCIsIjI5ODg3NTgiLCJGUiJd&_auth=ABpRRgF%2FAyEDLltsBXMELQRsAzYBd1J1AHwGZQtuVSgCZlM2UjdXMwdvVSgALwM1AC1TMQowVHMGelcyCm5ROgBrUTIBZwNpA3NbJgU9BDYEPgNiATpSdQB8BmYLYVUoAmBTNFIzVysHaVUxADcDKQAyUzAKMFRzBnpXMQpgUTIAa1EwAWEDZgNqWzkFPAQvBCgDZQE7Uj4AZQZiC2FVNgJoUzRSMVdmBzxVMgA0AykANVMwCj1UZQZgVzIKYFEyAHxRKgEbAxIDcVt5BXcEZQRxA34Ba1I0ADc%3D&_c=8b66ceafc114f7fc79640329ea69fe68");
});

async function ouvreTab(tab,adresse){
    //contrôle
    const ville=natif.lieu.value;
    //effacement onglet si existe déjà
    if (tab){
        let querying= await browser.tabs.query({currentWindow: true });
        for (const x of querying) {
            if(x.id==tab) {
                let removing=await browser.tabs.remove(tab);
                break;
            };
        }
    };
    //création onglet
    if (adresse.search("infoclimat")==-1) adresse+=ville;
    let creating = await browser.tabs.create({url: adresse});
    return creating.id;//Numéro tab
}


//*** API (non utilisée) pour déterminer utc à partir de lat/long ***
// function timezonedb(lat,lng,diff){
//     var url = 'http://api.timezonedb.com/v2.1/get-time-zone?key=3B75803HPF73&format=json&by=position&lat='+lat+'&lng='+lng+'&time='+diff;
//     return url;    
// }

