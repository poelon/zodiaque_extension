
//source : https://github.com/mdn/webextensions-examples/tree/master/quicknote

//si ajout ou suppression d'un champ, ajuster ligne 505 : max-3
var noteContainer = document.querySelector('.note-container');
var cadreBas = document.querySelector('.cadregaucheverticalbas');
var cadreHaut = document.querySelector('.cadregaucheverticalhaut');
var clearBtn = document.querySelector('.clear');
var groupesListe=document.getElementById('groupes');
var boutonExport = document.getElementById("export");
var boutonImport = document.getElementById("import");
var boutonAnnuler = document.getElementById("annuler");
var boutonOk = document.getElementById("ok");
var fileJson = document.getElementById("fileJson");
var champJson=[];
var importe="";
var affiSave;
var updateName;
var noteEdite=[];
var natifRef=[];
var grid;

//traductions groupes
var labelsGroupes=browser.i18n.getMessage("groupes").split(",");
boutonAnnuler.textContent = labelsGauche[6];
boutonOk.textContent = "\u00A0\u00A0ok\u00A0\u00A0";//ajoute 2 espaces en début et fin
var groupesNoms=[];
var groupeDefaut=labelsGroupes[5];

initialize();

//boutons du bas
clearBtn.addEventListener('click', clearAll);
boutonExport.addEventListener('click', exportJson);
fileJson.addEventListener('change', (event) => {
    importJson();
},onError);
boutonImport.addEventListener('click',() => { //ne pas modifier, nécessite fileJson ligne au-dessus
    fileJson.value="";//sinon chrome n'ouvre pas 2 fois de suite le même fichier
    fileJson.click();//ouvre boîte de dialogue pour choix fichier
},onError);

//boutons du haut
iconeGomme.addEventListener('click', function(){
   //cadre gauche : inputs
    var inputs=cadreHaut.getElementsByTagName("input")
    for (var i = 0; i < inputs.length; i++) {
        inputs[i].value = "";
        inputs[i].required = false;
        inputs[i].style.borderColor="";
    }
    labelLieu.textContent=labelTimezone.textContent=labelGroupes.textContent="";
    labelFiche.innerText="";
    if (affiSave) affiSave.style.fontWeight="";//enlève "gras" du nom sélectionné
    clefSave="";
});
iconeAdd.addEventListener('click', function(){
    if (formControl("add")==1) addNote();
});
iconeUpdate.addEventListener('click', function(){
    var x,y;
    if (!clefSave || clef=="") {commente("red",labelsGauche[29]);return};//pas de nom sélectionné
    //vérifie si cases remplies
    if (formControl("update")==0) return;
    //vérifie si changement des données de la fiche
    if (natifCompare()==9){//non
        commente("red",labelsGauche[26]);//"pas de changement"
        return;
    }
    labelFiche.innerText =abc="";
    //recherche fiche
    var clef=sp(natif.nom.value); 
    var [existe,groupe]=cgv(clef);
    //le nom a été changé : ok, sauf s'il existe déjà
    if (natifRef[8]!=natif.nom.value){
        if (existe==1){
            commente("red",natif.nom.value + "  " + labelsGroupes[6] + " (" + groupe + ")"); //"existe déjà";
            return;
        }else{
            [existe,groupe]=cgv(natifRef[8]);//récupère la fiche sous l'ancien nom
            //théoriquement inutile...
            if (typeof groupe==='undefined') {commente("red",clef+" : "+labelsGauche[28]);return};//fiche n'existe pas !
        }
    }
    
    //exécution
    updateName="";
    var valeur=preStore();
    //écriture disque
    updateNote(clefSave,clef,valeur,groupe,1);
    updateName=clef;
    natifCopie();
    var abc=labelsGauche[7]+"  ";
    if (clefSave!=clef) abc+=clefSave +"-->";
    abc+=clef + " : ok";
    commente("green",abc);
    clearBorders();
});
iconeDelete.addEventListener('click', function(){
    var clef=natif.nom.value;
    if (!clefSave || clef=="") {commente("red",labelsGauche[29]);return};//pas de fiche sélectionnée
    if (clef!=natifRef[8]) {commente("red",labelsGauche[27] + " impossible");return};////chgt de nom impossible
    var [existe,groupe]=cgv(clef);
    if (existe==0) {commente("red",clef+" : "+labelsGauche[28]);return};//fiche n'existe pas
    Promise.resolve().then(() => {//évite sur chrome message : [violation] click handler took xx msec  
        var abc=labelsGauche[9];//confirmer suppression
        if (window.confirm(abc+clef)) { 
            removeNote("fiche_"+clef+"/*"+groupe);
            devalideTout(false);
            initialize(groupe);
            iconeGomme.click();
            commente("green",labelsGauche[8]+"  "+clef+" : ok");
        }  
    });
});

function commente(a,b){
    labelFiche.style.color=a;
    labelFiche.style.fontSize= 0.8+"vw";
    labelFiche.innerText=b;
}

//clef/groupe/valeur
function cgv(x){//x=nom
    var y=0;
    for (i=0; i<fiches.length; i++){
        if (fiches[i].nom==x){
            var groupe=fiches[i].groupe;
            y=1;
        }
    }
    return [y,groupe];
}

function formControl(x){
var total=0,element,abc="";
var y=["","","","","utc","latitude","longitude"];

    for (var i=0; i<=6; i++){
        element=document.getElementById(String(i));
        if (element.validity.valid==false || sp(element.value)==""){
            if (i<=3) abc +=labelsGauche[i]+", ";
            else abc +=y[i]+", ";
            continue;
        }
        total+=1;
    }
    if (total!=7){
        commente("red",labelsGauche[15]+"  "+ abc);//manque...
        return 0;
    }
    return 1;
}
function natifCopie(){
    var a=Object.values(natif);
    for (var i=0;i<a.length;i++){
        natifRef[i]=a[i].value;
    }
}
function natifCompare(){
    var a=Object.values(natif);
    var b=0;
    for (var i=0;i<a.length;i++){
        if (natifRef[i]==a[i].value) b+=1;
    }
    return b;//max=9
}
//enlève les espaces au début et à la fin,source:https://www.toptip.ca/2010/02/javascript-trim-leading-or-trailing.html
function sp(x){//=string.trim() ?
    x = x.replace(/^\s+|\s+$/g, "");
    return x;
}

//création object Fiche
function Fiche(groupe,nom,details) {
  this.groupe = groupe;
  this.nom = nom;
  this.details = details;
}
var fiches = [];

//création object Affiche
function Affiche(nom,details) {
  this.nom = nom;
  this.details = details;
}

//*********** initialisation **************

function initialize(Group) {
    clearBtn.disabled=true;
    efface(noteContainer);
    efface(groupesListe);
    //options des groupes (créer, renommmer,etc.)
    groupesNoms=[groupeDefaut];//général
    var element;
    for (var i=0 ; i<=4; i++){
        element=document.createElement('option');
        if (i==0){element.setAttribute("id","trait");}
        element.text=labelsGroupes[i];
        groupesListe.appendChild(element);
    }
    var trait = document.getElementById("trait");
    element=document.createElement('option');
    element.text=groupeDefaut;
    element.selected=true;
    groupesListe.insertBefore(element, trait);
    //
    champJson=[];
    fiches = [];
    //lecture fichier local storage (clef=fiche_+nom+"/*"+groupe, valeurs=date,heure,lieu,utc,latitude,longitude)
    var valeur;
    var indice=0;
    var items=[];
    labelTotal.innerText=""; //nombre de groupes+fiches
    var gettingAllStorageItems = browser.storage.local.get(null);
    gettingAllStorageItems.then((results) => {
        var clefs = Object.keys(results); //ex. fiche_nom1, fiche_nom2
        if (clefs.length){
            for (var clef of clefs) {
                //ignore option ville par défaut ou stockages d'autres applis
                if (clef=="zodiaque" || clef.search("fiche_")==-1){continue;}
                valeur = results[clef]; 
                clef=clef.slice(6);
                //items(array) = clef(string) + valeur(array) => conversion de valeur de Array en String
                items[indice]=clef+"val:"+valeur; 
                indice+=1;
            }
        } 
        
        //affichage
        if(indice==0) {
            displayNote("Leonard de Vinci",["1452-04-14","22:30","Vinci","1","43.47","10.55"]);
        }
        else {
            // sort by name (majuscules=minuscules)
            items.sort(function(a, b) {
            var nameA = a.toUpperCase(); // ignore upper and lowercase
            var nameB = b.toUpperCase(); // ignore upper and lowercase
            if (nameA < nameB) {
                return -1;
            }
            if (nameA > nameB) {
                return 1;
            }
            // names must be equal
            return 0;
            });
            
            //affichage
            var abc,nom,groupe,x;
            for (var i=0; i<indice ; i++){
                clef=items[i].split("val:")[0];
                abc=items[i].split("val:")[1];
                valeur=[];
                //reconversion de Valeur de String en Array
                for (var j=0; j<=5; j++){
                    x=abc.split(",")[j];
                    //conversion ancien format date de "jj/mm/aaaa" à "aaaa-mm-jj" 
                    if (j==0) x=normaliseDate(x);
                    valeur.push(x);
                }
                
                //gestion des groupes
                abc=clef.split("/*");
                    nom=abc[0];
                    //groupe par defaut : général
                    groupe=groupeDefaut;
                    if (abc.length > 1 && abc[1] != ""){
                        groupe=abc[1];
                    }else{
                        //pas de groupe défini : ajoute fiche sur disque avec groupe "général" et efface fiche sans groupe
                        console.log(" ajoute " +nom+" - " +valeur+" - " +groupe);
                        updateNote(nom,nom,valeur,groupe); 
                        var removingNote = browser.storage.local.remove("fiche_"+nom);
                        removingNote.then(() => {
                            console.log("effacement : " + "fiche_"+nom);
                        }, onError);
                    }
                    //teste si groupe existe déjà
                    var existe=0;
                    for (var j=0; j<groupesNoms.length; j++){
                        if (groupesNoms[j]==groupe){
                            existe=1;
                            break;
                        }
                    }
                    //non, ajoute nouveau groupe
                    if (existe==0) groupesNoms[groupesNoms.length]=groupe;

                    //sauvegarde fiche en object
                    var fiche = new Fiche(
                        groupe,
                        nom,
                        valeur,
                    );
                    fiches.push(fiche);
                    //json
                    clef=nom+"/*"+groupe;
                    champJson.push({clef,valeur});//ne pas changer=nom des champs
                    //affiche le groupe sélectionné sinon le groupe "général"
                    if (Group && groupe==Group){displayNote(nom,valeur);}
                    else if (!Group && groupe==groupeDefaut){displayNote(nom,valeur);}
                    
            }
            
            //affichage liste des groupes alphabétisée (hors accents et majuscules/minuscules)
            var x=groupesNoms.sort();
            var y=-1;
            var z=0;
            for (var i=0;i<groupesNoms.length;i++){
                if (x[i]==groupeDefaut) {
                    z=1;
                    continue;
                }
                if (x[i]==Group) y=i-z;//l'indice change si le nom apparait dans groupesnoms avant ou après "général"
                element=document.createElement('option');
                element.text=x[i];
                groupesListe.insertBefore(element, trait);
            }
            //affiche groupe général (démarrage) ou groupe sélectionné (ajout nom)
            var a=document.getElementById("groupes");
            a[y+1].selected=true;//a[0]="général"
          //  if (groupe==Group) element.selected=true;
            clearBtn.disabled=false;
            //nombre de groupes+fiches (singulier/pluriel)
            x=labelsGauche[32];//groupes
            if (groupesNoms.length<=1) x=x.slice(0,x.length-1);
            y=labelsGauche[31];//fiches
            if (champJson.length<=1) y=y.slice(0,y.length-1);
            labelTotal.innerText=groupesNoms.length+" "+x+ " "+champJson.length+" "+y;
        }
    }, onError);
}
//*********** fin initialisation **************

//*** groupes ****

groupesListe.addEventListener("change",() => {
    
    switch (groupesListe.value){
        //trait
        case labelsGroupes[0]:
            efface(noteContainer);
            break;
        //créer groupe
        case labelsGroupes[1]:
            labelTotal.innerText=labelsGroupes[1];
            devalideTout(true);
            efface(noteContainer);
            groupesListe.hidden=true;
            creeGroupe();
            break;
        //renommer groupe
        case labelsGroupes[2]:
            if (groupesNoms.length > 1){
                labelTotal.innerText=labelsGroupes[2];
                devalideTout(true);
                efface(noteContainer);
                groupesListe.hidden=true;
                renommeGroupe();
            }
            break;
        //assigner groupes
        case labelsGroupes[3]:
            if (groupesNoms.length > 1){
                labelTotal.innerText=labelsGroupes[3];
                devalideTout(true);
                efface(noteContainer);
                groupesListe.hidden=true;
                assigneGroupes();
            }
            break;
        //effacer groupes
        case labelsGroupes[4]:
            labelTotal.innerText=labelsGroupes[4];
            devalideTout(true);
            efface(noteContainer);
            groupesListe.hidden=true;
            effaceGroupe();
            break;
        //nom d'un groupe
        default:
            efface(noteContainer);
            //affichage
            for (var i=0; i<fiches.length; i++){
                if (fiches[i].groupe==groupesListe.value){displayNote(fiches[i].nom,fiches[i].details);}
            }
    }
},onError);

function creeGroupe(){
    grid=1;
    var element=document.createElement('input');
    noteContainer.appendChild(element);
    element.setAttribute("id","entree");
    var entree=document.getElementById("entree");
    boutonsGroupes();
}

function renommeGroupe(){
    var element;
    grid=2;
    for (var i=0; i<groupesNoms.length; i++){
        element=document.createElement('input');
        element.setAttribute("id","ele"+String(i));
        element.value=groupesNoms[i];
        //ajoute mention "(protégé)" au groupe par défaut (général)
        if (groupesNoms[i]==groupeDefaut) element.value+="  "+labelsGroupes[9];
        noteContainer.appendChild(element);
    }  
    labelGroupes.innerText=labelsGroupes[10];//2 groupes avec le même nom seront fusionnés
    boutonsGroupes();
}

function assigneGroupes(){
    var nom,select,option,element;
    grid=3;
    for (var i=0; i<fiches.length; i++){
        nom=document.createElement('label');
        nom.textContent=fiches[i].nom+"  ";
        nom.setAttribute("for","ele"+String(i));
        noteContainer.appendChild(nom);
        select=document.createElement('select');
        select.setAttribute("id","ele"+String(i));
        for (var j=0; j<groupesNoms.length; j++){
            option=document.createElement('option');
            option.text=groupesNoms[j];
            if (option.text==fiches[i].groupe){option.selected=true;}
            select.appendChild(option);
        }
        noteContainer.appendChild(select);
        element=document.createElement('br');
        noteContainer.appendChild(element);
    } 
    boutonsGroupes();
} 

function effaceGroupe(){
    var select,option;
    grid=4;
    select=document.createElement('select');
    select.setAttribute("id","ele");
    for (var j=0; j<groupesNoms.length; j++){
        option=document.createElement('option');
        option.text=groupesNoms[j];
        select.appendChild(option);
    }
    noteContainer.appendChild(select);
    labelGroupes.innerText=labelsGroupes[8]+"  ";//attention ! le contenu du groupe sera aussi effacé
    infobulle(labelGroupes,labelsGroupes[11]); //utiliser 'assigner groupes' pour déplacer les fiches dans un autre groupe
    boutonsGroupes();
}

//listeners pour les modifs de groupes
boutonAnnuler.addEventListener('click',() => {
    efface(noteContainer);
    labelGroupes.innerText="";
    boutonsDefaut();
    groupesListe.hidden=false;
    devalideTout(false);
    initialize();
});

boutonOk.addEventListener('click',() => {
    labelGroupes.innerText="";
    switch (grid){
    //crée groupe
    case 1:
        if (entree.value=="") {entree.required=true;return};
        for (var j=0; j<groupesNoms.length; j++){
            if (groupesNoms[j]==entree.value){
                labelGroupes.textContent=labelsGroupes[6]; //"existe déjà";
                return;
            }
        }
        groupesNoms[groupesNoms.length]=entree.value;
        element=document.createElement('option');
        element.text=entree.value;
        groupesListe.insertBefore(element, trait);
        efface(noteContainer);
        labelGroupes.textContent=labelsGroupes[7]+" " +element.text;//"assigner au moins une personne au groupe"
        groupesListe.hidden=false;
        element=document.getElementById("trait");
        element.selected=true;
        boutonsDefaut();
        devalideTout(false);
        break;
    //renomme groupe(s)
    case 2:
        for (var i=0; i<groupesNoms.length; i++){
            element=document.getElementById("ele"+String(i));
            //nom groupe effacé, bordure mise en rouge
            if (element.value=="") {element.required=true;return};
            //nom groupe inchangé
            if (groupesNoms[i]==element.value) continue;
            //nom "général" protégé
            if (groupesNoms[i]==groupeDefaut) continue;
            //nom groupe changé (si existe déjà, tranfert des noms dans ce groupe)
            for (var j=0; j<fiches.length; j++){
                if (groupesNoms[i] == fiches[j].groupe){
                    //écriture nouveau groupe (général si sans nom)
                    updateNote(fiches[j].nom,fiches[j].nom,fiches[j].details,element.value || groupeDefaut);
                    console.log("mise à jour : ",fiches[j].nom + " groupe : ",(element.value || groupeDefaut));
                    //effacement ancien groupe
                    var removingNote = browser.storage.local.remove("fiche_"+fiches[j].nom+"/*"+fiches[j].groupe);
                    removingNote.then(() => {
                      //  console.log("effacement : " + fiches[j].nom + " groupe : " + fiches[j].groupe);//génère message d'erreur !
                    }, onError);
                }
            }
        }
        boutonsDefaut();
        groupesListe.hidden=false;
        devalideTout(false);
        initialize();
        break;
    //assigne groupes
    case 3:
        for (var i=0; i<fiches.length; i++){
            element=document.getElementById("ele"+String(i));
            if (element.value != fiches[i].groupe){
                //écriture nouveau groupe
                updateNote(fiches[i].nom,fiches[i].nom,fiches[i].details,element.value);
                console.log("mise à jour : ",fiches[i].nom, " groupe : ",element.value);
                //effacement ancien groupe
                var removingNote = browser.storage.local.remove("fiche_"+fiches[i].nom+"/*"+fiches[i].groupe);
                removingNote.then(() => {
                    // console.log("effacement : " + fiches[i].nom + " groupe : " + fiches[i].groupe);//génère message d'erreur !
                }, onError);
            }
        }
        boutonsDefaut();
        groupesListe.hidden=false;
        devalideTout(false);
        initialize();
        break;
    //efface groupe
    case 4:
        var a,b,c,car,removingNote;
        var groupe=document.getElementById("ele").value;
        if (groupe==groupeDefaut){
            labelGroupes.innerText="impossible !";
            return;
        }
        var liste=Object.values(champJson);
         //car : nombre de noms dans le groupe
        car=0;
        for (var j=0;j<liste.length;j++){
            a=liste[j].clef;
            b=a.search("/");
            c=a.slice(b+2);//nom groupe
            if (groupe==c) car+=1;
        }
        var abc=labelsGauche[30];//confirmer suppression groupe
        if (window.confirm(abc+groupe + " (+ "+car+" "+ labelsGauche[31]+")")) { //31=fiches
            for (var i=0; i<fiches.length; i++){
                if (fiches[i].groupe==groupe){
                    removingNote = browser.storage.local.remove("fiche_"+fiches[i].nom+"/*"+fiches[i].groupe);
                    console.log("effacement : " + fiches[i].nom + " groupe : " + fiches[i].groupe)
                }
            }
        }
        boutonsDefaut();
        groupesListe.hidden=false;
        devalideTout(false);
        initialize();
        break;
    }
});


function boutonsDefaut(){
    var a=[options,clearBtn,boutonImport,boutonExport];
    for (i of a){i.hidden=false};
    boutonAnnuler.hidden=true;
    boutonOk.hidden=true;
}
function boutonsGroupes(){
    var a=[options,clearBtn,boutonImport,boutonExport];
    for (i of a){i.hidden=true};
    boutonAnnuler.hidden=false;
    boutonAnnuler.disabled=false;
    boutonOk.hidden=false;
    boutonOk.disabled=false;
}
//*** fin groupes ***

/* display previously-saved stored notes on startup */

function efface(zone) {
    // Removing all children from an element
    while (zone.firstChild) {
        garbage=zone.removeChild(zone.firstChild);
    }
}


//conversion date de "jj/mm/aaaa" à "aaaa-mm-jj"
function normaliseDate(date){
    var x;
    if (date.split('/').length>=3){
        x=date.slice(6,10) +"-"+date.slice(3,5)+"-"+date.slice(0,2);
    }
    else x=date;
    return x;
}

//*********** import/export *********************

//remplace la permission "downloads" de manifest.json
//source : https://ourcodeworld.com/articles/read/189/how-to-create-a-file-and-generate-a-download-with-javascript-in-the-browser-without-a-server
function download (fichier, texte) {
    const element = document.createElement('a');
    element.setAttribute('href', texte);
    element.setAttribute('download',fichier);
    element.style.display = 'none';
    document.body.appendChild(element);
    element.click();
    document.body.removeChild(element);
}

//l'option "save as" apparaît si dans préférences de firefox, téléchargements : Toujours demander où enregistrer les fichiers
function exportJson(){
    if (champJson.length==0) return;
    //init
    imexInit("Export");
    var a;
    var b=groupesFiches(groupesNoms,fiches);
    //affichage liste des groupes (checkbox)
    for (var i=0;i<groupesNoms.length;i++){
         //attention, si changement 2 lignes ci-dessous nécessité de corriger i et i++ lignes 779 (imexcondense),804 et 809 (imexlisten "tous" et "aucun")
        infobulle(imexZone,b[i].toString());
        imexZone.append(" ");
        var [x,label]=creeCheck(groupesNoms[i],imexZone);
        label.textContent=" "+groupesNoms[i]+" ("+b[i].length+")  ";//groupes+(nbre)
        a = document.createElement('br');
        imexZone.append(a);
        x.checked=true;
        x.addEventListener("change",imexListen,false);
    }
    //boutons ok et annuler
    okAnnule("export");
}

function importJson(){
    var reader = new FileReader();
    var file = fileJson.files[0];
    //lecture fichier json
    reader.readAsText(file);
    reader.onload = function(event) {
   // console.log(reader.result);
    importe= JSON.parse(reader.result);
    //tests sur la "qualité" du fichier json
    if (Array.isArray(importe)==false) return;
    if (importe.length==0) return;
    if (typeof importe[0].clef==='undefined') return;
    //init
    imexInit("Import");
    var a,b,c,x,recu=[];
    
    //recherche groupes
    for (var j=0;j<importe.length;j++){
        x=0;
        a=importe[j].clef;
        b=a.search("/");
        c=a.slice(b+2);//nom groupe
        for (var i=0;i<recu.length;i++){
            if (recu[i]==c) {
                x=1;
                break;
            }
        }
        if (x==0) recu.push(c);
    }
    //tri alphabétique noms des groupes
    recu.sort(); 
    //nbre de fiches par groupes
    var b=groupesFiches(recu,importe);
    //affichage groupes
    for (i=0;i<recu.length;i++){
         //attention, si changement 2 lignes ci-dessous nécessité de corriger i et i++ lignes 779 (imexcondense),804 et 809 (imexlisten "tous" et "aucun")
        infobulle(imexZone,b[i].toString());//noms
        imexZone.append(" ");
        var [x,label]=creeCheck(recu[i],imexZone);
        label.textContent=" "+recu[i]+" ("+b[i].length+")  ";//groupe+(nbre de fiches);
        a = document.createElement('br');
        imexZone.append(a);
        x.checked=true;
        x.addEventListener("change",imexListen,false);
    }
    //boutons ok et annuler + message
    okAnnule("import");
    a = document.createElement('label');
    a.textContent=labelsDroite[41];
    a.setAttribute('for',"import");//pour chrome (un label doit être associé)
    imexZone.append(a);
    }
}    

function imexInit(type){
    devalideTout(true);
    svg.style.display="none";
    tableau.hidden=true;
    canvasCache(true);
    efface(imexZone);
    var a,b,c;
    a = document.createElement('br');
    b = document.createElement('br'); 
    c = document.createElement('label');
    c.textContent=type;
    c.style.fontWeight="bold";
    c.setAttribute('for',"tous");//pour chrome (un label doit être associé)
    imexZone.append(a,b,c);  
    //choix 'tous,aucun' (radio)
    var y=["tous","aucun"];
    for (var i=0;i<=1;i++){
        a = document.createElement('input');
        a.setAttribute('type', 'radio');
        a.setAttribute('id', y[i]);
        a.setAttribute('name', 'radioTousAucun');
        b = document.createElement('label');
        b.setAttribute('for',y[i]);//permet de cliquer sur le label
        b.textContent=labelsDroite[39+i];
        if (i==0) a.checked=true;
        imexZone.append(a,b);    
        a.addEventListener('click',imexListen,false);
    }
    a = document.createElement('br');
    imexZone.append(a);     
}

function okAnnule(type){
    var a,y=[labelsGauche[6],type];
    for (var i=0;i<=1;i++){
        a = document.createElement('input');
        a.setAttribute('type', 'button');
        a.setAttribute('id', y[i]);
        a.setAttribute('value', y[i]);
        a.addEventListener('click',imexListen,false);
        b = document.createElement('span');
        b.textContent = "\u00A0\u00A0\u00A0\u00A0";//ajoute 4 espaces
        imexZone.append(a,b);
    }   
}

function compareNumbers(a, b) {//pour tri nombres croissants
    return a - b;
} 

//nombre de noms par groupes
function groupesFiches(groupes,noms){
    var a,b,c,d,i,j,x=[];
    //init array
    for (j=0;j<groupes.length;j++){
        x[j]=[];
    }
    //noms
    for (i=0;i<noms.length;i++){
         //teste si import ou export
        if (typeof(noms[0].groupe)==='undefined'){
            //import
            a=noms[i].clef;
            b=a.search("/");
            c=a.slice(b+2);//nom groupe
            d=a.slice(0,b);//nom fiche
        }else {
            //export
            c=noms[i].groupe;
            d=noms[i].nom;
        }
        //x=groupes+noms
        for (j=0;j<groupes.length;j++){
            //groupe détecté = +1
            if (c==groupes[j]) {
                x[j].push("_"+d);
                break;
            }
        }
    }
    return x;
}

function imexCondense(liste){
 //filtre groupes avec liste par numéro des noms associés à un groupe désélectionné
    var a,b,c,noexport=[];
    //groupes
    for (var i=9;i<imexZone.children.length;i+=4){//corriger i  et i++ si changement lignes 626 (exportjson) et 676 (importjson)
        if (imexZone.children[i].checked==true) continue;
        //noms
        for (var j=0;j<liste.length;j++){
            a=liste[j].clef;
            b=a.search("/");
            c=a.slice(b+2);//nom groupe
            if (imexZone.children[i].id==c) noexport.push(j);
        }
    }
    //ordre croissant des numéros
    noexport.sort(compareNumbers);
    //suppression des noms non cochés
    var car=0;
    for (i=0;i<noexport.length;i++){
        liste.splice(noexport[i]-car,1);//car compense la # de longueur de liste à chaque effacement
        car+=1;
    }  
    return liste;
}

//listeners export/import
function imexListen(e){
    switch(e.target.id){
        case "tous": 
            for (var i=9;i<imexZone.children.length;i+=4){ //i=8 et i+=3 si pas de bulle info
                imexZone.children[i].checked=true;
            }
        break;
        case "aucun": 
            for (var i=9;i<imexZone.children.length;i+=4){ //i=8 et i+=3 si pas de bulle info
                imexZone.children[i].checked=false;
            }
        break;
        case "export":
            var liste=Object.values(champJson);
            liste=imexCondense(liste);
            //export 
            if (liste.length){
                var abc= JSON.stringify(liste,null, ' ');
                var blob = new Blob([abc], {type: "application/json"});
                var downloadUrl = URL.createObjectURL(blob);
                download("zodiaque.json",downloadUrl);
                URL.revokeObjectURL(blob);
            }
            devalideTout(false);
            svg.style.display="";
            canvasCache(false);
            efface(imexZone);
        break;
        case "import":
            var liste=Object.values(importe);
            liste=imexCondense(liste);
            var bcd,groupe;
            //import
            if (liste.length){
                for (var i=0; i<liste.length;i++){
                    bcd=liste[i].clef.split("/*")
                    if (bcd.length > 1 && bcd[1] != ""){groupe=bcd[1];}
                    else {groupe=groupeDefaut;}
                    console.log('json - réception données ' + bcd[0] + " - groupe : "+groupe);  
                    storeNote(bcd[0],liste[i].valeur,groupe);
                }
                console.log('réception données json terminé !');
            }
            devalideTout(false);
            svg.style.display="";
            canvasCache(false);
            efface(imexZone);
            initialize(); //ne pas mettre en-dehors des {} sinon ne fonctionne pas
        break;
        case labelsGauche[6]://annuler
            devalideTout(false);
            svg.style.display="";
            canvasCache(false);
            efface(imexZone);
        break;
     }
     //types : radio, button ou checkbox = détails groupe
     switch(e.target.type){
        case "checkbox":
            if(e.target.checked==false) document.getElementById("tous").checked=false;
            if(e.target.checked==true) document.getElementById("aucun").checked=false;
        break;
     }
}

//*********** fin import/export *********************

/* Add a note to the display, and storage */
//à partir du 24/08/2020 stockage dates de naissance au format "aaaa-mm-jj"
function addNote() {
    var clef=sp(natif.nom.value);//retire les espaces de début et fin
    //vérif si nom existe déjà
    var [existe,groupe]=cgv(clef);
    if (existe==1){
        commente("red",clef + "  " + labelsGroupes[6] + " (" + groupe + ")"); //"existe déjà";
        return;
    }
    //récupération éléments de la nouvelle fiche
    var valeur=preStore();
    
    labelFiche.innerText ="";
    //autres contrôles
    if(clef !== '' && valeur[0].split('-').length===3 && valeur[0].length==10 && valeur[1].split(':').length===2 && valeur[1].length==5) {
    
        //vérifie si groupe sélectionné, sinon groupe par défaut (général)
        var groupe=groupeDefaut;
        for (i=0; i<groupesNoms.length; i++){
            if (groupesNoms[i]==groupesListe.value){
                groupe=groupesListe.value;
                break;
            }
        }
        //sauvegarde
        var noteAffiche = new Affiche( 
                clef,
                valeur,
            );
        storeNote(clef,valeur,groupe);
        clef+="/*"+groupe;
        champJson.push({clef,valeur});
        clearBtn.disabled=false;
        initialize(groupe);
        afficheTheme(noteAffiche);
        natifCopie();
        commente("green",labelsGauche[4]+"  "+natif.nom.value+" : ok");
        clearBorders();
        labelGroupes.textContent="";//efface "assigner au moins un nom au groupe"
    }else{
        if (!labelFiche.innerText) {commente("red",labelsGauche[14]);} //erreur
    }
}

function preStore(){
    var valeur=[];
    var x=Object.values(natif),y;
    for (var i=0;i<=8;i++){
        valeur[i]=x[i].value;
        valeur[i]=valeur[i].replace(/,/g," ");//supprime les ","sinon pb à l'affichage du thçme (trop de champs dans array)et utc=NaN !
    };
    //lat/long : regroupe partie entiere et decimale
    var a=[4,5,4,6,7,5];
    for (i=0;i<=3;i+=3){
        x=Number(valeur[a[i]]);
        y=Number(valeur[a[i+1]])/100;
        if (x) y*=Math.sign(x);
        var z=x+y;
        if (valeur[a[i]]=="-0") z=-z;
        valeur[a[i+2]]=z.toFixed(2);//2 décimales, très important !;
    }
    //supprime les 3 dernieres valeurs
    valeur.length-=3; 
    return valeur;    
}

/* function to store a new note in storage */
function storeNote(clef, valeur, groupe) {
    if (!groupe){groupe=groupeDefaut;}
    var storingNote = browser.storage.local.set({ ["fiche_"+ clef + "/*" + groupe]:valeur});
    storingNote.then(() => {
   //   console.log (clef + " enregistré dans " +groupe);
    }, onError);
}


/****************note display box : clef=nom+prénom, valeur=date+heure+lieu+latitude+longitude****************/
 
function displayNote(clef,valeur,gras) {
    var note = document.createElement('div');
    var noteDisplay = document.createElement('ul');
    
    //nouvel objet contenant nom + détails, associé au clic ci-dessous
    var noteAffiche = new Affiche(
        clef,
        valeur,
    );
        
    var x=document.createElement('li');
    x.textContent = clef;
    noteDisplay.appendChild(x);
    note.appendChild(noteDisplay);
    noteContainer.appendChild(note);

    //mise en gras suite update
    if (updateName==clef){
        updateName="";
        for (let i = 0; i < noteContainer.children.length; i++) {
            if (noteContainer.children[i].textContent==noteAffiche.nom){
                affiSave=noteContainer.children[i].children[0].children[0];
                affiSave.style.fontWeight="bold";
                break;
            }
        }
    }
        
    //************listeners display box**********************
    //clic sur le nom
    if (!x) return;
    x.addEventListener('click',(e) => {
        iconeGomme.click();
        //mise en "non gras" du nom précédent et en gras du nom cliqué
            if (affiSave) affiSave.style.fontWeight="normal";
            affiSave=e.target;
            e.currentTarget.style.fontWeight="bold";
            if (okSynastrie==0) clefSave=clef;
        afficheTheme(noteAffiche);
        natifCopie();
    });
}
    
function afficheTheme(noteAffiche){
    var date=noteAffiche.details[0];
    var heure=noteAffiche.details[1];
    var jj=calcJourJulien(date,heure);
    //utc,coordonnées
    utc=Number(noteAffiche.details[3]);
    //conversion deg.min en degres.decimales
    var x=Number(noteAffiche.details[4]);
    var y=Number(noteAffiche.details[5]);
    [latitude,longitude]=convDegresMinutestoDegresDecimal(x,y);
            
    //theme ou synastrie ?
        //date,heure,nom
        var a1=noteAffiche.details[0];
        var a2=noteAffiche.details[1];
        var a3=noteAffiche.nom;
        nomTheme2="";
        //synastrie
        if (okTransits==1 && okSynastrie==1){
            var dateTheme2=a1;//permet compatibilité avec ancien format date du fichier json
            heureTheme2=a2;
            nomTheme2=a3;
            affiDate=affiHeure=0; //pas d'affichage date et heure au-dessus du titre
            if (controleDate(dateTheme2)=="oui"){
                //calcul des positions planètes, mises en "live"
                calcPosPlanetes(dateTheme2,heureTheme2);
                calcPosMaisons(dateTheme2,heureTheme2); //pour AS, MC
                recupPosMaisons("natal");
                dessins();
            }
            affiDate=affiHeure=1; //réautorise affichage date et heure au-dessus du titre
            return; //sortie
        }
        //thème
        choixHeure.value=noteAffiche.details[1];
        dateNatalBis=dateNatal=a1;//permet compatibilité avec ancien format date du fichier json
        heureNatal=a2;
        nomNatal=a3;
            //titre du thème
            Lieu=noteAffiche.details[2];
            coordonnees= Lieu + " (utc : " + noteAffiche.details[3] + ", lat. " + noteAffiche.details[4].replace(".","°")+"'" + ", long. " + noteAffiche.details[5].replace(".","°")+"'" + ")";
            titreNatal=titreTheme=noteAffiche.nom +labelsDroite[8] + dateLongue(dateNatal) + " - " + heure + labelsDroite[9] + coordonnees;
            //différents paramètres
            [latNatal,longNatal,utcNatal]=[latitude,longitude,utc];
            if (okUranien==0){
                margeNoir();
                margeDiv.hidden=false;
            }
            okTransits=okProgresse=okSynastrie=okArc=okRS=okMix=0;
            fixePhases=0;
            typeRef=0;
            var a=[divLabelsProg,divChoixDonnees,tableau,infoDateNaissance,labelDateNaissance];
                for (var i of a) {i.hidden=true};
            a=[checkMaisonsNatal,optTransits,choixTypeDonnees.item(3)];//dynamique
                for (var i of a) {i.disabled=false};
            devalideNow(false);
            checkMaintenant.checked=false;
            divNatProgRS.hidden=false;
            labelDateNaissance.textContent="natal "+dateMoyenne(dateNatalBis); 
            selectTransits.value="optTheme";
            //choix données natales par défaut
            NatalExterne=0;   
            //bouton radio natal
            choix1.item(0).checked=true;
            //boutons radio tableau transits
            choix2.forEach(function(item){item.checked=false})
            canvasCache(false);
            //supprime mise à jour horloge  
            clearTimeout(timerId1);
            timerId1 = null;
            
            requetes(dateNatal,heureNatal,titreTheme);
            
            //sauvegarde position soleil natal pour calcul arc solaire
            soleilNatalBis=soleilNatal=posPlanete[0][0];
            //sauvegarde planetes et maisons natales + mise à 0 des positions externes
            for (var i=0;i<=23;i++){
                posPlaneteNatal[i]=posPlanete[0][i];
                posPlanete[1][i]=0;
                posPlanete[2][i]=0;
                retroNatal[i]=retro[0][i];
                retro[1][i]=0;
            }
            sauvePosMaisons("natal");
            sauvePosMaisons("natalbis");
            
            choixTableauDonnees.item(0).checked=true;
            if (okUranien==0) divTableaux.hidden=false;
}

// *** fin editNote ***

//valide ou dévalide entrées
function devalideTout(x){
    //cadre gauche : boutons et select des groupes
    var inputs=cadre1.getElementsByTagName("button");
    deval(inputs,x);
    inputs=cadre1.getElementsByTagName("select");
    deval(inputs,x);
    inputs=cadre1.getElementsByTagName("input");
    deval(inputs,x);
    inputs=cadre1.getElementsByTagName("li");//liste des noms
    deval(inputs,x,1);
    //cadre droit : boutons radio, liste
    inputs=cadre.getElementsByTagName("input")
    deval(inputs,x);
    inputs = cadre.getElementsByTagName("select");
    deval(inputs,x);
    //cadre centre : boutons radio
    inputs=cadre2.getElementsByTagName("input")
    deval(inputs,x);
}

function deval(inputs,x,a){
    for (var i of inputs) {
        if (!a) i.disabled = x;
        else i.setAttribute("class","inerte"+x);
    }
}

function devalideNow(x){
    var inputs=divTableaux.getElementsByTagName("input")
    deval(inputs,x);
    inputs=divNatProgRS.getElementsByTagName("input")
    deval(inputs,x);
}
function devalideSynastrie(x){
    var inputs=cadre2.getElementsByTagName("input")
    deval(inputs,x);
    labelSynastrie.textContent="  "+labelsDroite[43];//sortie : clic sur theme seul ou maintenant
    labelSynastrie.hidden=!x;
    checkMaintenant.disabled=false;
}

/* function to update notes */

function updateNote(delNote,clef,valeur,groupe,init) {
    var storingNote = browser.storage.local.set({ ["fiche_"+ clef + "/*" + groupe] : valeur});
    storingNote.then(() => {
        if(delNote !== clef) {
            removeNote("fiche_"+delNote+ "/*" + groupe);
        }
        if (init==1){
            initialize(groupe);
            var noteAffiche = new Affiche(
                clef,
                valeur,
            );
             afficheTheme(noteAffiche);
        }
    }, onError);
}

function removeNote(delNote){
    var removingNote = browser.storage.local.remove(delNote);
    removingNote.then(() => {
        console.log("effacement : " + delNote);
    }, onError);
} 

/* Clear all notes from the display/storage */

function clearAll() { 
    Promise.resolve().then(() => {//évite sur chrome message : [violation] click handler took xx msec
        var abc=labelsGauche[10];
        if (window.confirm(abc)) {
            var gettingItem = browser.storage.local.get(null);
            gettingItem.then((results) => {
                var clefs = Object.keys(results);
                if (clefs.length){
                    for (let clef of clefs) {
                        if (clef.search("fiche_")!=-1){
                            var removingNote = browser.storage.local.remove(clef);
                            removingNote.then(() => {
                            }, onError);
                        }   
                    }
                }
                initialize();
            }, onError);
        }
    });
}
