//**************************gestion souris**************************

//**********survol souris sur canvas : affichage infos sur maisons, signes, planetes
let rouelibre=0;

function getMousePos(z, evt) {
    var rect = z.getBoundingClientRect();
    return {
        x: evt.clientX - rect.left,
        y: evt.clientY - rect.top
    };
}

canvasTarget.addEventListener('click', function() {//ne marche pas avec svg.addEvent...
    if (okUranien) return;
    fixePhases=0;
    canvasPhases.hidden=true;
    svg.style.display="";
    margeNoir();
    if (!okTransits) reaffiche();
}, false);

//réaffichage thème ou maintenant
function reaffiche(){
     //supprime mise à jour horloge si active
    if (timerId1!=null){
        clearTimeout(timerId1);
        timerId1 = null;
    }
    if (clefSave){
        var event = new Event('click');
        affiSave.dispatchEvent(event);
    }
    else{
        checkMaintenant.checked=true;
        var event = new Event('change');
        checkMaintenant.dispatchEvent(event);
    }
}

function croixListener(evt){
    var mousePos = getMousePos(svg, evt);
    var X=mousePos.x,
        Y=mousePos.y;
    if (rouelibre==1) urCroix(-1,0,X,Y);
}

function svgListenerOut(){
    displayDivInfo();
    var z=svg.querySelectorAll(".liste2");
    //démasquage de tous les aspects
    z=svg.querySelectorAll(".aspects");
    z.forEach((item) => {
        item.style.display="";
    })
}

function svgListener(e){
 //   console.log("id ",e.target.id);
    var a=e.target.id;
    var b=a.search("_");
    if (b<0) return;
    var c=a.slice(0,b);//maison,signe,planete
    switch (c){
        case "maison":
            var i=Number(a.slice(b+1));
            var message="";
            var abc=convPos2DegSigne(posMaison[i]);
            message=labelsDroite[42] + " " + AtoR( i+1 )+"  "+abc.degres+" "+Signes[abc.signe];
            //gouverneurs
            var l=browser.i18n.getMessage("dignites").split(",");
            abc=l[7];//gouverneurs
                //gouverneurs au singulier si un seul
                if(gouverneurs[i].length==1){
                    var index=abc.search("s");
                    abc=abc.slice(0,index)+abc.slice(index+1);
                }
            message+="_"+abc;
            for (var j=0;j<gouverneurs[i].length;j++){
                message+=Planetes[gouverneurs[i][j]];
                message+=" ";
            }
            displayDivInfo(message,e.pageX,e.pageY-20);
            break;
        case "signe":
            var i=Number(a.slice(b+1));
            var message=Signes[i]+"_";
            var l=browser.i18n.getMessage("dignites").split(",");
            //dignités
            var d=[pMaitre,,pExil,pChute,pExaltation];
            for (var k=0;k<=4;k++){
                if (k==1) continue;
                message+=l[k]+" : ";
                for (var m=0;m<d[k][i].length;m++){
                    message+=Planetes[d[k][i][m]];
                    if (d[k][i].length>1 && m<1){message+=", "}
                }
                message+="_";//LF
            }
            displayDivInfo(message,e.pageX-10,e.pageY-100);
            break;
        case "planete0":
        case "planete1":
        case "planete2":
            var i=Number(a.slice(b+1));
            var j=Number(a.slice(7,8));//0,1 ou 2
            if (okUranien) {survolUranien(i,j);break};
            //positions à gauche
            var [message,rl]=messagePosPlanete(i,j);
            //aspects à gauche sauf en domitude
            if (!checkDom.checked){;
                message+="_";//LF
                message+=messageAspects(i,listAspects[j]);
            }
            //affichage
            textMultiLine(message, 10, 30);
            //masque aspects non liés à la planète
            var abc="_"+i+"-";
            //liste aspects à droite
            if (j==0){
                if (tm()==0 && !checkDom.checked) tabAspects(i,0);
                if (tm()!=0)  abc="-"+i;//planète du thème avec transits actifs
            }
            a=svg.querySelectorAll(".aspects");
            a.forEach((item) => {
               if (item.id.search(abc)==-1) item.style.display="none";
            })
            break;
    }
}

//uranien : recherche mi-points et tracé croix
function survolUranien(ref,tt){
    if (ref==11) return; //lilith exclue
    planeteDefaut=ref;
    if (!MP && tt<=2) uranienAspects(ref,tt,1);//ne pas déplacer sous affichage maison !
    urCroix(ref,tt);
    //affichage secteurs maisons ?
    if (tt==0 && roueAngle==360 && checkMaisons.checked==true) dessins();
}

function messagePosPlanete(i,type,X,Y){//i:planete ou -1 si roue libre, type 0:interne, 1 et 2:externe
    var abc,message="",rl=0;
    if (i>=0) {
        if (type){
            //theme,transit,arc,rs,progresse,synastrie
            var m=["","Transit",labelsUranien[3],labelsCentre[20],labelsDroite[34],labelsDroite[30]];
            var a=selectTransits.selectedOptions;
            var j;
            //1er,2eme transit ou theme
            if (type) j=a.item(type-1).index;
            else j=a.item(0).index;
            message=m[j];
            message+=" ";
        }
        //position en signe
        abc=convPos2DegSigne(posPlanete[type][i]);
        message+=Planetes[i];
        if (retro[type][i]) message+=" [R]";
        if (i==0)message+=" "+abc.secondes+" "+Signes[abc.signe]; //soleil
        else message+=" "+abc.degres+" "+Signes[abc.signe];
        //position en maison
        if (type==0 && !okUranien){
            abc= checkDom.checked ? convPos2DegMaison(posDom[type][i]) : convPos2DegMaison(posPlanete[type][i]);
            message+=" ("+abc.degres+" "+labelsDroite[42]+ " "+AtoR(abc.maison)+")";
        }
        
    //roue libre : indication des degrés de la croix (4 angles)
    }else if (i==-1){
        var y=[0,roueAngle/2,roueAngle/4,roueAngle*3/4],//4 angles de la croix
        angle;
        message="";
        y.forEach(function(item){
            angle=Math.atan2((centre[1]-Y),(X-centre[0]));
            if (roueIndice>1) angle-=Math.PI/2;
            angle=angle*180/Math.PI/roueIndice;
            angle-=item;
            angle=r360(angle,roueAngle);
            if (item==0) rl=angle;
            message+=cDdDm(angle)+"  ";
        });
    }
    return [message,rl];//rl:position roue libre
}

function messageAspects(i,l){//i:planete, l:liste des aspects
    var message="",x=0;
    var liste=l.planete[i].split(",");
    var aspects=l.aspect[i].split(","); 
    var orbe=l.orbe[i].split(",");
    for (var k=1;k<liste.length;k++){
        if ((liste[k]==13 || liste[k]==14) && checkAsMc.checked==false) continue; //AS-MC
            //ajout d'un interligne en cas de double transits
            if (liste[k]/200>=1 && x==0) {message+="+";x=1};
        message+=Aspects[aspects[k]];
        message+=" "+Planetes[liste[k]%100];//%100 pour double transits
        message+=" "+ convPos2DegSigne(orbe[k]).degres;
        message+="_";//LF
    }   
    return message;
}

function textMultiLine(text, x, y) {
    svgEfface(".liste2");
    var fonte=0.9+"vw serif";
    var lineHeight = 20;
    var lines = text.split("_");
    //ecriture texte
    for (var i = 0; i < lines.length; ++i) {
        svgTexte("liste2","liste2",lines[i], x, y,fonte,"blue");
        y += lineHeight;
    }
}

//**********survols souris ********
function survol(objet,texte,x,y){
    if (!x) {x=10;y=-10};
    objet.onmouseover=function(e){
       //  if (!android)
             displayDivInfo(texte,e.pageX+x,e.pageY+y);
    }
    objet.onmouseout=function(){
        displayDivInfo();
    }
}
survol(autoSet,labelsGauche[20]+"_"+labelsGauche[22]+"_"+labelsGauche[23],20,0);
survol(infoUtc,labelsGauche[17]);
survol(infoLatitude,"latitude deg.min "+labelsGauche[13]);
survol(infoLongitude,"longitude deg.min "+labelsGauche[12]);
survol(iconeAdd,labelsGauche[4],-10,-60);
survol(iconeUpdate,labelsGauche[7],-10,-60);
survol(iconeDelete,labelsGauche[8],-10,-60);
survol(iconeGomme,labelsGauche[24],-10,-60);

survol(infoNow,labelsCentre[33]);
survol(infoMaisons,labelsCentre[34]);
survol(infoDom,labelsCentre[35]);
survol(infoTransits,labelsDroite[28],-100,10);
survol(infoSynastrie,labelsDroite[32]+"_"+labelsDroite[43],-100,10);
survol(infoWiki,labelsGauche[18]);
survol(infoDateNaissance,labelsDroite[33]);
survol(labelDonneesPositions,labelsDominantes[25]);
survol(labeldonneesDominantes,labelsDominantes[26]);
survol(labeldonneesChaine,labelsDominantes[27]);
survol(labeldynamique,labelsDominantes[30]+" + audio");

infoFormule.onmouseover=function(e){
    var message,m1,m2,m3="",l;
    //1ère ligne 
    if (MP==0) m1=labelsUranien[12]+" : a_";//passer la souris sur une planète du thème pour définir
    else m1=labelsUranien[8];//cliquer sur planètes du bandeau de droite pour sélectionner/désélectionner
    if (MP==1) m1+=" : a,b <min=1, max=2>";
    else if (MP==2) m1+=" : a,b,c <min/max=3>";
    l=m1.length;
    //2ème ligne : ou sur le pourtour du cercle pour faire tourner la croix
    m2="";
    if (MP==0) m2=labelsUranien[18];
    if (l<m2.length) l=m2.length;
    //3ème ligne : cliquer sur le 1er icône du bandeau pour inverser les états interne/externe
    if (okTransits){
        if (MP==0) m3="";
        else m3 =labelsUranien[14];
        if (l<m3.length) l=m3.length;
    }
    message=m1+"_"+m2+"_"+m3;
    displayDivInfo(message,e.pageX-3*l,e.pageY+20);
}
infoFormule.onmouseout=function(){
     displayDivInfo();
}

function displayDivInfo(text,x,y,titre){
    while (document.getElementById('divInfo')) {
       garbage=document.body.removeChild(document.getElementById('divInfo'));
    }
    if(text){
        var divInfo = document.createElement('div');
        divInfo.id = 'divInfo';
        divInfo.style.position = 'absolute';
        divInfo.style.left = x+20+'px';
        divInfo.style.top = y+'px';
        divInfo.style.background ="yellow";//"lightgoldenrodyellow"//jaune clair
        divInfo.style.color="blue";
        divInfo.style.font = 0.9+"vw serif";
        divInfo.style.border="1px dashed blue";
        divInfo.align="center";
        var abc=text.split("_");//séparateur de saut de ligne
        var h=abc.length;
        var element;
        //définit haut du texte si titre présent et si plusieurs lignes (sert pour les éléments en tropical)
        if (h>1 && titre){
            var z=y-15*(h+1);//haut du texte
            if (z<0) z=0;
            divInfo.style.top = z+'px';
            element=document.createElement('p');
            element.textContent=titre;
            element.style.fontStyle="italic"
            divInfo.appendChild(element);
        }
        //écrit message
        for (var i=0;i<h;i++) {
            if (!titre && i==0) {
                element=document.createElement('p');
                divInfo.appendChild(element);
            }
            element=document.createElement('div');
            if (abc[i]) element.textContent="\u00A0"+abc[i]+"\u00A0";//ajoute un espace en début et fin
            //si tableau des elements, mise en gras des signes du thème (avec () à la fin)
            if (abc[i].search(/\(/)>=0 && titre) element.style.fontWeight="bold";
            divInfo.appendChild(element);
        }
        element=document.createElement('p');
        divInfo.appendChild(element);
        document.body.appendChild(divInfo);
    }
}
