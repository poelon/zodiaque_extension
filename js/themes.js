
function calcPosPlanetes(date,heure,arc){
    
    var reponse="non, erreur de date",
        message="",
        h=heure.split(':'),
        heuredecimale=Number(h[0])+ Number(h[1])/60-utc,
        lPlan1,
        lPlan2,
        ecart;
        //secondes le cas échéant (RS en test)
        if (h.length>2)heuredecimale+=Number(h[2])/3600;
        
     //équation du temps : adaptation heure progressée (ex thème perso sur echelle humaine : +19' en 2016 sur heure de naissance)
    if (checkEquationTemps.checked==true && checkEquationTemps.hidden==false){
        heure0ref=heuredecimale;
        equationProg="";
        heuredecimale+=equationTemps(date)-equationTemps(dateNatal);
        equationProg=String(Math.round((heuredecimale-heure0ref)*60)) + "'";
    }
    
    //vérifie si format date correct (aaaa-mm-jj)
    if (date.split('-').length!=3) return reponse;
        //jour julien (les calculs se font à utc=0) 03/2019 : correction (-utc supprimé) sinon sauts lune entre 00:00 et 01:00
        var jj=calcJourJulien(date,heuredecimale+utc);//+utc sinon saut lune entre 00:00 et 01:00
        var jjRS=calcJourJulien(date,heuredecimale);//pas d'utc sinon valeur soleil décalée !
        var an=date.split('-')[0];
        var mois=Number(date.split('-')[1]);
        var jour=Number(date.split('-')[2]);
    
    //détermination jour de l'an chinois (NL entre 21 janvier et 20 février)
    if (okTransits==0 && okUranien==0 && choix1.item(0).checked==true){
        var jj1=calcJourJulien(an+"-01-21","00:00");
        var soleil,lune,jourAn,jourTheme;
        for (var i=0; i<=60; i++){
            //[soleil,xs,ys]=calcSoleil(Number(an),heuredecimale,jj1);
            soleil=calcSoleilRS(jj1);
            lune=calcPlanetes(Number(an),heuredecimale,jj1,1);
            if (Math.abs(soleil-lune)<=12) {
                jourAn=21+i;
                break;
            }
            jj1+=1;
        }
        anChinois=Number(an);
        jourTheme=100;
        if (mois==1) jourTheme=jour;
        else if (mois==2)jourTheme=31+jour;
        //avant jour de l'an chinois, signe de l'année précédente
        if (jourTheme <= jourAn) anChinois-=1;
    }  
    
    //calcul positions planètes
   // for (i=0; i<=maxPl; i++){
    for (i=0; i<=23; i++){//23 sinon pb de mipoints en transits uranien car manque des planetes dans posplanetenatal
        //Soleil
        if (i==0){
            lPlan1=calcPlanetes(Number(an),heuredecimale,jj,i);//pour calculs RA et dsa (domitude)
            lPlan2=calcSoleilRS(jjRS+1);
            lPlan1=calcSoleilRS(jjRS);
        }
        //Lune à Neptune 1-8 + 8 planètes uranien
        else if ((i>=1 && i <=8) || (i>=15 && i<=22)){
            lPlan2=calcPlanetes(Number(an),heuredecimale,jj+1,i);//laisser lplan2 avant lplan1 sinon décalages en domitude
            lPlan1=calcPlanetes(Number(an),heuredecimale,jj,i);
        }
        //Pluton
        else if (i==9){
            lPlan2=calcPluton(Number(an),heuredecimale,jj+1);
            lPlan1=calcPluton(Number(an),heuredecimale,jj);
        }
        //NN et Lilith vrais
        else if (i==10 || i==11){
            lPlan2=calcNNLilith(jj+1,i);
            lPlan1=calcNNLilith(jj,i);
        }
        //NS
        else if (i==12){
            lPlan2=lPlan1=r360(posPlanete[tm()][10]+180);
        }
        //point vernal
        else if (i==23){
            lPlan2=lPlan1=0;
        }
        ecart=reboucle(lPlan2-lPlan1);
        //arc solaire
        if (arc && i==0) var [soleilArc,ecartArc]= [lPlan1,ecart/365.25]
        //rétrograde si écart négatif
        ecartJour[i]=ecart;
        ecartJour[i]<0 ? retro[tm()][i]=1 : retro[tm()][i]=0;
        //sauvegarde position
        posPlanete[tm()][i]=lPlan1;
    }
    if (arc) return [soleilArc,ecartArc];
    reponse= "oui";        
    return reponse;
}

//mp[0]=[0+0])/2, mp[1]=[1+1])/2 mp[2]=[1+2])/2 
function calcMiPoints(){
var pos1,pos2;
    for (var x=0;x<=tm();x++){
        miPoint[x]=[];
        for (var i=0; i<=maxPl; i++){
            if (isNaN(posPlanete[x][i])) continue;
            pos1=posPlanete[x&&1][i]%roueAngle;//limite à 1 en double transit
            miPoint[x][i]=[];
            for (var j=0; j<=maxPl; j++){
                if (isNaN(posPlanete[x][j])) continue;
                pos2=posPlanete[x][j]%roueAngle;
                miPoint[x][i][j]=(pos1+pos2)/2;
            }
        }
    }
}

//formule[0]=[0+0-0], formule[1]=[1+1-1]
function calcFormules(){
var pos1,pos2,pos3;
    for (var x=0;x<=okTransits;x++){
        formule[x]=[];
        for (var i=0; i<=maxPl; i++){
            if (isNaN(posPlanete[x][i])) continue;
            pos1=posPlanete[x][i]%roueAngle;
            formule[x][i]=[];
            for (var j=0; j<=maxPl; j++){
                if (isNaN(posPlanete[x][j])) continue;
                pos2=posPlanete[x][j]%roueAngle;
                formule[x][i][j]=[];
                for (var k=0; k<=maxPl; k++){
                    if (isNaN(posPlanete[x][k])) continue;
                    pos3=posPlanete[x][k]%roueAngle;
                    formule[x][i][j][k]=r360(pos1+pos2-pos3,roueAngle);
                }
            }
        }
    }
}


//************************************ recherche Maisons *************************************************

//source : https://www.scribd.com/doc/6495552/An-Astrological-House-Formulary
function calcPosMaisons(date,heure,jours,traite){ //traite=traite maisons progressées
    var heure0ref,
        heure0=Number(heure.split(':')[0])+ (Number(heure.split(':')[1])/60); //hh+mm en nombre décimal
        
    //équation du temps : adaptation heure progressée (ex thème perso sur echelle humaine : +19' en 2016 sur heure de naissance)
 //   if (jours>=0 && checkEquationTemps.checked==true && checkMaisonsNatal.checked==false){
    if (jours>=0 && checkEquationTemps.checked==true){
        heure0ref=heure0;
        equationProg="";
        heure0+=equationTemps(date)-equationTemps(dateNatal);  //heure0+=age*0.30645/60;
        if (traite) equationProg=String(Math.round((heure0-heure0ref)*60)) + "'"
    }
                                   
    var an=Number(date.split('-')[0]),
        mois=Number(date.split('-')[1]),
        jour=Number(date.split('-')[2]);
    var T=((an+(mois*30.4375+jour)/365.25)-1900)/100,
        T2=T*T,
        T3=T2*T;
    /*calcul de l'obliquité : e = 23o 27' 08.26" - 46.845" x T - .0059" x T2 + .00181" x T3
    where T is in fractions of a century starting from Jan 1, 1900*/  
    var e=23+(27/60)+(8.26/3600)-(46.845/3600*T)-(0.0059/3600*T2)+(0.00181/3600*T3); 
    ecl=e;
    //temps sidéral en nombre décimal (hh.mm) =valeur à ajouter ou retrancher à l'heure solaire
    var heureSideraleNumerique=siderale(heure0,longitude,date);
    //temps sidéral en degrés (15 deg/heure)
    var ramc=heureSideraleNumerique*15;
    
    //AS
    var a1,a2,a120,b1,b2,b120,x,y,AS;
    a1=Math.sin(d2r*ramc);
    a2=Math.cos(d2r*e); 
    a120=a1*a2;
    b1=Math.tan(d2r*latitude);
    b2=Math.sin(d2r*e);
    b120=b1*b2;
    x=a120+b120
    y=-Math.cos(d2r*ramc);
    AS=Math.atan2(y,x)*180/Math.PI;
        //correction
        AS=reboucle180(AS);
        //ecart annuel en progressé, pas utilisé en natal
        ecartProg=r360(posMaison[0]-AS);
        //sauvegarde (laisser avant sortie ci-dessous sinon pb AS en transits progressé)
        posMaison[0]=AS;
        posMaison[12]=AS;
        posPlanete[tm()][13]=AS;
        
    //sortie si 1er passage en progressé (pour déterminer l'écart ascendant sur 1 an) 
    if (jours>=0 && !traite)return;
    
        //MC
        y=Math.tan(d2r*ramc);
        x=Math.cos(d2r*e);
        var MC=Math.atan2(y,x)*180/Math.PI;
            //corrections
            if (ramc>90 && ramc<=270){MC+=180;}
            else if(ramc>270 && ramc<=360){MC+=360;}
            //sauvegarde
            MC=r360(MC);
            posMaison[9]=MC;
            posPlanete[tm()][14]=MC;
        
        //Maisons 11,12,2,3
        var p=[10,11,1,2,30,60,120,150,1/3,2/3,2/3,1/3];
        var d11;
        var a11,m11,r11,h11,b1,b2;
        var f=latitude;
        for (var i=0; i<=3;i++){
            //cuspal declinations
            d11=Math.asin(Math.sin(d2r*e)*Math.sin(d2r*(ramc+p[i+4])))*180/Math.PI;
    
            //intermediate values
            b1=Math.tan(d2r*f);
            b2=Math.tan(d2r*d11);
            x=b1*b2;
            a11=p[i+8]*Math.asin(x)*180/Math.PI;

            h11=r360(ramc+p[i+4]);
            b1=Math.cos(d2r*(h11));
            b2=Math.tan(d2r*d11);
            x=b1*b2;
            y=Math.sin(d2r*a11);            
            m11=Math.atan2(y,x)*180/Math.PI;

            y=Math.tan(d2r*(h11))*Math.cos(d2r*m11);
            x=Math.cos(d2r*(m11+e));          
            r11=Math.atan2(y,x)*180/Math.PI; 
                
            //corrections
            if (h11>=180){r11+=180;}
            r11=r360(r11);
            //sauvegarde
            posMaison[p[i]]=r11;
        }   
            
            //Maisons complémentaires 4,5,6,7,8,9
            var pos=[3,4,5,6,7,8];
            var offset=6;
            for (var j=0; j<6; j++){
                if (j>=3) {offset=-6;}   
                posMaison[pos[j]]=posMaison[pos[j]+offset]+180;
                if (posMaison[pos[j]]>360){posMaison[pos[j]]-=360;} 
            }
        
    //calcule position Maisons Progressées au jour exact
        if (traite){
            for (i=0; i<=12 ; i++){
                //ecartProg=écart annuel
                posMaison[i]=posMaison[i]+(jours*ecartProg/365);
                posMaison[i]=r360(posMaison[i]);
            }
        }
    
    //paramètre asc (en double, laisser ici après modif progressé sortie en uranien)
            if (okTransits==0){
            // checkMaisons.checked==true ? asc=posMaison[0] : asc=ascDef //180;
                asc=posMaison[0];
                habiteMaisons();
                gouvMaisons();
            }
            if (okUranien==1) asc=ascDef;//sinon décalage planètes si maisons cochées (roue 360)
    
    
    
  //**** domitude ****** (sur tropical natal seulement)
    //source: http://mapage.noos.fr/astrolabe/latitude.htm#Domitude
    if (checkDom.checked) {
        var nsa,H,cadran,pos0,pos1,posx,dom;
        //planètes
        for (var i=0; i<=maxPl;i++){
            if (posPlanete[tm()][i]==undefined || posPlanete[tm()][i]==0) continue;
            //détermination du cadran (as-mc mc-ds as-fc fc-ds)
            for (var j=0;j<=12;j+=3){
                pos0=posMaison[j];
                pos1=posMaison[j+3];
                posx=posPlanete[tm()][i];
                if (pos0>pos1){
                    if (posx>pos0){
                        posx-=360;
                    }
                    pos0-=360;
                }
                if (posx>=pos0 && posx<pos1){
                    cadran=j/3;
                    //console.log(i,posPlanete[0][i],posMaison[j],posMaison[j+3],cadran);
                    break;
                }
            }
            //posDom=nouvelles positions planetes
            nsa=r360(180-dsa[i]);//nocturne semi-arc
            H=r360(ramc-RA[i]);//angle horaire
            //correction bug : saut au changement de cadran MC
            if (H<3 && cadran==3) cadran=2;
            if (H>357 && cadran==2) cadran=3;
            //if (i==1) console.log(i,posPlanete[0][i],posMaison[j],posMaison[j+3],cadran,H);
            
            switch(cadran){
                case 3://AS-MC
                    dom=r360(270 + 90 * (360 - H) / dsa[i]);//SAD
                    break;
                case 2://MC-DS
                    dom=r360(270 - 90 * H / dsa[i]);//SAD
                    break;
                case 1://FC-DS
                case 0://AS-FC
                    dom=r360(90 + 90 * (180 - H) / nsa);//SAN
                    break;
            }
            posDom[tm()][i]= !isNaN(dom) ? r360(dom+posMaison[0]) : posPlanete[tm()][i];
        }
        //maisons tous les 30deg.
        if (!okTransits || okSynastrie){
            sauvePosMaisons("save");
            for (i=1;i<=11;i++){
                posMaison[i]=r360(posMaison[i-1]+30);
            }
            //MC=posDom[14]
            posDom[tm()][14]=posMaison[9];
            habiteMaisons();
        }
    }
}

function habiteMaisons(){
    //maisons habitées
    var k,m1,m2,a1,a2,offset,position;

     for (var i=0; i<=12;i++){
         planeteHabite[i]=100; //sert pour maisons interceptées
         for (var j=0; j<=11;j++){
            k=j+1;
            if (k>11) k=0;
            m1=posMaison[j];
            m2=posMaison[k];
            //passage à 0 ?
            [a1,a2,offset]= testBoucle(m1,m2);
            position= checkDom.checked ? posDom[0][i] : posPlanete[0][i];
            if ((position>a1 && position<a2) || (position+offset>a1 && position+offset<a2)){
                planeteHabite[i]=j;
                break;
            }
        }
     }
}

function gouvMaisons(){
     //maisons gouvernées
     //signes en fonction des planètes; ex g[0]=soleil=4 (lion), g[2]=mercure=2 (gémeaux) et 5 (vierge)
    var g=[[4,4],[3,3],[2,5],[1,6],[0,7],[8,11],[9,10],[9,10],[8,11],[0,7]];
    var a;
    planeteGouverne=[[],[],[],[],[],[],[],[],[],[]]; //définie en global
    for (i=0; i<=11;i++){
        a=convPos2DegSigne(posMaison[i]).signe;
        for (j=0; j<=9;j++){
            if (a==g[j][0] || a==g[j][1]){
                k=planeteGouverne[j].length;
                planeteGouverne[j][k]=i;
            }
        }
    }
    
    //maisons interceptées
    var b,
        min,
        max,
        l;
    for (i=0; i<=9;i++){
        a=2;
        if (i<=1){a=1}; //évite doublons pour soleil et lune
        b=planeteGouverne[i].length;
        for (j=0;j<a;j++){
            min=30*g[i][j];
            max=min+30;
            for (k=0;k<=11;k++){
                l=k+1;
                if (l>11) l=0;
                m1=posMaison[k];
                m2=posMaison[l];
                //passage à 0 ?
                [a1,a2,offset]= testBoucle(m1,m2);
                if ((a1<min && a2>max) || (a1<min+offset && a2>max+offset)){
                    planeteGouverne[i][b]=k+100; //100 permet la détection pour écrire en mauve dans le tableau
                    b+=1; //plusieurs maisons interceptées possibles
                    break; 
                }
            } 
        }                
    }
    //gouverneurs maisons
    gouverneurs=[[],[],[],[],[],[],[],[],[],[],[],[]];
    for (i=0; i<=9;i++){
        b=planeteGouverne[i].length;
        for (j=0;j<b;j++){
            a=planeteGouverne[i][j];
            if (a>=100) a-=100;
            if (gouverneurs[a].includes(i)==false) gouverneurs[a].push(i);
        }
    }
}

function testBoucle(a1,a2){
    var offset=0;
    if (Math.abs(a1-a2)>300){
        offset=360;
        a1==Math.min(a1,a2) ? a1+=360 : a2+=360;
    }
    return [a1,a2,offset];
}

function sauvePosMaisons(type){
    for (var i=0;i<=12;i++){
        switch(type){
            case "natal":
                posMaisonNatal[i]=posMaison[i];
                break;
            case "natalbis":
                posMaisonNatalBis[i]=posMaison[i];
                break;
            case "progresse":
                posMaisonProgresse[i]=posMaison[i];
                if (i==0) posPlanete[tm()][13]=posMaison[0];//AS
                if (i==9) posPlanete[tm()][14]=posMaison[9];//MC
                break;
            case "save":
                posMaisonSave[i]=posMaison[i];
                break;
        }
    }
}

function recupPosMaisons(type){
    for (var i=0;i<=12;i++){
        switch(type){
            case "natal":
                posMaison[i]=posMaisonNatal[i];
                break;
            case "natalbis":
                posMaison[i]=posMaisonNatalBis[i];
                break;
            case "progresse":
                posMaison[i]=posMaisonProgresse[i];
                if (i==0) posPlanete[tm()][13]=posMaison[0];//AS
                if (i==9) posPlanete[tm()][14]=posMaison[9];//MC
                break;
            case "save":
                posMaison[i]=posMaisonSave[i];
                break;
        }
    }
    if (okUranien==0) asc=posMaison[0];
    habiteMaisons();
    gouvMaisons();
}

function resetPosMaisonsNatalbis(){
    for (var i=0;i<=12;i++){
        posMaisonNatalBis[i]=posMaisonNatal[i];
    }
}
//************************************ sous-routines ***************************************    

function siderale(heure,longitude,date){ 
   var  refTime=new Date('2000-01-01'),
        endTime=new Date(date),
        // intervalle en ms depuis ou avant le 1/1/2000
        timeDiff = endTime - refTime, 
        //conversion en jours (avec décimales)
        daysDiff = timeDiff / (1000 * 60 * 60 * 24),
        offsetSiderale;
    //ajout heure de naissance et -0.5 car référence greenwich prise à 12h
    daysDiff+=((heure-utc)/24)-0.5;
    //calcul temps sidéral
    offsetSiderale=((daysDiff*24.065709824419)+18.697374558+(longitude/15))% 24; //%=modulo, 18.697=heure sidérale du 1/01/2000 à 12h TU
    if (offsetSiderale<0){offsetSiderale +=24} 
    return offsetSiderale;
}  

//utilisée pour une différence entre 2 positions à cheval sur 360 (ou l'angle max de la roue en uranien)
function reboucle(valeur,angle){
    if (!angle) angle=360;
    var ecart=angle*5/6;
    if (valeur<-ecart){valeur+=angle}
    if (valeur>ecart){valeur-=angle} 
    return valeur;
}

//utilisée pour remettre une position entre 0 et 360 (ou l'angle max de la roue en uranien)
function r360(valeur,angle){
    if (!angle) angle=360;
    valeur=valeur%angle;
    if (valeur<0){valeur+=angle}
    if (valeur>angle){valeur-=angle} 
    return valeur;
}

//spécifique pour l'ascendant
function reboucle180(valeur){
    if (Number.isNaN(valeur)==false){
        valeur<180 ? valeur+=180 : valeur-=180; 
    }else{console.log("erreur NaN ");}
    return valeur;
}

//*********************************** progressé ********************************************************

function parametresProgresse(date,datetransit,anneetransit){
     //2 formats de date : new Date("aaaa-mm-jj") ou new Date("aaaa","m-1","j+1")
    //determination de l'âge
    var refTime=new Date(date),//date de naissance (aaaa,mm,jj)
        endTime=new Date(datetransit), // date affichée (aaaa-mm-jj)  
        timeDiff = endTime - refTime, // intervalle en ms
        daysDiff = Math.floor(timeDiff / (1000 * 60 * 60 * 24)); //intervalle en jours
    var age=Math.trunc(daysDiff/365.25); //partie entière
       
    //date anniversaire année transit
    refTime=new Date(anneetransit+"-"+date.slice(5,7)+"-"+date.slice(8));
    //la ligne d'éphémérides commence au jour d'anniversaire de l'année progressée
    if (refTime>=endTime){
        refTime=new Date(anneetransit-1+"-"+date.slice(5,7)+"-"+date.slice(8));
    }
    timeDiff=endTime-refTime;
    //jours à ajouter ou retrancher dans l'année progressée
    var joursProgresse=Math.floor(timeDiff / (1000 * 60 * 60 * 24));
    //remise à 0 des jours progressé au changement d'âge
    if (age>Math.trunc((daysDiff-1)/365.25)) joursProgresse=0;
    //jours à ajouter au jour anniversaire pour obtenir le 1er janvier de l'année progressée
    //attention, le changement d'âge fait avancer d'une ligne éphémérides
    var jours=Number(date.slice(8))+age; 
    //date à chercher dans les éphémérides (1er janvier de l'année progressée) =date anniversaire + age exprimé en jours
    refTime=new Date(date.slice(0,4),date.slice(5,7)-1,String(jours+1));
        
     return [joursProgresse,refTime,daysDiff/365.25];
}

function rechercheThemeProgresse(date,arc){
    var heure;
    if (okMix) heure=heureNatal;
    else heure=choixHeure.value;
    
    //récupère nombre de jours entre date anniversaire précédent et date en cours + date correspondant au 1er janvier de l'année progressée
    annee=dateMaisons.slice(0,4);//ajouté pour arc solaire
    var [jours,date1Janvier,age]=parametresProgresse(date,dateMaisons,annee) //dateMaisons sinon pb avec ancien format de date
    //sort si erreur (ex date > date actuelle)
    if (isNaN(jours)) return;
    
    //récupère positions des planètes au 1er janvier de l'année progressée (avec date format string jj/mm/aaaa)
    var jour=ajoutZero(String(date1Janvier.getUTCDate()));
    var mois=ajoutZero(String(date1Janvier.getUTCMonth() +1));
    var an=String(date1Janvier.getUTCFullYear());
    var [soleil,ecart]=calcPosPlanetes(an + "-" + mois + "-" + jour,heure,arc);
    //calcul soleil progressé du jour pour arc solaire
    if (arc){
        soleilProgresse=r360(soleil+jours*ecart);
        return;
    }
    //positions planètes et maisons progressées au jour exact
    traitePlaneteProgresse(jours);
    
    var texte1=" ("+labelsDroite[16] //heure de naissance;
    if (heureNatal !==heure){
       texte1+=labelsDroite[17] //" modifiée ";
    }
    
    var texte2= " - "+labelsDroite[13]+" - ";//thème progressé
        //maisons natales sauf transits
        if (checkMaisonsNatal.checked==true && okTransits==0){
            jours=0; //pas de correction heure progressée (equation du temps)
            var type="natal";
            if (okUranien==0) texte2+=labelsDroite[14];
            var dateMaisonsProgresse=dateNatal;
        }
        //maisons progressées
        else{
            var type="progresse";
            var dateMaisonsProgresse=String(date1Janvier.getUTCFullYear())+"-"+ajoutZero(String(date1Janvier.getUTCMonth()+1))+"-"+ajoutZero(String(date1Janvier.getUTCDate()));
            //recherche position ascendant à an+1 pour déterminer l'écart annuel ecartProg
            ascProgPlus1(heure,date1Janvier,jours);
            if (okUranien==0) texte2+=labelsDroite[15];
        }
    //laisser ici, valable aussi pour maisons natales (et uranien pour AS/MC !)
    calcPosMaisons(dateMaisonsProgresse,heure,jours,1); //1=traite maisons progressées jour par jour avec ecartProg
    if (type=="progresse") sauvePosMaisons(type); 
    
    //titre
    if (okTransits==1 || arc==1) return;
    titreTheme=nomNatal+texte2+dateLongue(dateMaisons)+texte1+choixHeure.value +")";
     if (checkEquationTemps.checked==true){
            titreTheme+=" ("+labelsCentre[15]+ " : "+equationProg+")";
        }
}

function traitePlaneteProgresse(jours){
    var ecart;
    for (var i=0; i<=maxPl ; i++){
        if (isNaN(posPlanete[tm()][i])) continue;
        //ecartJour=écart annuel
        ecart=ecartJour[i]/365.25;
        posPlanete[tm()][i]=r360(posPlanete[tm()][i]+jours*ecart); //+(ecartJour[i]/720) ; 1/2jour enlevé car décale les transits !
    }
}

function ascProgPlus1(heure,date,jours){
    //recherche position ascendant à an+1 pour déterminer l'écart annuel ecartProg
    var date1=new Date(String(date.getUTCFullYear()),String(date.getUTCMonth()),String(date.getUTCDate()+2));
    var dateMaisonsProgressePlus1=String(date1.getUTCFullYear())+"-"+ajoutZero(String(date1.getUTCMonth()+1))+"-"+ajoutZero(String(date1.getUTCDate()));
    calcPosMaisons(dateMaisonsProgressePlus1,heure,jours);
}

//********************************************* gestion thème/transits ***************************************************//

function theme(){
    if (resetNatal()==0) return;
            choixDate.value=dateNatal;
            choixHeure.value=heureNatal;
            //choix données natales par défaut
            NatalExterne=0;
            choixTableauDonnees.item(0).checked=true;
            cIdN(true);
            divChoixDonnees.hidden=true;
            divLabelsProg.hidden=true;
            fixePhases=0;
            okTransits=okSynastrie=okArc=okProgresse=okRS=0;
            checkMaintenant.checked=false;
            nomTheme2=""; //synastrie
            asc=posMaisonNatal[0];
            
             //annulé (8/8/2020) : en tropical, sortie sur thème du jour (permet d'afficher révolution solaire)
            if (okUranien==0){
                divTableaux.hidden=false; 
                //cache tableau et dévalide les options
                tableau.hidden=true;
                for (var i=0;i<=2;i++){
                    choix2.item(i).checked=false;
                }
            }else { //en uranien, sortie sur thème natal
                divTableaux.hidden=true;
                typeRef=0;
                labelRoue.hidden=false;
            }
            if (latNatal) [latitude,longitude,utc]=[latNatal,longNatal,utcNatal];
            requetes(dateNatal,heureNatal,titreNatal);
    
}
function transits(dateDepart){ //dateDepart en mode transits dynamiques
    if (resetNatal()==0) return;
            cIdN(true);
            divChoixDonnees.hidden=false;
            divTableaux.hidden=true;
            if (tableau.hidden==false) {okTransits=okProgresse=0;tableau.hidden=true};
            divLabelsProg.hidden=true;
            fixePhases=0;
            checkMaintenant.checked=false;
                        
            if ((okTransits || okProgresse || okArc || okRS) && okSynastrie==0){//conserve la date et restaure l'heure
                if (dateDepart){
                    choixDate.value=dateDepart;
                    incDateHeure();
                }
                choixHeure.value=heureSauve;
                infoDateNaissance.hidden=true;
            }else {//recalcule la date du jour si succède à "theme"
                if (!dateDepart) dateJour();
                else choixDate.value=dateDepart;
                incDateHeure();
            }
            dateMaisons=choixDate.value;
            okTransits=1;
            okRS=okArc=okProgresse=okSynastrie=0;
            [latitude,longitude,utc]=[latLocal,longLocal,utcLocal];//laisser après dateJour() sinon utc incorrect
                
            requetes(choixDate.value,choixHeure.value,titreTheme);
            if (okUranien) divTableaux.hidden=true;     
        }

function arc(){
    if (resetNatal()==0) return;
            if (okMix) cIdN(true);
            else cIdN(false);
            divChoixDonnees.hidden=false;
            divTableaux.hidden=true;
            if (tableau.hidden==false) {okTransits=okProgresse=0;tableau.hidden=true};
            divLabelsProg.hidden=true;
            //calcule ou non date du jour
            if ((okTransits || okProgresse || okArc || okRS) && okSynastrie==0){
               heureSauve=choixHeure.value;
            }else{
                dateJour();
                incDateHeure();
            }
            dateMaisons=choixDate.value;
            okTransits=okArc=1;
            okRS=okProgresse=okSynastrie=0;
            if (latNatal) [latitude,longitude,utc]=[latNatal,longNatal,utcNatal];
            if (okMix==0) choixHeure.value=heureNatal;
            incDateHeure();
            calcArcSolaire();
            if (okUranien) asc=ascDef;
            dessins();   
}

function RS(){
    if (resetNatal()==0) return;
            if (okMix) cIdN(true);
            else cIdN(false);
            divChoixDonnees.hidden=false;
            divTableaux.hidden=true;
            if (tableau.hidden==false) {okTransits=okProgresse=0;tableau.hidden=true};
            divLabelsProg.hidden=true;
            //calcule ou non date du jour
            if ((okTransits || okProgresse || okArc || okRS) && okSynastrie==0){
               heureSauve=choixHeure.value;
            }else{
                dateJour();
                incDateHeure();
            }
            dateMaisons=choixDate.value;
            okTransits=okRS=1;
            okArc=okProgresse=okSynastrie=0;
            if (okMix==0) choixHeure.value=heureNatal;
            incDateHeure();
            calcRS();    
}

function progresse(){
    if (resetNatal()==0) return;
            if (okMix) cIdN(true);
            else cIdN(false);
            divChoixDonnees.hidden=false;
            divTableaux.hidden=true;
            if (tableau.hidden==false) {okTransits=okProgresse=0;tableau.hidden=true};
            divLabelsProg.hidden=false;
            if (!dateNatal){
                selectTransits.value="optTheme";//theme seul
                return;
            }
            //calcule ou non date du jour
            if ((okTransits || okProgresse || okArc || okRS) && okSynastrie==0){
                heureSauve=choixHeure.value;
            }else{
                dateJour();
                incDateHeure();
            }
            dateMaisons=choixDate.value;//important si suit RS sinon n'incrémente pas !
            okTransits=okProgresse=1;
            okRS=okArc=okSynastrie=0;
            fixePhases=0;
            if (latNatal) [latitude,longitude,utc]=[latNatal,longNatal,utcNatal];//laisser après dateJour() sinon utc incorect
            if (okMix==0) choixHeure.value=heureNatal;
            incDateHeure();
            
            //en transits uniquement maisons natales (trop compliqué à gérer AS/MC)
            if (okTransits){
                divMaisonsNatal.hidden=true;
                if (okUranien) divCoeff.hidden=true;
            }else{
                checkMaisonsNatal.checked=false; //permet de sauver maisons progressées (donc AS/MC)
                divMaisonsNatal.hidden=false;
                divCoeff.hidden=false;
            }
            checkEquationTemps.hidden=false;
            rechercheThemeProgresse(dateNatalBis);
            checkMaisonsNatal.checked=true; 
            recupPosMaisons("natal");
            //affichage
            dessins();    
    
}

function synastrie(){
    if (resetNatal()==0) return;
    cIdN(true);
    devalideSynastrie(true);
    divChoixDonnees.hidden=false;
    divTableaux.hidden=true;
    if (tableau.hidden==false) {okTransits=okProgresse=0;tableau.hidden=true};
    divLabelsProg.hidden=true;
    okTransits=okSynastrie=1;
    okRS=okArc=okProgresse=0;
    fixePhases=0;
    if (okUranien==1) tabMP.rows[0].cells[0].textContent=labelsDroite[30]+" - " +labelsDroite[32];//titre tableau=synastrie    
}

//gestion des transits : 1er transit en position1 et 2ème transit en position2
//(idem pour les aspects, voir globalaspects à la fin)
async function mix(dateDepart){//async à cause du async de dessins...sinon "double transits" doublés
    okMix=0;
    if (!okUranien){
        margeNoir();
        margeDiv.hidden=false;
    }
    if (okSynastrie){
        devalideSynastrie(false);
        if (affiSave) affiSave.style.fontWeight="";//enlève "gras" du nom sélectionné
    }
    
    var a,b=selectTransits.selectedOptions;
    if (b.length==0) return;
    //surtout pour android, sinon obligé de décocher 'theme seul' pour pouvoir sélectionner le ou les transits
    if (b.item(0).value=="optTheme"){
        if (b.length>1) b.item(0).selected=false;
    }
    var limite=2;//2 transits sélectionnables
    //sauf synastrie
    if (b.item(b.length-1).value=="optSynastrie") limite=1;
    while (b.length>limite){
        b.item(b.length-1).selected=false;
    }
    //exécute 1 ou 2 transits
    for (var i=0;i<b.length;i++){
        if (i>0 && b.length==2) okMix=1;//=2 transits sélectionnés
        a=b.item(i).value;
        if (a=="optTheme") theme();
        else if (a=="optTransits") await transits(dateDepart);
        else if (a=="optArc") await arc();
        else if (a=="optRS") await RS();
        else if (a=="optProgresse") await progresse();
        else if (a=="optSynastrie") synastrie();
    }  
}

function calcRS(){
    if (!dateNatal) return;
    [utc,longitude,latitude]=[utcLocal,longLocal,latLocal];
    var date=choixDate.value; //dateMaintenant;
    var ceJour=new Date(date);
    var an=ceJour.getFullYear();
    var birthday; //date du précédent anniversaire
    var timeDiff;
    var abc;
    //recherche précédent anniversaire
    do {
        abc=String(an)+dateNatal.slice(4);//"-mm-jj"
        birthday=new Date(abc);
        timeDiff = ceJour - birthday; // intervalle en ms
        an-=1;        
    } while (timeDiff<0);
    //date anniversaire
    an+=1;
    var mois=birthday.getUTCMonth()+1;
    var jour=birthday.getUTCDate();
    //soleil lendemain anniversaire
    abc=String(an)+"-"+String(mois)+"-"+String(jour+1);
    jj=calcJourJulien(abc,0);
    var soleil=calcSoleilRS(jj);
    //soleil anniversaire
    abc=String(an)+"-"+String(mois)+"-"+String(jour);
    var jj=calcJourJulien(abc,0);
    var soleil0=calcSoleilRS(jj);
    //pas journalier du soleil
    var pas=r360(soleil-soleil0);
    //calcul
    var manque=soleilNatalBis-soleil0;
    var heuredecimale=manque/pas*24;
    //équation de temps (négligeable)
    var x0=equationTemps(abc);
    var x1=equationTemps(dateNatal);
    heuredecimale+=x0-x1;
    //heure RS en décimal (note : le calcul de l'ascendant ne dépend pas d'utc)
    heuredecimale+=utc;
    //ajuste date, heure
    if (isNaN(heuredecimale)) heuredecimale=0;
    if (heuredecimale<0){
        do {
            birthday.setUTCDate(birthday.getUTCDate()-1);
            heuredecimale+=24;
        }while (heuredecimale<0);
    }
    else if (heuredecimale>=24) {
        do {
            birthday.setUTCDate(birthday.getUTCDate()+1);
            heuredecimale-=24;
        }while (heuredecimale>=24);
    }
    //recul d'1 minute (décalage avec sites +1 à 3')
    heuredecimale-=1/60;
    
    an=birthday.getFullYear();
    mois=birthday.getUTCMonth()+1;
    jour=birthday.getUTCDate();
    //mois et jours sur 2 chiffres, sinon ça change l'asc (calc heure siderale) !
    dateRS=String(an)+"-"+ajoutZero(String(mois))+"-"+ajoutZero(String(jour));
    
    //mise en forme
    var a,b,c,a1;
        //heure
        a=Math.trunc(heuredecimale);//partie entière
        b=Math.floor(60*(heuredecimale-a));//partie décimale: .floor arrondit à la ' inférieure (décalage avec sites +1 à 3')
        heureRS=String(a)+":"+ajoutZero(String(b));
        //latitude
        a=Math.trunc(latitude);
        b=Math.abs(Math.round(60*(latitude-a)));
        var signe="";
        if (latitude<0) signe="-";
        var lat=signe+String(a)+"°"+String(b)+"'";
        //longitude
        a=Math.trunc(longitude);
        b=Math.abs(Math.round(60*(longitude-a)));
        signe="";
        if (longitude<0) signe="-";
        var long=signe+String(a)+"°"+String(b)+"'";
    
    if (okTransits==0) titreTheme=nomNatal+" - "+ labelsCentre[20]+" - "+dateLongue(dateRS)+" - "+heureRS+ labelsDroite[9] + placeDef + " (utc "+utc +", lat. "+lat +", long. "+ long +")";
      
    //affichage RS
    calcPosPlanetes(dateRS,heureRS);
    calcPosMaisons(dateRS,heureRS);
    if (okTransits) recupPosMaisons("natalbis");
    dessins();
}

function calcArcSolaire(date){
    if (!dateNatal) return;
    if (!date) var reponse=controleDate(choixDate.value);
    if (!dateNatal){
        dateNatalBis=dateNatal=dateMaisons;
        heureNatal=choixHeure.value;
    }
    rechercheThemeProgresse(dateNatalBis,1); //1=calcul arc
    arcSolaire=soleilProgresse-soleilNatalBis;//soleilNatal;
    if (isNaN(arcSolaire)) arcSolaire=0;
    for (var i=0;i<=maxPl;i++){
        posPlanete[tm()][i]=r360(posPlanete[0][i]+arcSolaire);
        if (checkDom.checked) posDom[tm()][i]=r360(posDom[0][i]+arcSolaire);
    }
}
