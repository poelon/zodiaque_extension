function definitions(choix){
    //aspect=["conjonction","semi-sextile","semi-carré","sextile","carré","trigone","sesqui-carré","quinconce","opposition","quinconce","sesqui-carré","trigone","carré","sextile","semi-carré","semi-sextile","conjonction"],
    var angle=[0,30,45,60,90,120,135,150,180,210,225,240,270,300,315,330,360],
        couleur=['blue','blue','red','blue','red','blue','fuchsia','green','red','green','fuchsia','blue','red','blue','red','blue','blue'],
        arc=[[0,0],[1,1],[1,2],[2,3],[3,3],[4,4],[4,4],[5,5],[5,6],[6,7],[7,7],[8,8],[8,8],[8,9],[9,10],[10,11],[11,11],[12,12],[12,12],[13,13],[13,14],[14,15],[15,15],[16,16],[0,0]];
        //couleurs sesqui-carrés et quinconces différentes sur graphe ou tableau transits (choix=1)
        if (choix==1){
            couleur[6]=couleur[10]='red'; //sesqui-carré
            couleur[7]=couleur[9]='blue'; //quinconce
        }
    return [angle,couleur,arc];
}


//******************************************** transits mondiaux ******************************************

function rechercheTransitsMondiaux(anneeTransit){
     //2 formats de date : new Date("aaaa-mm-jj") ou new Date("aaaa","m-1","j+1")
    var jour=1, //1er janvier année transit 
        premJanvier=(anneeTransit+"-"+"01"+"-"+"01"),
        transitante,
        transitee,
        position2,
        retrograde,
        ecart,
        tolerance,
        positionSigne,
        heuredecimale=12-utc,
        heure=ajoutZero(String(heuredecimale))+":00",
        ijmax=11,
        jourmax=366;//365+1 sinon pas d'affichage des transits du 31/12 car détectés le jour suivant
        if (Number(anneeTransit)%4==0 && Number(anneeTransit)!=1900){jourmax+=1;} //année bisextile
        
    okTransits=1;
    //départ au 1er janvier
    var jj=calcJourJulien(premJanvier,heure)-1 //-1 permet de récupérer certains transits du 1/01...
    var jj0=jj;
        
    //exclusion NN et Lilith si en-dehors plage 1800-2039
    if (Number(anneeTransit) <=1799 || Number(anneeTransit) >=2040){ijmax=9}
    
    //transitantes Jupiter à Lilith
    for (var i=5; i<=ijmax; i++){
       
        //transitées : planètes, maisons, signes
        for (var j=0; j<=11; j++){
            transitee= posPlanete[0][j];
            jour=0;//pas 1, car permet de récupérer certains transits du 1/01...
            jj=jj0;
            orbTab["planete"]=orbTab["maison"]=100;//plafond orbes (doit être > tolerance sinon erreurs dates transits, ex lilith) 
            stationTab["planete"]=stationTab["maison"]="";//stationnaire
            retroTab["planete"]=retroTab["maison"]="";//retrograde
            
            //jours
            do {
                //Jupiter à Neptune
                if (i<9){
                    position2=calcPlanetes(Number(anneeTransit),heuredecimale,jj-1,i);
                    transitante=calcPlanetes(Number(anneeTransit),heuredecimale,jj,i);
                }
                //Pluton
                else if (i==9){
                    position2=calcPluton(Number(anneeTransit),heuredecimale,jj-1);
                    transitante=calcPluton(Number(anneeTransit),heuredecimale,jj);
                }
                //NN  Lilith
                else if (i==10 || i==11){ 
                    position2=calcNNLilith(jj-1,i);
                    transitante=calcNNLilith(jj,i);
                } 
                //rétrograde si écart journalier négatif
                ecart=reboucle(transitante-position2);
                retrograde= ecart>=0 ? "" : diversFonts[0];
                
                //orbe max
                tolerance=Math.max(1,2*Math.abs(ecart));//2*Math... pour Lilith
                    //si non prise en compte des transits stationnaires :
                    //tolerance=2*Math.abs(ecart));
                
            //éléments transités  
                //maisons
                    traiteTransitsMondiaux(retrograde,jj,anneeTransit,transitante,posMaisonNatal[j],i,j,jour,"maison",tolerance,ecart); 
                //signes
                if (i<10 && j==0){//pas de transits sur signe pour NN, lilith et un seul passage (j=0)
                    traiteTransitsMondiaux(retrograde,jj,anneeTransit,transitante,transitante,i,0,jour,"signe",0.5,ecart);
                }
                //planètes
                if (j<=ijmax && isNaN(posPlanete[0][j])==false){
                                    //abandonné, planetes externes bougent avec l'heure...
                                    //si domitude, calcmaisons pour avoir position transitante en domitude
                                    // if (checkDom.checked) {
                                    //     date=new Date(anneeTransit,"0",String(jour+1));
                                    //     date=date.toJSON().slice(0,10);//"aaaa-mm-jj"
                                    //     posPlanete[1][i]=transitante;
                                    //     calcPosMaisons(date,heure);
                                    //     transitante=posDom[1][i];
                                    // }
                                    // transitee= checkDom.checked ? posDom[0][j] : posPlanete[0][j];
                    
                    traiteTransitsMondiaux(retrograde,jj,anneeTransit,transitante,transitee,i,j,jour,"planete",tolerance,ecart);
                }
                jour+=1;
                jj+=1;
            } while (jour<=jourmax);
        }
    }
    chronoTab();//ordre chronologique
    okTransits=0;
    tableau.rows[0].cells[0].textContent=nomNatal+" - " +labelsDroite[10] +anneeTransit;
    tableau.rows[1].cells[0].textContent="0"+"°"+" < "+labelsDroite[44]+" < 1"+"°";//noir
}

function traiteTransitsMondiaux(retrograde,jj,anneeTransit,transitante,transitee,i,j,jour,type,tolerance,ecart){
    var [angle,couleur,arc]=definitions(1);
    var planete,
        aspect,
        date2,
        jourReel,
        mois,
        message="",
        stationnaire=0,
        colore;
    var gap=transitante-transitee;//=0 pour les signes
        if (gap<0){gap+=360}
    var gapArc=Math.floor(gap/15);
    
    //recherche aspects
    for (var k=arc[gapArc][0]; k<=arc[gapArc][1]; k++){
        //exclusions
        switch(type){
        case "planete":
            //Jupiter : conjonction, carré, trigone et opposition seulement
            if (i==5 && (k==1 || k==2 || k==3 || k==6 || k==7 || k==9 || k==10 || k==13 || k==14 || k==15)){
                continue;
            }
            //NN : conjonction, carré, opposition
            if ((i==10 || j==10) && (k==1 || k==2 || k==3 || k==5 || k==6 || k==7 || k==9 || k==10 || k==11 || k==13 || k==14 || k==15)){
                continue;
            }
            //Lilith : conjonctions
            if (i==11 && k>0 && k<16){    
                continue; 
            }
            break;
        case "maison":
            //Maisons : ni 7 ni 4, AS et MC par conjonction, carré, trigone et opposition, sauf lilith ou autres maisons par conjonction seulement
            if ((j==6 || j==3) || i<11 && (k==1 || k==2 || k==3 || k==6 || k==7 || k==9 || k==10 || k==13 || k==14 || k==15) || (i==11 && k>0 && k<16) || ((i==11 || i==10) && j!=0 && j!=9) || (j!=0 && j!=9 &&  k>0 && k<16)){
                continue;
            }
            break;
        }
        //aspect trouvé
        if (gap<=(angle[k]+tolerance) && gap>=(angle[k]-tolerance)){
            message="en cours";
            //minimum déjà trouvé : sortie si pas de chgt retrograde, sinon reset plafond orbe et poursuite
            if (orbTab[type]==-1){
                if (retroTab[type]==retrograde) break;
                else orbTab[type]=100;
            }
            //autres cas
            retroTab[type]=retrograde;
            //aspects
            aspect=aspectsFonts[k%16];
                //"+" = croissants, "," = décroissants
                if (k>0 && k<8){aspect+="+";}
                else if(k>8 && k<16){aspect=String.fromCharCode((109+k)-2*(k-8))+",";}
                
        //signes + sortie
            if (type=="signe"){
                //date (jour est compris entre 2 et 366 ou 367)
                date2=new Date(anneeTransit,"0",String(jour+1));
                mois=date2.getUTCMonth()+1;
                jourReel=date2.getUTCDate();
                //recherche signe
                var x=Math.floor(transitante/30);
                var signe=signesFonts[x];
                var abc="";
                //abc : dernier signe enregistré
                if (tableau.rows[3].cells[i-4].lastChild){
                    abc=tableau.rows[3].cells[i-4].lastChild.textContent;
                }
                //écriture du signe au 1er janvier si abc="", sinon de chgt de signe + date
                if (abc.search(signe)==-1){
                    var cellule=tableau.rows[3].cells[i-4];
                    abc="";
                    if (jour>1)abc="  " + String(jourReel) + "/" + ajoutZero(String(mois));//après le 1er janvier
                    ecritCell(cellule,'green',signe +abc);
                }
                break;
            } 
            
        //planetes et maisons
            //enregistre le signe (>0 ou <0) de la 1ere occurence
            var x=gap-angle[k];
            if (!stationTab[type]) stationTab[type]=Math.sign(x);
            //cherche l'orbe le + petit
            x=Math.abs(x);
            if (x < orbTab[type]){
                orbTab[type]=x;
                break;
            }
            //trouvé
            orbTab[type]=-1;
            if (jour<=1) break; //transit=j-1 = 31/12 année précédente
            //compare le signe (>0 ou <0) de la 1ère et de la dernière occurence; si égaux = stationnaire
            if (stationTab[type] == Math.sign(gap-angle[k])) stationnaire=1;
            //stationnaire ?
            if (stationnaire==0) colore=couleur[k];
            else{
                //sortie si NN, Lilith
                if (i>=10) break;
                colore="black";
            }
            //symboles
            if (type=="planete"){
                planete=planetesFonts[j];
            }else if (type=="maison"){
                //AS,MC,autres maisons
                if (j==0) planete=planetesFonts[13];
                else if(j==9)planete=planetesFonts[14];
                else planete=j+1;
            }
            
            //date : jour=date transit +1 mais date2 ramène en arrière d'1 jour
            date2=new Date(anneeTransit,"0",String(jour));
            mois=date2.getUTCMonth()+1;
            jourReel=date2.getUTCDate(); 
            //données
            var abc=aspect+planete+"  " + String(jourReel);
            if (type=="maison"){abc+="/" + ajoutZero(String(mois));}    
            //cellule tableau
            var tabCell;
            if (type=="planete"){tabCell=tableau.rows[mois+3].cells[i-4]}
            else{tabCell=tableau.rows[2].cells[i-4]};
            // écriture dans tableau
            ecritCell(tabCell,colore,retrograde+abc);
            stationTab[type]="";
            break;
        }
     }
     if (message==""){
         orbTab[type]=100;//reset plafond orbe
         stationTab[type]="";
         retroTab[type]="";//retrograde;
     }
}


//*************************************** transits progressés ************************************

function rechercheTransitsProgresses(date,heure,pnpp){
    //pnpp=0=transits progressé/natal, pnpp=1=transits progressé/progressé
    var tolerance;//=Number(sensibilite.value); //0.1;
    var positionTransitante,
        positionTransitee,
        positionSigne,
        dateMaisonsProgresse,
        joursmois=[31,28,31,30,31,30,31,31,30,31,30,31];

    //année bissextile
    if (anneeTransit.value%4 ==0 && anneeTransit.value !=1900){
        joursmois[1]=29;
    }    
    //enregistre les positions des planètes au 1er janvier (= date anniversaire) de l'année progressée (recherche avec date format string jj/mm/aaaa)
    var [jours,date1Janvier,age]=parametresProgresse(date,"01/01/"+anneeTransit.value,anneeTransit.value);
    var jour=ajoutZero(String(date1Janvier.getUTCDate()));
    var mois=ajoutZero(String(date1Janvier.getUTCMonth() +1));
    var an=String(date1Janvier.getUTCFullYear());
    //calcul positions planetes progressées mises en posPlanete[1]
    calcPosPlanetes(an+"-"+mois+"-"+jour,heure);
    //positions planètes progressées au jour exact
    traitePlaneteProgresse(jours);
    
    //récupère les positions des maisons progressées (car peuvent être vides)
    if(pnpp==1){
        //recherche position ascendant à an+1 pour déterminer l'écart annuel ecartProg
        ascProgPlus1(heure,date1Janvier,jours);
        //sauve positions maisons progressées au 1er janvier
        var dateMaisonsProgresse=String(an)+"-"+String(mois)+"-"+String(jour);
        calcPosMaisons(dateMaisonsProgresse,heure,jours,1);
        sauvePosMaisons("progresse"); 
    }
    
jours=0;    //après le 1er janvier
 for (mois=1; mois<=12; mois++){ 
    
    for (jour=1; jour<=joursmois[mois-1]; jour++){
        
        //transitantes Soleil à Saturne
        for (var i=0; i<=6 ; i++){
            positionTransitante=posPlanete[1][i];
            tolerance=Math.abs(ecartJour[i]/365);
            
            //transités : Signes, Maisons et Planètes
            for (var j=0; j<=11; j++){ 
                //maisons
                if (pnpp==0){positionTransitee=posMaisonNatal[j]}
                else {
                    //ecartProg=écart annuel sur l'ascendant
                    positionTransitee=r360(posMaison[j]+(jours*ecartProg/365));
                }
                    traiteTransitsprogresses(positionTransitante,positionTransitee,tolerance,i,j,mois,jour,"maison");
               
                //planètes
                if (i==j && pnpp==1) continue; //en progressé/natal conserve transits par ex de lune progressée sur lune natale
                pnpp==1 ? positionTransitee=posPlanete[1][j] : positionTransitee=posPlanete[0][j];
                    traiteTransitsprogresses(positionTransitante,positionTransitee,tolerance,i,j,mois,jour,"planete");     
            } 
            //signes
            traiteTransitsprogresses(positionTransitante,positionTransitante,tolerance,i,j,mois,jour,"signe");
        }
     //incrémente d'1 jour (dans themes.js)
     traitePlaneteProgresse(1);
     jours+=1;
    }
 }
  //  chronoTab();
    if (pnpp==1){var abc=" - "+labelsDroite[12];} //transits progressé/progressé "
    else {var abc=" - " + labelsDroite[11];} //]transits progressé/natal " 
  //  tableau.rows[0].cells[0].textContent=titreTheme.split(" ")[0] + abc + anneeTransit.value;
    tableau.rows[0].cells[0].textContent=nomNatal + abc + anneeTransit.value;
    if (checkEquationTemps.checked==true){
        tableau.rows[0].cells[0].textContent+=" ("+labelsCentre[15]+ " : "+equationProg+")";
    }
}

function traiteTransitsprogresses(transitante,transitee,tolerance,i,j,mois,jour,type){
    var [angle,couleur,arc]=definitions(1);    
    var gap,   
        gapArc,
        aspect,
        signe,
        planete;
    gap=transitante-transitee;//=0 pour les signes
    if (gap<0){gap+=360;}
    gapArc=Math.floor(gap/15);    

 // uniquement les conjonctions pour signes et maisons sauf AS et MC
 if (type=="planete" || ((type=="signe" || type=="maison") && (gapArc==0 || gapArc>=23)) || (type=="maison" && (j==0 || j==9))){ 
    //recherche aspects
    for (var k=arc[gapArc][0]; k<=arc[gapArc][1]; k++){
        //aspect trouvé
        if (gap<=(angle[k]+tolerance) && gap>=(angle[k]-tolerance)){
           aspect=aspectsFonts[k%16];
            if (k>0 && k<8){
                //+ = croissants
                aspect+="+";
            }else if(k>8 && k<16){
                //, = décroissants
                aspect=String.fromCharCode((109+k)-2*(k-8))+",";
            }
            //planètes
            if (type=="planete"){
                planete=planetesFonts[j];
                //écriture aspect+transitée+jour
                var abc=tableau.rows[mois+3].cells[i+1].textContent
                //évite doublons (si -1, la même planète n'existe pas déjà pour ce mois-là)
                if (abc.search(planete)==-1){
                    var cellule=tableau.rows[mois+3].cells[i+1];
                    ecritCell(cellule,couleur[k],aspect+planete+"  "+String(jour));
                }
            //maisons
            }else if (type=="maison"){
                var abc=tableau.rows[2].cells[i+1].textContent
                if (abc.search(j+1+" ")==-1){
                    var cellule=tableau.rows[2].cells[i+1]
                    ecritCell(cellule,couleur[k],aspect+String(j+1) +"  "+ String(jour) + "/" + ajoutZero(String(mois)));
                }
            }
            //signes
            else if (type=="signe"){
                var x=Math.floor(transitante/30);
                var signe=signesFonts[x];
                var abc="";
                if (tableau.rows[3].cells[i+1].lastChild){
                    abc=tableau.rows[3].cells[i+1].lastChild.textContent;
                }
                //écriture signe au 1er janvier si abc="", sinon chgt de signe
                if (abc.search(signe)==-1){
                    abc="";
                    if ((jour+mois)>2) abc="  " + String(jour) + "/" + ajoutZero(String(mois));//après le 1er janvier
                    var cellule=tableau.rows[3].cells[i+1];
                    ecritCell(cellule,'green',signe +abc);
                }
                break;
            }  
        }
    } 
  } 
}


// **********************************************tableau transits **********************************************

function feuilleTransits(colonneDebut,colonneFin){
    var tabCell;
    choix1.item(0).checked=false;
    choix1.item(1).checked=false;
     if (tableau) {
  	garbage=cadre.removeChild(tableau);
    }
    //création tableau
    tableau = document.createElement('table');
    tableau.setAttribute('width', '100%');
    cadre.appendChild(tableau);
    //en-tête général
    var header=document.createElement('theader');
    tableau.appendChild(header);
    var row = document.createElement('tr');
    tableau.appendChild(row);
    var cell = document.createElement('th');
    cell.setAttribute('height', '30px');
    cell.setAttribute('colspan', 8);
    cell.textContent = 'en-tête';
    row.appendChild(cell);
    //en-têtes lignes et colonnes
        //lignes
    for (var i=0; i<=15; i++){
        row = document.createElement('tr');
        tableau.appendChild(row);
        tableau.rows[i+1].style.fontFamily="Zodiac";//taille dans le css (td)
            
        //colonnes
        for (var j=colonneDebut-1; j<=colonneFin; j++){
            cell = document.createElement('td');
            row.appendChild(cell);
            if((i==0 || i==15) && j>=colonneDebut){
                cell.width="12.5%";
                cell.textContent = planetesFonts[j];
                cell.style.backgroundColor="rgb(240,240,240)"; //gris
                     
            }else if(j<colonneDebut){
                cell.style.font=1+'vw serif';
                cell.style.backgroundColor="rgb(240,240,240)";
                if(i==1){
                    //"Maison";  
                    cell.textContent =labelsDroite[19];
                }
                else if(i==2){
                    //"Signe"; 
                    cell.textContent =labelsDroite[20];
                }
                else if(i>2 && i<15){
                    //mois en lettres
                    var date = new Date(Date.UTC(2012,i-3,20));
                    var options = { month: 'long'};
                    var langue=browser.i18n.getUILanguage()
                    var dateTime=new Intl.DateTimeFormat(langue, options).format(date);  
                    cell.textContent =dateTime;
                }
            }
        }
    } 
    tableau.rows[1].cells[0].style.backgroundColor='white';
    tableau.rows[16].cells[0].style.backgroundColor='white';
}  

function ecritCell(cellule,couleur,contenu){
    cellule.align='center';
    //création d'une sous-cellule
    var row=document.createElement('tr');
    cellule.appendChild(row);
    var cell=document.createElement('td');
    row.appendChild(cell);
    cell.setAttribute('class','tabcell'); //pas de bordures
    cell.style.color=couleur;
    cell.textContent=contenu;
}

function chronoTab(){
    var x,
        max,
        cellule,
        couleur,
        cell0,
        cell1;
        
    for (var l=1;l<=7;l++){ //colonnes
        for (var k=2;k<=15;k++){ //lignes
            if (k==3) continue;//ligne signes exclue
            cellule=tableau.rows[k].cells[l];
            max=cellule.children.length;
            if (max<=1) continue;
            cell0=[];
            cell1=[];
           
             //copie du contenu cellule dans cell0 + date dans cell1
            for (var i=0;i<max;i++){
                cell0[i]=cellule.children.item(i).textContent.split("  ");//aspect et date au format string
                couleur=cellule.children.item(i).cells[0].style.color;
                cell0[i][2]=couleur;
                //date au format numérique
                if (k==2){//ligne maisons
                    x=cell0[i][1].split("/");
                    cell1[i]=Number(x[1])*100+Number(x[0]) //ex. "19/12" donne 1219
                }else cell1[i]=Number(cell0[i][1]); 
            }
            //réécriture cellule en partant de la + petite date
            for (i=0;i<max;i++){
                x=Math.min(...cell1);//garder les 3... !
                for (var j=0;j<max;j++){
                    if (cell1[j]!=x) continue;
                    cellule.children.item(i).textContent=cell0[j][0]+" "+cell0[j][1];
                    cellule.children.item(i).style.color=cell0[j][2];
                    cell1[j]=9999;
                    break;
                }
            }
            //colonne lilith : plus petits caracteres
            if (l==7) cellule.style.fontSize=0.65+'vw';
        }
    }
}
