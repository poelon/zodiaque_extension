//canvas
var canvasTarget = document.getElementById("canvasTarget"),
    imexZone = document.getElementById("imexZone"),
    svg=document.getElementById("svg"),
//div
    cadre=document.querySelector('.cadredroite'),//ne marche pas avec document.getElementsByClassName('cadredroite')
    cadre1=document.querySelector('.cadregauche'),
    cadre2=document.querySelector('.cadrecentre'),
    commentaire=document.getElementById("commentaire"),
    margeDiv=document.getElementById("marge"),
    divMP=document.getElementById('divMP'),
    divTableaux=document.getElementById('divTableaux'),
    divtabMP=document.getElementById('divtabMP'),
    divChoixResumes=document.getElementById('divChoixResumes'),
    divDynamique=document.getElementById('divDynamique'),
//image
    infoEquationTemps=document.getElementById("imgEquationTemps"),
    infoTransits=document.getElementById("infoTransits"),
    infoWiki=document.getElementById("infoWiki"),
    infoDateNaissance=document.getElementById("infoDateNaissance"),
//input button
    boutonBascule = document.getElementById("bascule"),
    boutonVideBandeau = document.getElementById("videBandeau"),
    boutonTest = document.getElementById("test"),
//input checkbox
    checkMaisons=document.getElementById('checkMaisons'),
    checkMaintenant=document.getElementById('checkMaintenant'),
    checkDom=document.getElementById("checkDom"),
    cacheGauche=document.getElementById("cacheGauche"),
    cacheCentre=document.getElementById("cacheCentre"),
    cacheTitre=document.getElementById("cacheTitre"),
    checkMaisonsNatal = document.getElementById("checkMaisonsNatal"),//maisons natales en progressé
    checkEquationTemps = document.getElementById("checkEquationTemps"),
    checkAsMc = document.getElementById("checkAsMc"),
//input date, time
    choixDate=document.getElementById('choixDate'),
    choixHeure=document.getElementById('choixHeure'),
//input file pas utilisé
    fileElem = document.getElementById("fileElem"),
//input number
    anneeTransit=document.getElementById("anneeTransit"),
    coeffOrbe=document.getElementById("coefforbe"),
    luminosite=document.getElementById("luminosite"),
    horizon=document.getElementById("horizon"),
    incJour=document.getElementById("incJour"),
    incMois=document.getElementById("incMois"),
    incAn=document.getElementById("incAn"),
    incHeure=document.getElementById("incHeure"),
    incMinute=document.getElementById("incMinute"),
//input radio
    choixTypeDonnees=document.getElementsByName("choixTypeDonnees"), //positions, dominantes, arbre, dynamique
    choix1=document.getElementsByName("choix1"), //thème natal ou progressé
    choix2=document.getElementsByName("choix2"), //transits
    choix3=document.getElementsByName("choix3"), //éléments
    choixZodiaque=document.getElementsByName("zodiaqueType"), //tropical,uranien
    choixRoue=document.getElementsByName("roue"), //roue 90 ou 360
    choixTableauDonnees=document.getElementsByName("tableauUranienType"), //tableau uranien theme ou transits
    selectMP=document.getElementById("selectMP"),//mi-points ou formules
    selectTransits=document.getElementById("selectTransits"),//choix des transits
    
//label, titre
    titreCanvas=document.getElementById("titrecanvas"),
    labelCoeff=document.getElementById("labelcoeff"),
    labelCoeff2=document.getElementById("labelcoeff2"),
    divLabelsProg=document.getElementById("divLabelsProg"),
    labelMaisonsNatales=document.getElementById("labelMaisonsNatales"),
    labelEquationTemps=document.getElementById("labelEquationTemps"),
    choixUtc=document.getElementById("utc"),
    choixLatitude=document.getElementById("latitude"),
    choixLongitude=document.getElementById("longitude"),
    labelRoue=document.getElementById("labelRoue"),
    labelMP1=document.getElementById("labelMP1"),
    labelMP2=document.getElementById("labelMP2"),
    labelMP3=document.getElementById("labelMP3"),
    labelDateNaissance=document.getElementById("labelDateNaissance"),
    labelSynastrie=document.getElementById("labelSynastrie");
 
    
var titreTheme="?",
    titreNatal="?",
    coordonnees,
    ns='http://www.w3.org/2000/svg',
    canvasPhases,
    canevasWidth,
    canevasHeight,
    centre=[],
    rayon,
    taille=20,
    fontTableau,
    navigateur,
    android,
    garbage,
    fixePhases=0,
    refPhase,
    timerId1;


var utc,
    utcLocal,
    utcNatal,
    longitude,
    longLocal,
    longNatal,
    latitude,
    latLocal,
    latNatal,
    lux=0.15,
    hrz=0,
    demarrage;
    
//valeurs par défaut (lieu...)
var utcDef="",
    latDef="48.51",
    longDef="2.21",
    luxDef, //="0.2";
    horizonDef=0,
    orbesDef=[];
    
var tableau = document.createElement('table');  
    cadre.appendChild(tableau);
var tabResume = document.createElement('table');  
var tabDominantes = document.createElement('table');  
var tabChaine = document.createElement('table');
var tabUranien = document.createElement('table');  
    cadre2.appendChild(tabUranien);
var tabMP = document.createElement('table');  
    divtabMP.appendChild(tabMP);
    
var dateMaisons,//(format "aaaa-mm-jj") la date Firefox est aaaa-mm-jj avec dom.forms.datetime=true
    dateNatal,
    dateNatalBis,//utilisé si changement heure de naissance en progressé et arc solaire
    dateLong,
    dateMaintenant,
    dateRS,
    heureNatal,
    heureTheme2,
    heureMaintenant,
    heureSauve,
    heureRS,
    nomNatal,
    nomTheme2,
    equationProg,
    anChinois,
    elementChinois;
    

//Zodiac-Enigma.ttf (ZODIAC.TTF + 8 planètes astro uranienne de EnigmaAstrology.ttf)
    //Soleil-Pluton(0-9),NN(10),Lilith(11),NS(12),AS(13),MC(14)
    //Cupidon(15),Hadès(16),Zeus(17),Kronos(18),Apollon(19),Admète(20),Vulcain(21),Poséidon(22),Bélier(23),roue libre(24)
var planetesFonts=["A","B","C","D","E","F","G","H","I","J","M","N","W","K","L","(",")","!","'","#","$","%","&","a","0"];
var signesFonts=["a","b","c","d","e","f","g","h","i","j","k","l"];
var aspectsFonts=["m","n","o","p","q","r","s","t","u"];
    //rétrograde,mondial
var diversFonts=["@","ï"];

//EnigmaAstrology.ttf (inutilisable car ne gère pas les nombres, pour affichage tableaux transits)
    //Soleil-Pluton(0-9),NN(10),Lilith(11),NS(12),AS(13),MC(14)
/*var planetesFonts=["a","b","c","d","f","g","h","i","j","t","{",",","}","A","M"];
var signesFonts=["1","2","3","4","5","6","7","8","9","0","-","="];
var aspectsFonts=["B","G","I","F","E","D","J","H","C"];
    //rétrograde,mondial
var diversFonts=["R","e"];*/

/* Audio : dans la gamme pythagoricienne
le si (B) est attribué à Saturne, le do (C) à Jupiter, le ré (D) à Mars, le mi(E)  au Soleil, le fa (F) à Mercure, le sol (G) à Vénus et le la (A) à la Lune*/ 
var planetesNotes=[];
    planetesNotes[0]=["E","A","F","G","D","C","B","B","C","D"];//notes de Soleil à Pluton
    planetesNotes[1]=["E","A#","F#","G#","D#","C#","B","B","C#","D#"]; //dièses
//autre méthode : "correspondance entre planetes et notes de musique - calculs precis.pdf"
//planetesNotes[0]=["G","E","C","D","F","A","B"];
var air=[];//transits du jour
air[0]=[];//transitée
air[1]=[];//transitante
air[2]=[];//rétrograde (0 ou 1)
var tableNotes=[];//contient l'url des notes (évite recréation)
var totalNotes=0;

//secteurs radians=(Math.PI/180)*degrés : (degrés de 0 à 360)
var dignites={
   maitrise : [[4],[3],[2,5],[1,6],[0,7],[8,11],[9,10],[9,10],[8,11],[0,7]],//[planete[signe]]
   exil: [[10],[9],[11,8],[0,7],[1,6],[2,5],[3,4],[3,4],[2,5],[1,6]],
   chute: [[6],[7],[2,11],[5,8],[3,10],[4,9],[0,1],[20],[20],[20]],
   exaltation: [[0],[1],[5,8],[2,11],[4,9],[3,10],[6,7],[20],[20],[20]]
}
//planetes maîtresses, exil,etc en fonction des signes; ex pMaitre[0]=belier=4(mars) et 9(pluton), pMaitre[2]=gemeaux=2(mercure)
var pMaitre=[[4,9],[3],[2],[1],[0],[2],[3],[4,9],[5,8],[6,7],[6,7],[5,8]];
var pExil=[[3],[4,9],[5,8],[6,7],[6,7],[5,8],[4,9],[3],[2],[1],[0],[2]];
var pChute=[[6],[6],[2],[4],[5],[3],[0],[1],[3],[5],[4],[2]];
var pExaltation=[[0],[1],[3],[5],[4],[2],[6],[6],[2],[4],[5],[3]];

//chaine planetaire
var chaine={
    racine:[],
    signe:[],
    dispositeur:[],
    dispose:[]
}

//uranien
var roueAngle=90,
    roueIndice=4,
    planeteDefaut=14, //affiche aspects MC par défaut 
    arcSolaire,
    miPoint=[],
    formule=[],
    affiDate=1,
    affiHeure=1,
    copie=[],
    soleilNatal,
    soleilNatalBis,
    soleilProgresse,
    typeRef=0,//type dernière planète pointée (0:interne, 1:externe)
    MP=0,
    listeBlanche=[],//mi-points (0) ou formules (1)
    NatalExterne=0;//0:données thème, 1:données externes

var posPlanete=[],//natal ou live 
    posPlaneteNatal=[],
    posMaison=[163,185,214,248,285,317,343,6,34,69,105,137,163],
    posMaisonNatal=[],
    posMaisonNatalBis=[],
    posMaisonProgresse=[],
    posMaisonSave=[],
    posDom=[],
    maxPl=14, //nombre max de planètes
    retro=[],
    retroNatal=[],
    lPlanete=[],
    hPlanete=[],
    lMaison=[],
    hMaison=[],
    lSigne=[],
    hSigne=[],
    lLive=[],
    hLive=[],
    lInt=[],
    hInt=[],
    planeteHabite=[],
    planeteGouverne=[0,0,0,0,0,0,0,0,0,0,0],
    gouverneurs=[],
    listAspects=[],
    orbTab=[],
    retroTab=[],
    stationTab=[],
    dsa=[],
    RA=[],
    ecl;

//ex utilisation : dominante["binaire"[0], dominante["binaire"[1], dominante[a[j]][0].length, dominante[a[j]][0][i]
var dominantes={
        binaire: [], //yang,yin[planètes personnnelles,collectives]
        ternaire: [], //cardinal,fixe,mutable
        quaternaire: [], //feu,terre,air,eau
        qualite: [], //sec,humide
        primarite: [], //primaire,secondaire
        hemicyclesAB: [], //A,B
        hemicyclesCD: [] //C,D
   };
   
var signes_indiv=[];
var bulles=[[0,2,4,6,8,10],[1,3,5,7,9,11],[0,3,6,9],[1,4,7,10],[2,5,8,11],[0,4,8],[1,5,9],[2,6,10],[3,7,11],[0,3,4,7,8,11],[1,2,5,6,9,10],[0,1,2,9,10,11],[3,4,5,6,7,8],[6,7,8,9,10,11],[0,1,2,3,4,5]];
 /*   yang: [0,2,4,6,8,10]=belier,gemeaux,etc.
    yin,cardinal,fixe,mutable,feu,terre,air,eau,primaire,secondaire,A,B,C,D=bélier à vierge}*/

var asc,
    ascDef=180,
    AS,//offset en degrés de l'ascendant
    AStemp,
    r2d = 180 / Math.PI,
    d2r=Math.PI/180;
 
//progressé
var ecartJour=[],
    ecartProg;
    
//marge droite
var margePl=[];

//ok
var okProgresse=0,
    okTransits,
    okSynastrie,
    okUranien=0,
    okArc,
    okRS,
    okMix,
    okDynamique=0;
    
//********************************* traductions *****************************************************
    var Planetes=browser.i18n.getMessage("planetes").split(",");
    var Signes=browser.i18n.getMessage("signes").split(",");
    var Aspects=browser.i18n.getMessage("aspects").split(",");
    var labelOptions=browser.i18n.getMessage("options").split(",");
    var placeDef=labelOptions[2]; //ville par défaut non définie

    var labelsGauche=browser.i18n.getMessage("labelsGauche").split(",");
    var labelsCentre=browser.i18n.getMessage("labelsCentre").split(",");
    var labelsDroite=browser.i18n.getMessage("labelsDroite").split(",");
    var labelsDominantes=browser.i18n.getMessage("dominantes").split(",");
   
    var labelsChine=browser.i18n.getMessage("chine").split(",");
    var labelsUranien=browser.i18n.getMessage("uranien").split(",");
   
function traductions(){
    //cadre gauche
    var a=["nom","date","heure","lieu","effacer","1"];
    var b=[0,1,2,3,5,11];
    for (var i=0;i<b.length;i++){
        document.getElementById(a[i]).textContent=labelsGauche[b[i]];
    }
    //cadre centre
    a=["maintenant","maisons","theme","choixTransitsProgresseNatal","choixTransitsProgresseProgresse","choixTransitsMondiaux","labelThemeProgresse","labelMaisonsNatales","labelEquationTemps","idUranien","idTitre","labelTableauUranien","labelThemeRS"];
    b=[0,1,2,3,4,5,8,9,15,17,18,19,20];
    for (i=0;i<b.length;i++){
        document.getElementById(a[i]).textContent=labelsCentre[b[i]];
    }
    //cadre droit
    a=["labelCache0","labelCache1","labelCache2","labelLuminosite","jma"];
    b=[23,24,25,26,29];
    for (i=0;i<b.length;i++){
        document.getElementById(a[i]).textContent=labelsDroite[b[i]];
    }
    //dominantes
    a=["label2Choix3","label3Choix3","label4Choix3","labelDonneesPositions","labeldonneesDominantes","labeldonneesChaine","labeldynamique"];
    b=[19,20,21,28,23,24,31];
    for (i=0;i<b.length;i++){
        document.getElementById(a[i]).textContent=labelsDominantes[b[i]];
    }
    
    //uranien
    a=["labelDonneesTheme","labelDonneesTransit"];
    b=[4,5];
    for (i=0;i<b.length;i++){
        document.getElementById(a[i]).textContent=labelsUranien[b[i]];
    }
    //options mi-points en uranien
    selectMP.item(0).innerText=labelsUranien[16];//marche aussi avec ".label" sauf sur android
    selectMP.item(1).innerText=labelsUranien[13];
    selectMP.item(2).innerText=labelsUranien[7];
    //options transits en uranien
   // optTransits.label=labelsUranien[19] + " (2 max)";//couronne
    selectTransits.item(0).innerText=labelsDroite[35];
    selectTransits.item(2).innerText=labelsUranien[3];
    selectTransits.item(3).innerText=labelsCentre[20];
    selectTransits.item(4).innerText=labelsDroite[34];
    selectTransits.item(5).innerText=labelsDroite[30];
    
    boutonBascule.value=labelsCentre[6];
    labelCoeff.textContent=labelsCentre[7]+" "+String.fromCharCode(177);
    labelCoeff2.textContent=labelsCentre[12];
}

//***************************exécution au démarrage***************************************** 
 
 //  workerEphemerides1.postMessage('blabla');
  // console.log('envoi demande chargement éphémérides1');     
   
//**********************************initialisations******************************************
/* generic error handler */
function onError(error) {
  console.log(error);
}

//création object
function ListAspects(){
  this.planete = [];
  this.aspect = [];
  this.couleur = [];
  this.orbe = [];
}

function tm(){
    return okTransits+okMix;
}

function terminaison(texte,fin){
    if (texte.endsWith(fin)==true){texte=texte.slice(0,texte.length-fin.length)}
    return texte;
}

function canvasCache(choix){
    if (canvasPhases) canvasPhases.hidden=true; 
    if (okUranien==0) margeDiv.hidden=choix; 
    if (choix==true) svg.style.display="none";
    else svg.style.display="";
}

function convPos2DegSigne(z){
    var negatif="";
    if (z<0) {
        negatif="-";
        z=Math.abs(z);
    }
    var x=z%30;
    var deg=Math.floor(x);
    var minutesFull=60*(x-deg);
    var minutesLow=Math.floor(minutesFull);
    var minutes=Math.round(minutesFull);
    var secondes=Math.round(60*(minutesFull-minutesLow));
    var degSec=deg;
    var inc=0;
    
    //utile si minutes=60
    if (minutes >=60){
        deg+=1;
        if (deg >= 30) {
            deg=0;
            inc=1;
        }
        if (minutesFull>=60) degSec+=1; // si affichage des secondes, pas d'arrondissement des minutes donc pas d'augmentation d'1 degré si minutes réelles <60
        minutes=0;
    }
     //utile si secondes=60
    if (secondes >=60){
        minutes+=1;
        secondes=0;
    }
    var min=ajoutZero(String(minutes));
    if ((minutes-minutesLow)>1) minutesLow+=1; //sinon passage de x'59 à x+1'00 par x'00 !...
    var minReel=ajoutZero(String(minutesLow));
    var sec=ajoutZero(String(secondes));
    var y=Math.floor(z/30)+inc; //inc=1 si passage à 0deg pour incrémenter le signe sinon le changement de signe est décalé
    if (y==12)y=0;//passage poissons à belier
    var signe=Signes[y];
    return {
        degres: negatif+deg+"°"+min+"'",
        signe: y,
        secondes:negatif+degSec+"°"+minReel+"'"+sec+'"'
    };
}

function convPos2DegMaison(z){
    var pos0,pos1;
    //j=maison habitée
    for (var j=0;j<=12;j++){
        pos0=posMaison[j];
        pos1=posMaison[j+1];
        if (posMaison[j]>posMaison[j+1]){
            if (z>pos0){
                z-=360;
            }
            pos0-=360;
        }
        if (z>=pos0 && z<pos1) break;
    }
    //position en deg.min / début maison
    var x=z-pos0;
    var deg=Math.floor(x);
    var minutesFull=60*(x-deg);
    var minutesLow=Math.floor(minutesFull);
    var minutes=Math.round(minutesFull);
    var secondes=Math.round(60*(minutesFull-minutesLow));
    var inc=0;
    
    //utile si minutes=60
    if (minutes >=60){
        deg+=1;
        if (deg >= 30) {
            deg=0;
            inc=1;
        }
        minutes=0;
    }
     //utile si secondes=60
    if (secondes >=60){
        minutes+=1;
        secondes=0;
    }
    var min=ajoutZero(String(minutes));
    return {
        maison: j+1,
        degres: deg+"°"+min+"'"
    };
}

function convDegresMinutestoDegresDecimal(lat,long){
    //latitude
    var x=Math.abs(lat);
    var y=Math.floor(x);
    latitude=y+((x-y)*10/6);
    if (lat<0) latitude=-latitude;
    //longitude
    x=Math.abs(long);
    y=Math.floor(x);
    longitude=y+((x-y)*10/6);
    if (long<0) longitude=-longitude;
    
    return [latitude,longitude];
}

// conversion d'un entier en nombre romain
//source : https://www.developpez.net/forums/d1276262/webmasters-developpement-web/general-conception-web/contribuez/conversion-chiffres-arabes-chiffres-romains-inversement/
function AtoR( nb ){
    var A = [ 1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1 ],
	R = [ "M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I" ],
	Alength = A.length;
	// on s'assure d'avoir un entier entre 1 et 3999.
	var x = parseInt( nb, 10 ) || 1,
		str = "";
 
	if ( x < 1 ){
		x = 1;
	} else if ( x > 3999 ){
		x = 3999;
	}
 
	// pour chaque A[ i ], tant que x est supérieur ou égal on déduit A[ i ] de x.
	// arrêt de la boucle si x == 0
	for ( var i = 0; i < Alength; ++i ){
		while ( x >= A[ i ] ){
			x -= A[ i ];
			str += R[ i ];
		}
 
		if ( x == 0 ){
			break;
		}
	}
 
	return str;
}
 
 

//****************************************** tracé zodiaque ******************************************************
//ajustement canvas en position et taille
//source : http://jsfiddle.net/9Rmwt/11/show/ 
function setCanvas(canevas){       
    var pw = canevas.parentNode.clientWidth;//différent pour tropical ou uranien !
    //=0 !  var ph = canvasNode.parentNode.clientHeight;
    okUranien==0 ? canevas.height = pw*0.9 : canevas.height = pw*0.9;
    okUranien==0 ? canevas.width = pw : canevas.width = 1.23*pw;
    canevasWidth=canevas.width;
    canevasHeight=canevas.height;
     //top : éventuellement à augmenter si on affiche plus d'options (theme,tansits,sysnastrie,etc.)- corriger centre en Y  dans dessins du même chiffre  (modifié en css)
    canevas.style.top = 0 + "px";//voir css, n'agit pas ici
    canevas.style.left = 0 + "px";
}

function creeCanvas(id){
    var nested = document.getElementById(id);
    if (nested) {garbage = canvasTarget.removeChild(nested);}
    var canevas = document.createElement('canvas');
    canevas.setAttribute("Id",id);
    canvasTarget.appendChild(canevas);
    setCanvas(canevas);
    var ctx= canevas.getContext("2d",{willReadFrequently: true});//ajout pour éliminer messages d'avertissement avec Chrome
    return [canevas,ctx];
}

//tracé secteurs signes du zodiaque
function secteursSignes(){
    var x;
    for (var i=0;i<=3;i++){
        if (choix3.item(i).checked){
         //conditional operator, syntaxe : condition ? vrai : faux
            i==0 ? x=12 : x=i+1;
            break;
        }
    }
    couleurSecteurs(x);
    //légende
    var y=[[0,1],[2,4],[5,8]];
    x-=2;
    if (x<10)LegendeElements(y[x][0],y[x][1]);
}

/*  ajouté async et await sinon messages sur chrome : [Violation] Forced reflow while executing JavaScript took 34ms*/
async function dessins() {
    var fonte=1+"vw serif",
        angleHorizon=0,
        numMaison,
        maisonsUranien=[4,10,,,,,,,,,1,,,1,10,,,,,,,,,7];
        tableau.hidden=true;
    efface(svg);
    svg.style.display="";//afiche svg
    
    //création canvas (conserver, pemet d'effacer à chaque passage)
    [canvasPhases,ctx]=creeCanvas("canvasPhases");
    canvasPhases.hidden=true; 
    
    //laisser après la création des canvas sinon non affiché
    titreCanvas.textContent= checkDom.checked ? "domitude - "+titreTheme : titreTheme ; 
  
    //positionnement du thème 
    rayon=0.22*canevasWidth;
    centre= okUranien==0 ? [1.9*rayon,0.36*canevasHeight] : [1.8*rayon,0.44*canevasHeight];
    
    //position AS (radians)
    AS=d2r*(asc-180);
    if (isNaN(AS)){//évite blocage programme ! 
        console.log ("erreur AS, zodiaque.js dessins",AS);
        return;
    }
        //si horizon (affichage aligné sur ciel local)
        if (!checkMaisons.checked  && titreTheme.search("Zodia")==0 && !okUranien && !fixePhases && !okTransits){
            angleHorizon=d2r*hrz;
            AS+=angleHorizon;
            //pointeurs verticaux
            svgLigne("lignes",centre[0],centre[1]-1.10*rayon,centre[0],centre[1]-rayon,"gray");
            svgLigne("lignes",centre[0],centre[1]+1.10*rayon,centre[0],centre[1]+rayon,"gray");
            //pointeurs rotatifs sud (+angle) et nord
            let a=["S",,"N"];
            let b=[-1.0,-1.1,1.0,1.1];
            let L1,H1,L2,H2;
            for (var i=0;i<=3;i+=2){
                L1=centre[0]+b[i]*rayon*Math.sin(-angleHorizon);
                H1=centre[1]+b[i]*rayon*Math.cos(-angleHorizon);
                L2=centre[0]+b[i+1]*rayon*Math.sin(-angleHorizon);
                H2=centre[1]+b[i+1]*rayon*Math.cos(-angleHorizon);
                svgLigne("lignes",L1,H1,L2,H2,"gray");
                if (i==0) svgTexte("x","angle","\u00A0"+String(hrz)+"°",L2,H2-0.8*taille,fonte,"bold");//"\u00A0"=espace
                svgTexte("x",a[i],a[i],L2,1.01*H2,fonte,"bold");
            }
            //pointeurs horizontaux
            svgLigne("lignes",centre[0]-rayon,centre[1],centre[0]-1.10*rayon,centre[1],"gray");
            svgLigne("lignes",centre[0]+rayon,centre[1],centre[0]+1.10*rayon,centre[1],"gray");
        }
    //initialisations
    if (okUranien==0) {
        //tableaux résumés
        if (okDynamique==0){//très important sinon saturation processeur ! (à cause de await...)
            await tableauDominantes(NatalExterne);//appelle tableauResume() etc.
        } 
        //secteurs signes (couleurs en fonction de choix3:normal,binaire,etc.)
         if (!checkDom.checked) secteursSignes();  
   }
   
    else if (okUranien==1) {
        await tableauUranien(NatalExterne);
        //cercle externe (le cercle interne est dessiné avec la croix)
        svgCercle("ur",centre[0],centre[1],1.05*rayon);
        //graduations et chiffres associés
        var x,y,z,t;
        var angle,miAngle;
            //départ au point vernal
            roueAngle==360 ? z=0 : z=-Math.PI/2;
            //une annotation tous les 20deg en 360, tous les 10/4=2.5 deg en 90, tous les 10/16 deg en 22.5
            for (var i=0;i<=356;i+=4){
                angle=z-d2r*i;
                //graduation principale (=5 sur roue 360)
                if (i%20==0){
                    //chiffres des graduations
                    //*100/100=arrondi à 2 décimales (sans 0 si entier)
                    t=Math.round(i/roueIndice*100)/100;
                    if (roueAngle==22.5) t=cDdDm(t);
                    x=centre[0]+Math.cos(angle)*1.11*rayon;
                    y=centre[1]+Math.sin(angle)*1.11*rayon; 
                    svgTexte("repere","ur_marque",t,x,y,fonte,"bold");
                    //marque
                    x=centre[0]+Math.cos(angle)*rayon;
                    y=centre[1]+Math.sin(angle)*rayon; 
                    urGraduation(x,y,angle,8);
                    continue;
                }
                //graduation secondaire
                else{
                    x=centre[0]+Math.cos(angle)*(1.02*rayon);
                    y=centre[1]+Math.sin(angle)*(1.02*rayon); 
                    urGraduation(x,y,angle,4);
                }
            } 
    }
    //début
        //maisons
        for (var i=0; i <=11 ; i++){
            if (checkMaisons.checked==false || (okUranien==1 && roueAngle!=360)) break;
            
            //ecart0=écart/origine (maison 1 à 180 deg.en tropical sauf si horizon actif)
            var ecart0= okUranien==0 ? posMaison[i]-posMaison[0] : 360-posPlanete[0][planeteDefaut]-(i*30);
            if (ecart0<0) ecart0+=360;
            var startAngle= okUranien==0 ? Math.PI-(d2r*ecart0)+angleHorizon : d2r*ecart0;
            
            //ecart1=écart entre 2 positions maisons
            var ecart1= okUranien==0 ? posMaison[i+1]-posMaison[i] : 30;
            if (ecart1<0) ecart1+=360;
            var endAngle=startAngle-(d2r*ecart1);
            
            //annotations axes en tropical
            if (okUranien==0){
                let lMC=centre[0]+Math.cos(startAngle)*1.08*rayon;
                let hMC=centre[1]+Math.sin(startAngle)*1.08*rayon;
                //AS
                if (i===0){
                    svgTexte("maison","maison_0",convPos2DegSigne(posMaison[0]).degres+" AS", 0.85*lMC, 0.99*hMC,fonte,"bold");
                } 
                //FC
                else if (i===3){
                    svgTexte("maison","maison_3","FC ", lMC, hMC,fonte,"bold");
                }
                //DS
                else if (i===6){
                    svgTexte("maison","maison_6","DS ", 1.01*lMC, 0.99*hMC,fonte,"bold");
                } 
                //MC
                else if (i===9){
                    svgTexte("x","maison9",convPos2DegSigne(posMaison[9]).degres, lMC, hMC-taille/2,fonte,"bold");
                    svgTexte("maison","maison_9","MC", lMC, hMC,fonte,"bold");
                } 
            }
           
            //cercle externe + secteurs maisons
            var x0=centre[0]+Math.cos(startAngle)*1.04*rayon;
            var y0=centre[1]+Math.sin(startAngle)*1.04*rayon;
            var x1=centre[0]+Math.cos(endAngle)*1.04*rayon;
            var y1=centre[1]+Math.sin(endAngle)*1.04*rayon;
            svgArc ("arc_maison",x0,y0,rayon,x1,y1,centre[0],centre[1],"transparent","gray");

            //cercle interne (autour des signes)
            if (i==0 && !checkDom.checked) svgCercle("x",centre[0],centre[1],0.33*rayon);
              
            //en uranien, cercle interne
            if (okUranien)svgCercle("x",centre[0],centre[1],0.1*rayon);

            //numéros maisons
            if (okUranien==0){
                lMaison[i]=centre[0]+Math.cos((startAngle+endAngle)/2)*1.05*rayon;
                hMaison[i]=centre[1]+Math.sin((startAngle+endAngle)/2)*1.05*rayon; 
            }
            if (okUranien==1){
                lMaison[i]=centre[0]+Math.cos((startAngle+endAngle)/2)*0.25*rayon;
                hMaison[i]=centre[1]+Math.sin((startAngle+endAngle)/2)*0.25*rayon; 
            }
            if (okUranien==0) numMaison=i+1;
            else if (okUranien==1){
                if (i==0){
                    if (typeof(maisonsUranien[planeteDefaut])!="undefined") numMaison=maisonsUranien[planeteDefaut];
                    else numMaison=0;
                }
                if (i>=1 && numMaison>0) numMaison+=1;
                if (numMaison>=13) numMaison=1;
            }
            //écriture numéro maison - le 8 permet d'aligner les numéros sur le cercle
            if (numMaison && okUranien==0) svgTexte("maison","maison_"+i,AtoR(numMaison),lMaison[i], hMaison[i],fonte);
            else if (numMaison && okUranien==1) svgTexte("maison","maison_"+i,numMaison,lMaison[i], hMaison[i],fonte);
        }//fin maisons
        
        
        //signes et planètes
        for (var i=0; i <=maxPl ; i++){
            //signes
            if (i>=0 && i <=11 && okUranien==0 && !checkDom.checked) {
                //position des symboles signes (x/y)
                lSigne[i]=centre[0]+Math.cos(i*Math.PI/6+Math.PI/12-AS)*rayon/4;
                hSigne[i]=centre[1]-Math.sin(i*Math.PI/6+Math.PI/12-AS)*rayon/4;
                //écriture symboles signes svg
                svgTexte("signe","signe_"+i,signesFonts[i],lSigne[i],hSigne[i],1+"vw Zodiac");
            }
        //planètes
            if (isNaN(posPlanete[0][i])) continue;
            //lilith, NS exclus en uranien
            if ((i==11 || i==12) && okUranien==1){
                lPlanete[i]=hPlanete[i]=NaN;
                continue;
            }
            //AS-MC
            if ((i==13 || i==14) && checkAsMc.checked==false) continue;
            
            //position des symboles planètes (x/y) avec décalage si superposition
            let position= checkDom.checked ? posDom[0] : posPlanete[0];
            [lInt[i],hInt[i]]=[lPlanete[i],hPlanete[i]]=calcXYPlanete(i,position,AS,svg);
            
            //choix fonts
            var couleur;
            okUranien==0 ? fonte=1.2+"vw Zodiac" : fonte=urFont(1,i)+"vw Zodiac";
            okUranien==0 ? couleur="rgb(100,100,100)" : couleur="mediumblue";
            if (okUranien==1 && roueAngle==360 && checkMaisons.checked==true && i==planeteDefaut) couleur="red";
            
            //symboles planètes
            svgTexte("planete","planete0_"+i,planetesFonts[i],lPlanete[i],hPlanete[i],fonte,couleur);
 
            //rétrograde ?
            if (retro[0][i]==1){
                svgTexte("x","r0",diversFonts[0],lPlanete[i]+taille/2,hPlanete[i],1+"vw Zodiac",couleur);
            }
        }//fin signes et planètes
    
    //choix sortie
    if (okTransits) affiLive(AS); //laisser avant globalAspects sinon aspects live-theme non tracés
    okUranien==0 ? globalAspects() : uranienAspects(planeteDefaut); 
    if (okUranien) urCroix(planeteDefaut,typeRef);    
}

//********** gestion svg *****************

function svgArc (cl,x0,y0,r,x1,y1,x2,y2,f,s){//classe,départ,rayon,arrivée,centre,couleur,bordure
    let q = document.createElementNS (ns,'path');
    var c=0;//c=0:ccw
    if (s=="transparent") c=1;//secteurs signes
    if (!f) f="green";
    if (!s) s="black";
    q.setAttribute ('d',"M"+ x0 +" "+ y0 +" A" + rayon + " " + rayon + " 0 0 "+ c + x1 +" "+ y1 + " L" + x2 + " " + y2);
    q.setAttribute ("stroke",s);//bordure
    q.setAttribute ("class",cl);//bordure
    q.style.fill=f;
    svg.appendChild(q);
}
function svgLigne(cl,x1,y1,x2,y2,c,pl){//classe,départ,arrivée,couleur,planètes de l'aspect
    let q = document.createElementNS(ns,'line');
    q.setAttribute("x1",x1);
    q.setAttribute("y1",y1);
    q.setAttribute("x2",x2);
    q.setAttribute("y2",y2);
    q.setAttribute ("class",cl);
    if (!c) c="black";
    q.style.stroke=c;
    if (pl)q.setAttribute("id",pl);
    svg.appendChild (q);
}
function svgCercle(cl,x,y,r,c){//classe,centre,rayon
    let q = document.createElementNS(ns,'circle');
    q.setAttribute ('cx',x);
    q.setAttribute ('cy',y);
    q.setAttribute ('r',r);
    q.setAttribute ('class', cl); 
    if (!c) q.style.fill="transparent";
    else q.style.fill=c;
    q.style.stroke="gray";
    
    svg.appendChild(q);
}
function svgTexte(cl,id,t,x,y,f,c,r){//classe,id,texte,position,font,couleur,rotation
    //"x","liste","maison","signe","planete0" pour le thème,"planete1" et "planete2" pour les transits
    let q = document.createElementNS(ns,'text');//createElementNS : très important sinon pas de texte
    q.setAttribute ('x', x);
    q.setAttribute ('y', y);
    if (cl!="x") q.setAttribute ('dy', ".5em");//centre verticalement, ne pas modifier ! sinon icônes décalés / aspects !
    q.textContent=t;
    
    if (!f) f = 0.8+"vw serif";
    q.style.font=f;
    if (r){//annotaton rotative en mode "horizon" (adapter à la longueur du texte)
        q.style.transformOrigin=String(x/r)+"px "+String(y)+"px";
        q.style.transform="rotate("+hrz+"deg)";
    }
    if (!c) c="rgb(100,100,100)";//"black";
    q.style.fill=c;

    if (cl=="liste2") q.setAttribute ("text-anchor","left");//liste2=liste de gauche
    else q.setAttribute ("text-anchor","middle");
    q.setAttribute ("class",cl);
    q.setAttribute ('id',id);
    
    //listeners
    if (cl.search("maison")!=-1 || cl.search("signe")!=-1 || cl.search("planete")!=-1){
        q.addEventListener("mousemove",svgListener,false);
        q.addEventListener("mouseout",svgListenerOut,false);
    }
    svg.appendChild(q);
}
function svgRectangle(cl,x,y,w,h,c){//classe,départ,largeur,hauteur,couleur
    let q = document.createElementNS(ns,'rect');//createElementNS : très important sinon pas de texte
    q.setAttribute ('x', x);
    q.setAttribute ('y', y); 
    q.setAttribute ('width', w); 
    q.setAttribute ('height', h); 
    q.setAttribute ('class', cl); 
    q.style.fill=c;
    svg.appendChild(q);
}
function svgEfface(a){//suppression d"éléments (par classes par ex.)
    z=svg.querySelectorAll(a);
    z.forEach((item) => {
        item.remove();
    })    
}

//*************** fin gestion svg ***************

//*********** uranien *************
 
function urGraduation(x,y,angle,l){//x/y:position, l:longueur du trait
    let q = document.createElementNS (ns,'path');
    var x1=centre[0]+Math.cos(angle)*(1.05*rayon);
    var y1=centre[1]+Math.sin(angle)*(1.05*rayon); 
    var s="black";
    q.setAttribute ('d',"M"+ x +" "+ y +" A" + rayon + " " + rayon + " 0 0 0 " + x1 +" "+ y1);
    q.setAttribute ("stroke",s);//bordure
    q.style.fill="black";
    svg.appendChild(q);
}

//uranien - tracé croix (2 axes perpendiculaires passant par la planète x et le centre) + cercle intérieur
function urCroix(x,type,L,H){//x=planete ou roue libre si=-1, type 0=interne, 1 et 2=externe,L-H=position souris en roue libre
    svgEfface(".croix");
        var slope,y,z,h,l,s2,r,ctx,message,rl;
        if (!L) L=0;
        if (!H) H=0;
        
        if (x>=0){//survol planete
            if (type==0){
                l=lPlanete[x]-centre[0];
                h=hPlanete[x]-centre[1];
            }
            else{
                l=lLive[type][x]-centre[0];
                h=hLive[type][x]-centre[1];
            }
        }else{//roue libre
            l=L-centre[0];
            h=H-centre[1];
        }
        //longueur variable du rayon r de la croix
        if (okTransits==0) r=1.05*rayon;//theme seul+planete interne : longueur fixe
        else if (okTransits && type==0) r=1.5*rayon;//transits+planete interne : longueur fixe
        else r=Math.sqrt(h*h+l*l);//transits+planete externe : longueur adaptée à la position exacte de la planete
        //tracé croix
        slope=h/l;

        for (var i=1;i<=2;i++){
            if (slope==-Infinity) slope=1000000; //axe vertical
            s2=slope*slope;
            z=Math.sqrt(r*r/(1+s2));
            y=slope*z;

            svgLigne("croix",centre[0], centre[1],centre[0]+z,centre[1]+y,"red");
            svgLigne("croix",centre[0], centre[1],centre[0]-z,centre[1]-y,"red");
            
            //pente axe perpendiculaire
            slope=-1/slope;
        } 
        //position planete sélectionnée ou souris
        var fonte=0.8+"vw Serif";
        [message,rl]=messagePosPlanete(x,type,L,H);
        textMultiLine(message, 20, rayon/5);
        //tableau MPs correspondant à la position rl de la croix
        if (!MP && x==-1) uranienAspects(rl,typeRef,1);
        //cercle interne
        svgCercle("croix",centre[0],centre[1],0.1*rayon,"white");
        //valeur roue dans cercle
        svgTexte("croix","ur_centre",roueAngle+"°",centre[0],centre[1]-2); 
}

//change taille fonts pour planètes uraniennes (sauf apollon,vulcain,poseidon et point vernal)
function urFont(taille,x){
        var y;
        if (x<=14 || x==19 || x==21 || x==22 || x==23) y=taille;
        else y=1.3*taille;//+3;
        return y;
}

//********* fin uranien ****************

function calcXYPlanete(i,position,ascendant,canevas,offset){
    if (isNaN(position[i])) return;
    if (!offset) offset=0.93; //offset<1 intérieur, >1 exterieur
    var x,y,sortie,total,r1,r2,pos;
    //position fonction de la roue en uranien
    pos= okUranien==0 ? position[i] : position[i]%roueAngle*roueIndice;

    do {
        sortie=1;
        x=centre[0]+Math.cos(d2r*pos-ascendant)*rayon*offset;
        y=centre[1]-Math.sin(d2r*pos-ascendant)*rayon*offset;
        if (offset==0.94) return [x,y];//mondial
        //en live, contourne la position AS
        if (okUranien==0 && offset>1 && y<centre[1]+taille && y>centre[1]-taille && x<centre[0]) x-=taille*2;
        
        //vérifie si emplacement déjà occupé
        switch(canevas){
            case svg:
                //sensibilité collision
                var seuil=20*svg.width.baseVal.value /1122;//20 est ok aussi !
                //écrit puis efface un caractère témoin
                svgTexte("test","test","A",x,y,1.2+"vw Zodiac");
                var j=svg.querySelector(".test");
                r1=j.getBoundingClientRect();
                if (r1.x==0 || r1.y==0) break;//anormal ! bloque le programme...(si chgt date+ fixedphase sur firefox 2.6, pas sur chrome - normalement résolu par await dessins dans changedateheure)
                let x1=r1.left+0.5*r1.width;//centre icone planete
                let y1=r1.top+0.5*r1.height;
                j.remove();
                
                //compare aux autres planètes
                var l=svg.querySelectorAll(".planete");
                if (l.length==0) break;
                for (var k of l){
                    r2=k.getBoundingClientRect();
                    let x2=r2.left+0.5*r2.width;
                    let y2=r2.top+0.5*r2.height;
                    let collision=(Math.abs(x1-x2) < seuil && Math.abs(y1-y2) < seuil);//true and true =true
                    if (collision){
                        offset+=(offset-1)/8;
                        sortie=0;
                        break;
                    }
                }
                break;
            case canvasPhases:
                var ctx= canevas.getContext("2d");
                var myImageData,data;
                //récupère l'image de l'emplacement : 4 octets par pixel (rgba)
                myImageData = ctx.getImageData(x-taille/3, y-taille/3, taille*2/3, taille*2/3);
                data=myImageData.data;
                // a>0  et r=g=b=0 ? = 1 pixel en noir = 1 planète déjà présente
                for (var j=0;j<data.length;j+=4){
                    if (data[j+3]>0){
                        if (data[j]==0 && data[j+1]==0 && data[j+2]==0){
                            offset+=(offset-1)/8;
                            sortie=0;
                            break;
                        }
                    }
                }
                break;
        }
    } while (sortie==0);
    return [x,y];
}

 //affiche planetes en transit à l'extérieur du thème et enregistre les transits en conjonction (orange ou rouge) pour affichage avec "souris.js"
function affiLive(ascendant){
    var posX,posY,position,gap,orbe,fonte,couleur;
    //planetes du jour
    for (var i=0;i<=maxPl;i++){
        if (i==12 || isNaN(posPlanete[1][i])) continue; //NS
        if (i==11 & okUranien==1) continue; //lilith
        okUranien==0 ? fonte=1+"vw Zodiac" : fonte=urFont(1,i)+"vw Zodiac";
        couleur="green"; //"#008000"
        //planetes du theme - recherche transits par conjonction en tropical
        if (okUranien==0 && okMix==0) {
            for (var j=0;j<=maxPl;j++){ //13,14=AS,MC
                if (j==12 && isNaN(posPlanete[0][j])) continue; // NS
                if ((j==13 || j==14) && checkAsMc.checked==false) continue; //AS-MC
                gap= checkDom.checked ? Math.abs(posDom[0][j]-posDom[1][i]) : Math.abs(posPlanete[0][j]-posPlanete[1][i]);
                if (gap<=1){
                    if (couleur=="#008000") couleur="orange";
                    if (gap<=1/360) couleur="red";
                }
            }
        }
        //affiche planete (1er ou 2ème transit)
        for (j=1;j<=okMix+1;j++){
            position= checkDom.checked ? posDom[j] : posPlanete[j];
            [lLive[j][i],hLive[j][i]]=[posX,posY]=calcXYPlanete(i,position,ascendant,svg,1.2); //(1.15=offset s'éloigne du cercle)
            //copie positions (utilisé par survol pour les transits)
            if (okMix==0) [lLive[2][i],hLive[2][i]]=[posX,posY];
            //2eme transit ?
            if (j>1) couleur="orange";
            //dessin
            svgTexte("planete","planete"+j+"_"+i,planetesFonts[i],posX,posY,fonte,couleur);
            //retrograde ?
            if (retro[j][i]==1){
                svgTexte("x","r1",diversFonts[0],posX+taille/2,posY,fonte,couleur);
            }
        }
    }
}


//********************************************** calcul, affichage, sauvegarde aspects ******************************************
function uranienAspects(ref,type,survol){//ref : planete, type 0:interne, 1,2:externe, -1:roue libre
var tt,
    y=[0,roueAngle/2,roueAngle/4,roueAngle*3/4],//4 angles de la croix
    gap,
    orbe,
    lbMin, //nombre d'éléments sélectionnés en liste blanche
    lb=[], //liste des éléments sélectionnés en liste blanche
    pos0,pos1,pos2,pos3,hmin;
    //sortie anticipée (attention car besoin de sélection int/ext !)
     if (MP && survol && type==typeRef) return;//non, car pb si nbre planetes selectionnees < valeur requise
     
    lbMin=0;
    //nombre d'éléments sélectionnés en liste blanche
    if (MP){
        for (x=0;x<listeBlanche.length;x++){
            if (listeBlanche[x]==1){
                lb[lbMin]=x;
                lbMin+=1;
            //  if (lbMin>1) lbMax*=x+1;//nombre de comnbinaisons (ex a+b-c, a+c-b, b+c-a)
            }
        }
        //sortie  si pas assez de planetes selectionnees à droite (min=1 et pour formule:min=3)
       // if (lbMin<MP+1) return;
        if (lbMin==0 || (MP==2 && lbMin<3)) return;
    }

    //type de la dernière planète pointée
    //type non défini suite changements : date,heure,thème,transits
    if (typeof(type)=="undefined"){
        type=typeRef;
        calcMiPoints();
        calcFormules();
    }
    //type défini si survol 
    else typeRef=type&&1; //1 max : si double transit empêche que typeref=2
    //type défini si survol 
    
    //mi-points calculées sur interne ou externe ?
        //si planete externe : on se réfère aux mipoints internes
        //si planete interne : on se réfère aux mipoints internes si "theme" sélectionné ou externes si "transits" sélectionné
    tt=okTransits-(type&&1);//&&1:si double transit limite tt à 1
    //pour 2eme transit
    var tt2=tt+okMix;
    if (tt==0) tt2=0;
    var ty2=type+okMix;
    if (type==0) ty2=0;
    
    initAspects();
    
    //orbesDef=orbes stockés (id 9 pour uranien)
    orbe=0.7;
    if (orbesDef.length>=9 && orbesDef[9]) orbe=orbesDef[9];
    if (roueAngle==360) orbe*=3;
    else if(roueAngle==22.5) orbe/=3;
    //orbe=orbe/90*roueAngle;
    
    //g-h : paire résultat
    for (var g=0; g<=maxPl; g++){
        if (g==11 || g==12) continue; //exclus Lilith et NS
        if (okMix==0 || tt2==0) hmin=g;
        else if (okMix) hmin=0;
            
        for (var h=hmin; h<=maxPl; h++){
            if (h==11 || h==12) continue; //exclus Lilith et NS
            
            //planète pointée par la croix (ref)
            if (MP==0){
                //teste si planete ou roue libre
                if (ref%1==0){//planete
                    pos1=ref;
                    if (pos1==g && pos1==h && okTransits==0) continue;
                    pos0=posPlanete[type][pos1]%roueAngle;
                }else {pos0=ref;pos1=24}//roue libre
                //test 4 angles de la croix
                y.forEach(function(item){
                    gap=miPoint[tt2][g][h]-pos0-item;
                    filtreMP(gap,orbe,ref,g,h,pos1);
                });
            }
            //paire définie dans bandeau (ou 1 planete en double, utile pour les doubles transits)
            else if (MP==1){
                pos1=lb[0];
                if (lb[1]) pos2=lb[1];
                else pos2=pos1;
                if (pos1==g && pos1==h && okTransits==0) continue;
                //test 4 angles de la croix
                y.forEach(function(item){
                    gap=miPoint[tt2][g][h]-miPoint[ty2][pos1][pos2]-item;
                    filtreMP(gap,orbe,ref,g,h,pos1,pos2);
                });
            //formule définie dans bandeau
            }else if (MP==2){
                var a=[0,1,2,0,2,1,1,2,0];//permet de tester a+b-c, a+c-b et b+c-a
                for (var i=0; i<a.length; i+=3){
                    pos1=lb[a[i]];
                    pos2=lb[a[i+1]];
                    pos3=lb[a[i+2]];
                    //test 4 angles de la croix
                    y.forEach(function(item){
                        gap=miPoint[tt2][g][h]-formule[type][pos1][pos2][pos3]-item;
                        filtreMP(gap,orbe,ref,g,h,pos1,pos2,pos3);
                    });
                }
            }
        }
    }
    tableauMP(ref,type);
}


function filtreMP(gap,orbe,r,a,b,c,d,e){
    //test si accord
    gap=reboucle(gap,roueAngle);
    gap=Math.abs(gap);
    if (gap>orbe) return;
    //accord trouvé
    if (MP==0) d=-1;
    if (MP<2) e=-1;
    //sauve 5 valeurs dans listAspects.planete
        //planète, paire ou formule définies
        sauveAspect(r,c,0,0,-gap,0);
        sauveAspect(r,d,0,0,0,0);
        sauveAspect(r,e,0,0,0,0);
        //paire résultat
        sauveAspect(r,a,0,0,0,0);
        sauveAspect(r,b,0,0,0,0);
}

function sauveAspect(i,j,k,angle,gap,couleur){
    var x=0;
    if (okUranien==0) x=100*tm();//en tropical, +100 pour le 1er transit ou +200 pour le 2eme (utilisé par survol et affiphase)
   //sauvegarde aspects/planète sujet(i)                      
    listAspects[0].planete[i]+=","+(j+x);
    listAspects[0].aspect[i]+=","+k;
    listAspects[0].orbe[i]+=","+(angle-gap);
    listAspects[0].couleur[i]+=","+couleur;
     
    if (okUranien) return;
    
    //tropical : sauvegarde aspects/planète agent(j), pour theme et transits séparément 
    listAspects[tm()].planete[j]+=","+i;
    listAspects[tm()].aspect[j]+=","+k;
    listAspects[tm()].orbe[j]+=","+(gap-angle);
    listAspects[tm()].couleur[j]+=","+couleur;
}

//tracé des aspects
function traceAspects(){
    //pas d'aspects en domitude (élimine les aspects aussi dans tous les types de transits)
    if (checkDom.checked) return;//commenter pour afficher les aspects en domitude
    
    var l0,h0,l,h,a,pl,co,liste;
    
    for (var k=okTransits; k<=okTransits+okMix; k++){ //0,1 ou 2 transits
        liste=listAspects[k];
        
        for (var i=0; i<=14; i++){ //agent
            a=liste.planete[i];
            pl=a.split(",");
            if (pl.length<=1) continue;
            co=liste.couleur[i].split(","); 
            [l0,h0]= okTransits==0 ? [lInt[i],hInt[i]] : [lLive[k][i],hLive[k][i]];
            
            for (var j=1;j<pl.length;j++){ //sujet
                //intérieur
                [l,h]=[lInt[pl[j]],hInt[pl[j]]];
                svgLigne("aspects",l0,h0,l,h,co[j],"aspect_"+String(i)+"-"+String(pl[j]));
            }
        }
    }
}

//initialisation matrices des aspects (theme et transits)
function initAspects(){
    for (var j=0;j<=2;j++){
        listAspects[j] = new ListAspects();
        for (var i=0; i<=maxPl; i++){ //12,13,14 pour NS,AS,MC
            //"x" permet ensuite de faire aspect...+=
            listAspects[j].planete[i]="x";
            listAspects[j].aspect[i]="x";
            listAspects[j].couleur[i]="x";
            listAspects[j].orbe[i]="x";
        }
    }
}

function globalAspects(){
    //aspect=["conjonction","semi-sextile","semi-carré","sextile","carré","trigone","sesqui-carré","quinconce","opposition","quinconce","sesqui-carré","trigone","carré","sextile","semi-carré","semi-sextile","conjonction"],
    var tolerance,
        gap,
        gapArc,
        decroissant,
        debut,
        coul,
        posX,
        posY,
        [angle,couleur,arc]=definitions(); //voir transits.js
       
    //orbesDef=orbes stockés
    var orbe=[15,2,2,4,8,8,2,0.5,10,0.5,2,8,8,4,2,2,15];
    if (orbesDef.length>=9){
        for (var i=0;i<=16;i++){
            if (i<=8 && orbesDef[i]) orbe[i]=orbesDef[i];
            else if (i>8) orbe[i]=orbe[i-2*(i-8)];//aspects descendants
         }
    }
    //tolerance si transits  
    if (okTransits==1) tolerance=1;
    
    //initialisation des aspects (seulement au 1er passage si 2 transits)
    if (okMix==0) initAspects();

    //planete sujet (toujours du thème affiché) : transitée
    for (i=0; i<=14; i++){ 
        if (okDynamique==1 && i==7) break; //mode dynamique :transits sur planètes individuelles seulement
        if (i==12) continue; //NS
        if (i==13 && checkAsMc.checked==false) break; //AS/MC
        if (isNaN(posPlanete[0][i])){continue}; //si NN, Lilith en-dehors plage 1800-2039
        if (okProgresse && okTransits==0){tolerance=coeffOrbe.value*Math.abs(ecartJour[i])}
        if (okTransits) debut=0;
        else debut=i+1;
        
        //planete agent (du thème ou en transit) : transitante
        for (var j=debut; j<=14; j++){
            if (j==12) continue; //NS
            if ((j==13 || j==14) && checkAsMc.checked==false) continue; //AS/MC
            if (isNaN(posPlanete[tm()][j]) || isNaN(posPlanete[0][j])){continue};
            gap= checkDom.checked ? posDom[0][i]-posDom[tm()][j] : posPlanete[0][i]-posPlanete[tm()][j];
            decroissant=0;
            if ((gap>0 && gap <60) || (gap<0 && gap <-300)) decroissant=1;
            if (gap<0) gap+=360;
            
            gapArc=Math.floor(gap/15);
            //aspects
            for (var k=arc[gapArc][0]; k<=arc[gapArc][1]; k++){
                //Lilith : conjonctions
                if ((j==11) && (k>0 && k<16)) continue; 
                //NN et axes : conjonction, carré, opposition
                if ((i==10 || j==10 || i==13 || i==14 || j==13 || j==14) && (k==1 || k==2 || k==3 || k==5 || k==6 || k==7 || k==9 || k==10 || k==11 || k==13 || k==14 || k==15)) continue;

                //tolérance natal
                if (okProgresse==0 && okTransits==0){
                    //Lilith et NN, tolerance conjonction=8 deg.
                    if (i==10 || i==11 || j==10 || j==11 && (k==0 || k==16)) tolerance=8;
                    else tolerance=orbe[k];
                } 
                
                //aspect trouvé
                if (gap<(angle[k]+tolerance) && gap>(angle[k]-tolerance)){
                    
                    //dynamique transits sur Lune à Saturne du thème
                    if (okDynamique==1 && i<=6){
                        //les transits du jour, pour l'audio
                        air[0].push(i);
                        air[1].push(j);
                        air[2].push(retro[1][j]);
                    }
                  
                    //phase 12 en rouge (lilith phases 1 et 12)
                    if (okTransits==0 && (k==0 || k==16) && i<=6 && (decroissant || j==11)){
                        if (i==0 && (j==2 || j==3)) coul="blue";//conjonction soleil-mercure ou soleil-vénus en bleu 
                        else coul="red";
                    }else coul=couleur[k];
                    //sauvegarde
                    sauveAspect(i,j,k,angle[k],gap,coul);
                break;
                }
            }
        }
    }
    traceAspects();
    tabAspects();
}

//**************************************** liste aspects ****************************************

//affichage liste des aspects à droite du theme
function tabAspects(ref,tt){//optionnel-ref:planete, tt:0,1,2
    var planetes,aspects,orbes,inc,x,y=70,num,i,j,jmin,jmax,k,l,lmin,lmax,tt,a,fonte,color;
    var message="",couleur=["blue","green","orange"];
    //effacement
    x=svg.querySelectorAll(".liste");
    x.forEach(function(item) {svg.removeChild(item)});
        
    //paramètres de boucles
    if (ref>=0){//planete
        lmin=lmax=tt;
        jmin=jmax=ref;
    }else if (okTransits){//tous les transits
        lmin=1;
        lmax=tm();
        jmin=0;
        jmax=11;
    }else{ //theme seul (annulé,trop d'aspects !)
        return;
      /*  lmin=lmax=0;
        jmin=0;
        jmax=11;*/
    }
    //en-tête
    message=titreTableaux();
    fonte=0.9+"vw serif";
    svgTexte("liste","listeAspects0",message,centre[0]+(1.9*rayon),y-50,fonte,"blue");//texte,,position,font,couleur
    if (checkDom.checked) return;
    message="Aspects";
    if (okTransits) message+=" < 1"+"°";
    svgTexte("liste","listeAspects1",message,centre[0]+(1.9*rayon), y-30,fonte,"blue");
    
    //0,1 ou 2 transits
    for (l=lmin;l<=lmax;l++){
        
        //planetes du thème ou ref
        for (j=jmin;j<=jmax;j++){
            planetes=listAspects[l].planete[j].split(",");
            
            //planetes du theme ou en transit
            for (i=0;i<=maxPl;i++){
                if (i==12) continue; //NS
                if ((i==13 || i==14) && checkAsMc.checked==false) continue; //AS-MC
                
                //recherche aspects
                for (k=1;k<planetes.length;k++){
                    a=Math.floor(planetes[k]/100);//0,1,2
                    num=Number(planetes[k])%100;
                    if (num==i){
                        aspects=listAspects[l].aspect[j].split(","); 
                        orbes=listAspects[l].orbe[j].split(",");
                        //symboles aspects
                        x=Number(aspects[k]);
                            //aspects décroissants
                            if (x>8){
                                inc=2*(x-8);
                                x-=inc;
                            }
                        //affichage sujet-aspect-agent
                        fonte=0.8+"vw Zodiac"; 
                            //sujet
                            var car=1.7;
                            message="";
                            if (retro[l][j]) {message=diversFonts[0];car=1.67};
                            message+=planetesFonts[j];
                            svgTexte("liste","listeAspects2",message,centre[0]+(car*rayon),y,fonte,couleur[l]);
                            //aspect + sujet
                            message=aspectsFonts[x];
                            svgTexte("liste","listeAspects3",message,centre[0]+(1.82*rayon),y,fonte,"blue");
                            //agent
                            message=planetesFonts[i];
                            if (retro[0][i]) message+=diversFonts[0];
                            color=couleur[a];
                            svgTexte("liste","listeAspects4",message,centre[0]+(1.94*rayon),y,fonte,color);
                        //affichage orbe en degrés-minutes-secondes
                        fonte=0.9+"vw serif";
                        message=convPos2DegSigne(orbes[k]).secondes;
                        if (message.search("0°00'0")>0) color="red";//en rouge si orbe <1'
                        svgTexte("liste","listeAspects5",message,centre[0]+(2.2*rayon),y,fonte,color);
                        //écart lignes
                        y+=25;
                        break;
                    }
                }
            } 
        }
    }
}

function initAspectsPhases(ref,tt){//tt:0,1 ou 2
    //lecture données
    if(!tt) tt=0;
    var x=listAspects;
    var liste=x[tt].planete[ref].split(",");
    var couleur=x[tt].couleur[ref].split(","); 
    
    return [liste,couleur];
}

function affiPhases(ref){ //ref=planete : mondial=12,AS=13, MC=14
    var startAngle=Math.PI/2,
        endAngle=5*Math.PI/2,
        ecart30=d2r*30,
        k=0,
        car=0,
        aspect,
        posX,
        posY,
        posX0,
        posY0,
        position,
        inc=1,
        affiche,
        message,
        ctx;
    
    //mondial, sortie si transits ou natal non coché
    if (ref==12 && (okTransits || !choix1.item(0).checked)) return;
    
    svg.style.display="none";
    [canvasPhases,ctx]=creeCanvas("canvasPhases");
    //si phases fixes  positionne capricorne en bas  (nécessite incrément date ou heure par changedateheure pour se positionner correctement grâce à asc=180
    if (choix1.item(0).checked && fixePhases && asc!=180 && !okTransits){//natal seulement
        var event = new Event('change');
        incJour.dispatchEvent(event);
    }  
    //cercle externe pour les phases
    ctx.setLineDash([1, 10]); //pointillés
    ctx.textAlign="center";
    ctx.textBaseline="middle";
        
    //tracé cercle avec symboles aspects et numéros de phases
        //très important ! r,g,b sont >0 pour être distingués des pixels noirs des planètes (=0) (v. calcXYPlanete)
        ctx.strokeStyle = 'rgba(50, 50, 50,1)'; 
        ctx.lineWidth = 1.2; //meilleure détection des chevauchements de planètes ?
        
    for (var i=startAngle;i<=endAngle;i+=ecart30){
        ctx.beginPath();
        ctx.arc(centre[0], centre[1], rayon+10, i, i+ecart30,false);
        ctx.lineTo(centre[0], centre[1]);
        ctx.stroke();
 
        //écriture symboles aspects (de conjonction m(109) à opposition u(117))
        var x=[1];
        if (car==1 || car==4 || car==7 || car==10) x=[1/2,1]; //interphases
        for (var j=0;j<x.length;j++){
            //mondial  : trace signes au lieu d'aspects
            if (ref==12 && asc==180){
                if (x[j] !=1) continue;
                ctx.font=1+"vw Zodiac";
                posX=centre[0]+Math.cos(i+d2r*15*x[j])*(rayon+30);
                posY=centre[1]+Math.sin(i+d2r*15*x[j])*(rayon+30);
                ctx.fillText(signesFonts[(20-car)%12], posX, posY);
                continue;
            }
            //aspects
            posX=centre[0]+Math.cos(i+ecart30*x[j])*(rayon+10);
            posY=centre[1]+Math.sin(i+ecart30*x[j])*(rayon+10);         
            aspect=aspectsFonts[(k+inc)%16]; //String.fromCharCode(109+(k+inc)%16);
            ctx.font=1.2+"vw Zodiac";
            ctx.fillText(aspect, posX, posY);
            //interphases
            if (x[j]==1/2){
                ctx.beginPath();
                ctx.moveTo(posX,posY);
                ctx.lineTo(centre[0], centre[1]);
                ctx.stroke();
            }
            //numéros des phases
            if (x[j]==1){
                posX=centre[0]+Math.cos(i+ecart30*x[j])*(rayon+30);
                posY=centre[1]+Math.sin(i+ecart30*x[j])*(rayon+30);
                ctx.font=1+"vw serif";
                ctx.fillText(String(12-car), posX, posY);
            }
            k=k+inc;
            if (k==8) inc=-1;
            if (k==0) inc=1;
        }
        car+=1;
    }
    
    //mondial
    if (ref==12) {
        message=labelsDroite[27]+" "+dateMoyenne(dateMaisons);
        ctx.font= 1+"vw serif";
        ctx.fillText(message, centre[0], centre[1]-10);
    }
    else {
        //affiche planète sujet au centre
        ctx.font=1.4+"vw Zodiac";
        ctx.fillText(planetesFonts[ref], centre[0], centre[1]);
            if (retro[0][ref]) ctx.fillText(diversFonts[0],centre[0]+taille/2,centre[1]);//si retrograde
        //ajoute 2 messages
        if (!isNaN(posPlanete[0][ref])){
            var abc=convPos2DegSigne(posPlanete[0][ref]);
            //thème
            if (okTransits==0) message=labelsCentre[2]; //theme;
            //transit
            else if (okTransits==1) message="transits "+labelsDroite[38]; // "sur"
            //texte au centre
            ctx.font= 1+"vw serif";
            ctx.fillText(message, centre[0], centre[1]-30);
            message=abc.degres+"   ";//signe planète du centre
            let l=ctx.measureText(message).width;//longueur du message
            ctx.fillText(message, centre[0], centre[1]+30);
            ctx.font=1+"vw Zodiac";
            ctx.fillText(signesFonts[abc.signe], centre[0]+l/2, centre[1]+30);
        }
    }
    
    //initialisation
    var x=0,y;
    var liste,couleur;
    if (ref==12) x=1;//mondial
    ctx.font=1.2+ "vw Zodiac";
    ctx.setLineDash([]);
    
    //recherche aspects
    var [debut,fin]=[[ref,4],[ref,9]];
    position= checkDom.checked ? posDom[tm()] : posPlanete[tm()];
    for (k=debut[x];k<=fin[x];k++){
        [liste,couleur]=initAspectsPhases(k,0);
        //affiche planete ref en mondial
        AStemp= checkDom.checked ? d2r*(posDom[0][k]+90) : d2r*(posPlanete[0][k]+90);
        [posX0,posY0]= ref==12 ? calcXYPlanete(k,position,AS,canvasPhases,0.94) : [centre[0],centre[1]];
        if (ref==12) ctx.fillText(planetesFonts[k], posX0, posY0);  
            //retrograde ?
            if (retro[tm()][k]) ctx.fillText(diversFonts[0],posX0+taille/2,posY0);
        //affiche aspects
        var [pl0,pl1]=[[0,k+1],[maxPl,9]];
        for (var i=pl0[x];i<=pl1[x];i++){
            if (isNaN(posPlanete[tm()][i])) continue;
            affiche=0;
            //saute AS-MC si non coché
            if ((i==13 || i==14) && checkAsMc.checked==false) continue;
            //supprime agent si sujet=agent
            if (i==refPhase && okTransits==0) continue;
            //fixe, on affiche toutes les planetes
            if (fixePhases==1) affiche=1;
            //recherche aspect
            for (var j=1;j<liste.length;j++){
                //1er transit=100-123,2eme:200-233, réduit à 0-23
                y=Number(liste[j])%100;
                if ((ref!=12 && i==y) || (ref==12 && (y>=4 && y<=9) && i==y)) {
                    affiche=2;
                    break;
                }
            }
            if (affiche==0) continue;
            //coordonnées planète
            [posX,posY]= ref==12 ? calcXYPlanete(i,position,AS,canvasPhases,0.94) : calcXYPlanete(i,position,AStemp,canvasPhases);
            //trace aspect si existe
            if (affiche==2) {
                ctx.beginPath();
                //1er point aspect
                ctx.moveTo(posX0,posY0);
                //2ème point aspect
                ctx.lineTo(posX,posY);
                ctx.strokeStyle=couleur[j];
                ctx.stroke();
            }
            //affiche planete (toujours si "fixePhases" sinon seulement si aspect (mode survol))
            if (ref!=12) ctx.fillText(planetesFonts[i], posX, posY);  
                //retrograde ?
                if (retro[tm()][i]) ctx.fillText(diversFonts[0],posX+taille/2,posY);
        } 
    }
}


//********************************************** gestion date *****************************************************    
function setDate(date,i){//i:incrément/décrément des heures selon décalage utc
    if (!i) i=0;
    var an,mois,jour,heure,minute,dec,offset=0;
    
    //ajuste heure suivant i
    heure=date.getHours()+i;
    minute=date.getMinutes();
    //dec=partie décimale si utc non entier
    dec=heure-Math.floor(heure);
    minute+=dec*60;
    minute=Math.round(minute);
    heure=Math.floor(heure);
    if (minute>=60) {
        minute-=60;
        if (heure>=0) heure+=1;
        else heure-=1;
    }
    if (heure<0) offset=-1;
    else if (heure>=24) offset=1;
    heure=heure-24*offset;
    //corection date
    date.setDate(date.getDate()+offset);
    an=date.getFullYear();
    mois=date.getMonth()+1;
    jour=date.getDate();
    an=String(an);
    mois=ajoutZero(String(mois));
    jour=ajoutZero(String(jour));
    heure=ajoutZero(String(heure));
    minute=ajoutZero(String(minute));
    
    return[an,mois,jour,heure,minute];
}

function dateJour(x){//x=1 : horloge temps réel pour "maintenant'
    //date du jour
    var datetime = new Date();
    
    //gestion utc : permet d'afficher l'heure correspondant à utc et non l'heure réelle si utc est incorrect
    var utcReel= datetime.getHours()-datetime.getUTCHours();
    var utcDiff=0;
    if (utcDef=="") utc=utcReel;
    else utc=Number(utcDef);
    utcDiff=utc-utcReel;
    utcLocal=utc;
    
    //date
    var [an,mois,jour,heure,minute]=setDate(datetime,utcDiff);
    dateMaintenant=an+"-"+mois+"-"+jour;
    anneeTransit.value=an;
    heureSauve=heureMaintenant=choixHeure.value=heure+":"+minute;

    //contrôle format
    var reponse=controleDate(dateMaintenant);
    //pour affichage cyclique (maintenant)
    if (x) return datetime.getSeconds();
}

function controleDate(date){
 var reponse="non";
 if (date.length ==10){
     reponse="oui";
    //type date : aaaa-mm-jj, type text : jj/mm/aaaa
   if (choixDate.type==="date") { //dom.forms.datetime=true (aaaa-mm-jj)
       if (date.split('-').length>1){
            dateMaisons=date;
        }
        else if (date.split('/').length>1){
            dateMaisons=date.slice(6,10) +"-"+date.slice(3,5)+"-"+date.slice(0,2);
        }
   } else if (choixDate.type==="text"){ //dom.forms.datetime=false (jj/mm/aaaa) 
       if (date.split('-').length>1){
            dateMaisons=date;
        }
        else if (date.split('/').length>1){
            dateMaisons=date.slice(6,10) +"-"+date.slice(3,5)+"-"+date.slice(0,2);
        }
     } 
 }
    
    dateLong=dateLongue(dateMaisons);
    var annee=dateMaisons.slice(0,4);
    if (affiDate==1) choixDate.value=dateMaisons;
    return reponse;
}  

//3 formats de date
function dateFormat(date,options){
    var date = new Date(date);
    var langue=browser.i18n.getUILanguage();
    var dateTime=new Intl.DateTimeFormat(langue, options).format(date); 
    return dateTime;    
}

//date au format long (jour de la semaine et mois en lettres)
function dateLongue(date){
    var options = { year: 'numeric', month: 'long', day: 'numeric', weekday: 'long'};
    return dateFormat(date,options);
}

//date au format moyen (mois en lettres abrégées)
function dateMoyenne(date){
    var options = { year: 'numeric', month: 'short', day: 'numeric'};
    return dateFormat(date,options);
}

//date au format court (jj/mm/aaaa)
function dateCourte(date){
    var options = { year: 'numeric', month: 'numeric', day: 'numeric'};
    return dateFormat(date,options);
}

//**************************************fin date ***************************************

function requetes(date,heure,nom,sauteDessins){
    //date
    if (controleDate(date)!="oui") return;
    if (affiHeure==1) {
    choixHeure.value=heure;
    incDateHeure(); 
    }
    //planètes
    if (calcPosPlanetes(date,heure) !="oui") return;
    //titre
    if (okTransits==0 && (!nom || nom=="?")){
        titreTheme=labelsDroite[0]+ " "+ dateLongue(date) + " -  " + choixHeure.value;
        if(affiSave)  affiSave.style.fontWeight="normal";//enlève gras du nom sélectionné
        if (!nom){
            coordonnees=" "+placeDef + " (utc "+String(utcLocal) +", lat. "+latDef.replace(".","°")+"'" +", long. "+ longDef.replace(".","°")+"'" +")";
        }
        titreTheme+=" "+coordonnees;
    }else if(nom==nomNatal){
        titreTheme=nomNatal +labelsDroite[8]+dateLongue(date) +" - " + choixHeure.value + labelsDroite[9] + coordonnees;
    }else titreTheme=nom;
    //maisons
    calcPosMaisons(date,heure);
    if (okTransits) {
        if (okArc || okProgresse || okRS) recupPosMaisons("natalbis");
        else recupPosMaisons("natal");
    }
    //affichage coordonnées dans panneau de gauche
    var valeur=[date,heure,Lieu,utc,cDdDm(latitude,1),cDdDm(longitude,1)];
    if (tm()==0 && checkMaintenant.checked==false) affiNatif(nomNatal,valeur);//sauf si transits ou "maintenant" coché
    //affichage
    if (!sauteDessins) dessins();
}    

function affiNatif(clef,valeur){
    var a,b,i,j,x,y;
    natif.nom.value=clef;
    for (i=0;i<valeur.length;i++){
        a=Object.values(natif);
        //séparation decimales pour lat/long
        if (i==4) {
            b=[4,4,5,5,6,7];
            for (j=0;j<=3;j+=3){
                x=valeur[b[j]];
                //partie entière
                y=Math.trunc(x);
                if (x<0 && x>-1) y="-"+y;
                a[b[j+1]].value=y;
                //partie décimale
                a[b[j+2]].value=Math.round(Math.abs((x-y).toFixed(2))*100);
            }
            break;
        }else a[i].value=valeur[i];
    }     
}

//recalcule le natal si changement heure natale (Arc,RS,Progressé)
function reCalcNatal(d,h,heure,x){//d=0 : date affichée inchangée, h=0 : heure affichée inchangée, x=pas de dessins
        var okSave=okTransits;
        okTransits=0;
        affiDate=d;
        affiHeure=h;
        var l0=latitude;
        var l1=longitude;
        var l2=utc;
        if (latNatal) [latitude,longitude,utc]=[latNatal,longNatal,utcNatal];
        var d0=dateMaisons;//sinon requetes change dateMaisons à date natale au lieu de date du jour ! (affecte progressé)
    requetes(dateNatalBis,heure,titreTheme,x);// x>0: pas de dessins
    sauvePosMaisons("natalbis");
        okTransits=okSave;//1
        affiDate=1;
        affiHeure=1;
        [latitude,longitude,utc]=[l0,l1,l2];
        dateMaisons=d0;
    
    if (okTransits && okArc) calcArcSolaire();
    //pour RS et arcsolaire  
    soleilNatalBis=posPlanete[0][0];
}

//récupère les données du natal sans recalcul
function resetNatal(){
    choix1.item(0).checked=true; //natal
    if (!posPlaneteNatal[0] || !dateNatal || checkMaintenant.checked==true) {
        optTransits.disabled=true;
        return 0;
    }
    for (var i=0;i<=maxPl;i++){
        posPlanete[0][i]=posPlaneteNatal[i];
        retro[0][i]=retroNatal[i];
        if (i<=12) posMaison[i]=posMaisonNatal[i];
    }
    asc=posMaisonNatal[0];
    titreTheme=titreNatal;
    resetPosMaisonsNatalbis();
    soleilNatalBis=soleilNatal=posPlanete[0][0];
    dateNatalBis=dateNatal;
}

//****************************************** listeners ********************************************************

 checkMaintenant.addEventListener("change",() => {
    if (checkMaintenant.checked==false) {
        //supprime mise à jour horloge  
        clearTimeout(timerId1);
        timerId1 = null;
        return;
    }
    //charge les valeurs par défaut (ville,utc,etc.)
    var getting = browser.storage.local.get("zodiaque");
    getting.then((result) => {
        var objTest = Object.keys(result); 
        if (objTest.length){ //=1 (clef "zodiaque")
            //adaptation suite introduction paramètre horizon v2.6.5
            var inc,b=result.zodiaque;
            if (b.length==15) inc=5;//version 2.6.3 ou inférieures
            else inc=6;//versions > 2.6.3 length=16
            //lieu, utc, longitude,latitude, luminosité, horizon
            if (b[0]) placeDef=b[0];
            if (b[1]) utcDef=b[1];
            if (b[2]) latDef=b[2];
            if (b[3]) longDef=b[3];
            //luminosité + horizon
            if (b[4]) luxDef=b[4];
            if (inc==6 && b[5]) horizonDef=b[5];

            if (demarrage==1){
                if (luxDef) {
                    lux=Number(luxDef);
                    luminosite.value=luxDef; 
                } 
                if (horizonDef) {
                    hrz=Number(horizonDef);
                    horizon.value=horizonDef; 
                }
                demarrage=0;
            }
            //orbes
            orbesDef=[];
            for (var i=0;i<=9;i++){
                if (b[i+inc]) orbesDef[i]=Number(b[i+inc]);
            }
        }
        maintenantSuite();
     },onError);
},onError);
 
function maintenantSuite(){
    iconeGomme.click();
    if (demarrage==0) canvasCache(false);   
    //conversion deg.min en degres.decimales
    var x=Number(latDef); //48.51;
    var y=Number(longDef); //2.21;
    [latitude,longitude]= convDegresMinutestoDegresDecimal(x,y);
    [latNatal,longNatal]=[latLocal,longLocal]=[latitude,longitude];
    //divers
    Lieu=placeDef;
    nomTheme2="";
    nomNatal=labelsDroite[0];//zodiaque
    okTransits=okSynastrie=okProgresse=okTransits=okArc=okRS=okMix=0;
    fixePhases=0;
    arcSolaire=0;
    NatalExterne=0;//reset choix données natal/externe
    margeNoir();
    //bouton radio "dynamique"
    efface(divDynamique);
    divDynamique.hidden=true;
    choixTypeDonnees.item(0).click();
    //checkbox
    choix1.item(0).checked=true;
    choixTableauDonnees.item(0).checked=true;
    selectTransits.value="optTheme"; //"";pb titre revient sur titre natal si changement de date/heure
    //div hidden ou dévalidés
    cIdN(true);
    divLabelsProg.hidden=true;
    tableau.hidden=true;
    devalideSynastrie(false);//laisser avant devalideNow
    devalideNow(true);
    choixTypeDonnees.item(3).disabled=true;//dynamique
    optTransits.disabled=true;
    divChoixDonnees.hidden=true;
    if (okUranien==0) margeDiv.hidden=false;
    if (!checkMaisons.checked) horizon.disabled=false;
    //enlève affichage en gras du précédent nom sélectionné
    if (affiSave){
        affiSave.style.fontWeight="normal";
        clefSave="";
    }
    
    // affichage toutes les 60", avec correction en cas de décalage/horloge pc (avec firefox, chrome ok)
    (function maintenantCycle(){//IIFE : Immediately Invoked Function Expression
        //date + utc
        var decalage=dateJour(1);//nbre de secondes à l'horloge pc
        utcNatal=utc;
        requetes(dateMaintenant,choixHeure.value,"");
        canvasCache(false);//laisser après requetes
        if (dateMaintenant.search("/")>0) cacheHorloge(true); //ancien firefox
        timerId1=setTimeout(maintenantCycle, 60000-1000*decalage);//msec - pour info setInterval ne permet pas de modifier le délai  
    })();
}

//**************************** date et heure *************************************

/* exemple d'incrémentation : 
var tomorrow = new Date();
tomorrow.setDate(tomorrow.getDate() + 1);*/
function incDateHeure(){
    var abc=choixDate.value.split("-");
    //empêche jour vide ("") quand on arrive à 1 90 ou 360 avec le calendrier principal, dans certains cas
    if (abc[2]) incJour.value=abc[2];
    if (abc[1]) incMois.value=abc[1];
    if (abc[0]) incAn.value=abc[0];
    
    abc=choixHeure.value.split(":");
    if (abc[0]) incHeure.value=abc[0]; //heures
    if (abc[1]) incMinute.value=abc[1]; //minutes
}

async function changeDateHeure(){
    checkMaintenant.checked=false;
    //supprime mise à jour horloge  
    clearTimeout(timerId1);
    timerId1 = null;

    if (tableau.hidden==false && okUranien==0) {
        selectTransits.selectedIndex=0;//theme seul
        mix();
        return;
    }
    
    if (okUranien==0){
       
        divTableaux.hidden=true;
    }
    
    //2ème transit
    if (okMix) {mix();return}
    
    //retour à "theme" si date ou heure changées en synastrie
    if(okSynastrie){
        selectTransits.value="optTheme";
        mix();
        return;
    }
    if (okRS){
        dateMaisons=choixDate.value;
        await calcRS();
        if (fixePhases && okMix==0) affiPhases(refPhase);
        return;
    }
    
    tableau.hidden=true;
    if (fixePhases==0) canvasCache(false);
    
    if (okProgresse==0){
        //theme seul
        if (selectTransits.value=="optTheme"){
            requetes(choixDate.value,choixHeure.value,"?",1); // nom="?" : efface nom natal, remplacé par "zodiaque" et permet de conserver coordonnées (longitude, etc) 
        }
        
        /*await évite sur chrome en mode dynamique : 
            [Violation] Forced reflow while executing JavaScript took 31ms
            [Violation] 'setTimeout' handler took 82ms (zodiaque.js:1958)*/
        else await requetes(choixDate.value,choixHeure.value,titreNatal,1);//1:pas de dessins
        
        if (fixePhases){
            if (okMix) fixePhases=0;
            if (!okTransits) asc=180;//en mondial met le capricorne en bas
            await dessins();//sinon blocage dans calcxyplanete si chgt date+fixedphase sur firefox 2.6 (pas chrome)
            if (okMix==0) affiPhases(refPhase);
            return;
        }
        if(okArc) calcArcSolaire();//dessins indispensable après!
        
    } else if(okProgresse){
       var reponse=controleDate(choixDate.value);
       rechercheThemeProgresse(dateNatalBis);
        if (fixePhases) {
            globalAspects(); //pour recalcul des aspects
            affiPhases(refPhase);
            return;
        }
        //pour récup maisons natales
        if (okTransits) {
           // reCalcNatal(0,0,choixHeure.value,1); //ne pas supprimer (transits progressés) !
            recupPosMaisons("natalbis");
            posPlanete[tm()][13]=posMaisonProgresse[0];//AS
            posPlanete[tm()][14]=posMaisonProgresse[9];//MC
        }
    }
    dessins(); 
}

//********* date

choixDate.addEventListener("change",changeDate,false);

function changeDate(x){//si x="minuit", évite double affichage au chgt heure+date (23h59)
    incDateHeure();
    if (x!="minuit") changeDateHeure();
}

function checkDate(jour,mois,an,x){//x="minuit"
    var date=choixDate.value.split("-");
    if(jour==-1) jour=Number(date[2]);
    if (mois==-1) mois=Number(date[1]);
    if (an==-1) an=Number(date[0]);
    var joursMois=[0,31,28,31,30,31,30,31,31,30,31,30,31];
    //année bissextile sauf 1900
    if (an%4==0 && an!=1900)joursMois[2]=29;
    checkMois();
    
    function checkMois(){
        if (mois<1){
            mois=12;
            an-=1;
        }
        else if (mois>12){
            mois=1;
            an+=1;
        }
    }
    
    if (jour<1){
        mois-=1;
        checkMois();
        jour=joursMois[mois];
    }
    else if (jour>joursMois[mois]){
        mois+=1;
        checkMois();
        jour=1;
    }
    choixDate.value=String(an) + "-" + ajoutZero(String(mois)) + "-" + ajoutZero(String(jour));
    changeDate(x);
}

incJour.onchange=function(e){//e.detail="minuit" si appelé par checkJour2Date
    var jour=Number(incJour.value);
    checkDate(jour,-1,-1,e.detail);
}

incMois.onchange=function(){
    var mois=Number(incMois.value);
    checkDate(-1,mois,-1);
}

incAn.onchange=function(){
    var an=Number(incAn.value);
    checkDate(-1,-1,an);
}


 //**************** heure
 
choixHeure.addEventListener("change",changeHeure,false);
  
function changeHeure(){
    incDateHeure();
    
    //tableau progressé ? si oui, on le conserve affiché
    if (tableau.hidden==false){
        for (var i=1;i<=2;i++){
            if (choix2.item(i).checked==true){
                okProgresse=1;//évite changement de date après minuit
                choix2.item(i).checked=false;
                choix2.item(i).click();
                return;
            }
        }
    }
   // changeDateHeure();//laisser avant sinon pas de changement des maisons natales
    
    //recalcule theme natal (+maisons) si heure de naissance changée
    if (okMix==0 && (okArc || okProgresse || okRS)) reCalcNatal(0,1,choixHeure.value,1);

    changeDateHeure();
}

function checkJour2Date(hh){
    var x;
    if (hh>23 || hh<0){
        if (hh>23){//incrémente jour
            hh=0;
            x=1;
        } else if (hh<0){//décrémente jour
            hh=23;
            x=-1;
        }
        if (okProgresse==0 && okArc==0 && okRS==0){
            incJour.value=Number(incJour.value)+x; //sinon string
            // Create a new 'change' event
            var event = new CustomEvent('change', { detail: "minuit" });
            // Dispatch it.
            incJour.dispatchEvent(event);
        }
        //avec progressé,arc solaire et RS changement de date natale si passage à minuit
        else if (okProgresse || okArc || okRS){
            var refTime=new Date(dateNatalBis);
            var jour=refTime.getDate();
            refTime.setDate(jour+x);
            jour=refTime.getDate();
            var mois=refTime.getMonth()+1;
            var an=refTime.getFullYear();
            dateNatalBis=ajoutZero(String(an)+"-"+ajoutZero(String(mois))+"-"+String(jour))
            labelDateNaissance.textContent="natal "+dateMoyenne(dateNatalBis);
        }
    }
    return hh;
}

incHeure.onchange=function(){
    //date ancienne
    var heure=choixHeure.value.split(":");
    var minute=Number(heure[1]);;
    //nouvelle heure
    var hh=Number(incHeure.value);
    hh=checkJour2Date(hh);
    choixHeure.value=ajoutZero(String(hh)) + ":" + ajoutZero(String(minute));
    changeHeure();
}

incMinute.onchange=function(){
    //date ancienne
    var heure=choixHeure.value.split(":");
    var hh=Number(heure[0]);
    //nouvelle minute
    var minute=incMinute.value;
    
    //fin d'heure
    if (minute>59){
        minute=0;
        hh+=1;
        hh=checkJour2Date(hh);
    }
     //début d'heure
    else if (minute<0){
        minute=59;
        hh-=1;
        hh=checkJour2Date(hh);
    } 
    choixHeure.value=ajoutZero(String(hh)) + ":" + ajoutZero(String(minute));
    changeHeure();
}


//********************************** entrées utilisateur ****************************************
function choixEcoute(){
    //choix1,2,3,4,roue : pour boutons radio avec plusieurs choix
    var x=[choix1,choix2,choix3,choixTypeDonnees,choixZodiaque,choixRoue,choixTableauDonnees];
    for (var i=0;i<x.length;i++){
        for (var j=0;j<x[i].length;j++){
            x[i][j].addEventListener("change",choixAgit,false);
        }
    }
     //autres (ne pas utiliser forEach sinon les fonctions du dessus (choix1,etc. sont effectuées 2 fois !)
    x=[checkMaisons,checkDom,checkMaisonsNatal,checkEquationTemps,anneeTransit,checkAsMc,cacheGauche,cacheCentre,cacheTitre,coefforbe,luminosite,horizon,selectMP,selectTransits];
    for (i=0;i<x.length;i++){
        x[i].addEventListener("change",choixAgit,false);
    }
}

function choixAgit(e){
    switch(e.target.id){ //le "id" vient de zodiaque.html : doit être identique à la variable
    //*** boutons radio
        //choix1 : thème natal, progressé, rs
        case "radioThemeNatal":
            if (checkMaintenant.checked==true) return;
            okTransits=okProgresse=okRS=okSynastrie=okArc=0;
            if (latNatal) [latitude,longitude,utc]=[latNatal,longNatal,utcNatal];
            choix1Init(true);
            if (okUranien==1) labelRoue.hidden=false;
            //choix données natales par défaut
            NatalExterne=0;
            choixTableauDonnees.item(0).checked=true;
            requetes(dateNatal,heureNatal,nomNatal);
            break;
        case "radioThemeRS":
            if (checkMaintenant.checked==true) {choix1.item(0).checked=true;return};
            if (tableau.hidden==false) okTransits=okProgresse=0;
            if ((okTransits==0 && okProgresse==0) || okSynastrie) dateJour();
            okRS=1;
            okTransits=okProgresse=okArc=okSynastrie=0;
            selectTransits.value="";
            choix1Init(false);
            choixHeure.value=heureNatal;
            incDateHeure();
            //choix données natales par défaut
            NatalExterne=0;
            choixTableauDonnees.item(0).checked=true;
            calcRS();
            break;
        case "radioThemeProgresse":
            if (checkMaintenant.checked==true) {choix1.item(0).checked=true;return};
            if (tableau.hidden==false) okTransits=okProgresse=0;
            if ((okTransits==0 && okRS==0) || okSynastrie) dateJour();
            okProgresse=1;
            okTransits=okRS=okArc=okSynastrie=0;
            selectTransits.value="";
            choix1Init(false);
            if (latNatal) [latitude,longitude,utc]=[latNatal,longNatal,utcNatal];
            choixHeure.value=heureNatal;
            incDateHeure();
            //choix données natales par défaut
            NatalExterne=0;
            choixTableauDonnees.item(0).checked=true;
            if (okUranien){
                divCoeff.hidden=true;
                divMaisonsNatal.hidden=true;
            }
            else{
                divCoeff.hidden=false;
                divMaisonsNatal.hidden=false;
            }
            checkMaisonsNatal.checked=false; //maisons progressées par défaut
            rechercheThemeProgresse(dateNatalBis);
            dessins();
            break;
        //choix2 : tableaux de transits
        case "transitsMondiaux":
            Promise.resolve().then(() => {//évite sur chrome message : [violation] change handler took xx msec
                checkDom.checked=false;
                cIdN(true);
                divLabelsProg.hidden=true;
                //checkMaintenant.checked=false;
                okProgresse=okTransits=okRS=0;
                canvasCache(true);
                feuilleTransits(5,11);
                rechercheTransitsMondiaux(anneeTransit.value);
            });
            break;
        case "transitsProgresseNatal":
            checkDom.checked=false;
            cIdN(false);
            okProgresse=1; //bloque les transits même si pas de perte du theme natal !
            choix2ProNat(0);
            break;
        case "transitsProgresseProgresse":
            checkDom.checked=false;
            cIdN(false);
            okProgresse=1; //bloque les transits sinon perte du theme natal
            choix2ProNat(1);
            break;
        //choix3 : dominantes
        case "normal":
            couleurSecteurs(12);
            svgEfface(".rect_elements");
            dessins();//permet que les éléments soient redétectés par la souris après un changement
            break;
        case "binaire":
            couleurSecteurs(2);
            LegendeElements(0,1);
             dessins();
            break;
        case "ternaire":
            couleurSecteurs(3);
            LegendeElements(2,4);
             dessins();
            break;
        case "quaternaire":
            couleurSecteurs(4);
            LegendeElements(5,8);
             dessins();
            break;
        //choixTypeDonnees
        case "donneesPositions":
            choixTypeDonneesAffiche(tabResume);
            break;
        case "donneesDominantes":
            choixTypeDonneesAffiche(tabDominantes);
            break;
        case "donneesChaine":
            choixTypeDonneesAffiche(tabChaine);
            break;
        case "dynamique":
            if (checkMaintenant.checked==true){
                choixTypeDonnees.item(3).checked=false;
                break;
            }
            choixTypeDonneesAffiche(divDynamique);
            modeDynamique();
            break;
        //choixZodiaque
        case "tropical":
            okUranien=0;
            maxPl=14;
            ascDef=180;
            choixZodiaqueInit(true);
            margeSet();
            choixTypeDonnees.item(0).click();
            svg.removeEventListener('mousemove',croixListener,false);
            break;
        case "uranien":
            checkDom.checked=false;
            dateNatalBis=dateNatal;
            soleilNatalBis=soleilNatal;
            okUranien=1;
            maxPl=23;
            ascDef=90;
            if (roueAngle==360) ascDef=180;
            //réinit choix mp et formules
                for (var i=0;i<=maxPl;i++){
                    listeBlanche[i]=0;
                }
                //soleil, lunne sélectionnés par défaut
                listeBlanche[0]=listeBlanche[1]=1; //=listeBlanche[13]=listeBlanche[14]=1;
                MP=0;
                selectMP.value="optMipoint";
                margeDiv.hidden=false;
                margeSet();
            svg.addEventListener('mousemove',croixListener,false);
            svg.addEventListener('mousedown', e => {
                rouelibre=1;
            });
            svg.addEventListener('mouseup', e => {
                rouelibre=0;
            });
            choixZodiaqueInit(false);//laisser à la fin !
            break;
        //choixRoue en uranien
        case "225":
            roueAngle=22.5,
            roueIndice=16;
            asc=ascDef=90;
            checkMaisons.checked=false;
            dessins();
            break;
        case "90":
            roueAngle=90,
            roueIndice=4;
            asc=ascDef=90;
            checkMaisons.checked=false;
            dessins();
            break;
        case "360":
            roueAngle=360,
            roueIndice=1;
            asc=ascDef=180;
            checkMaisons.checked=true;
            dessins();
            break;
        //choixTableauDonnees
        case "donneesTheme":
            NatalExterne=0;
            if (okUranien) tableauUranien(0);
            else tableauDominantes(0);
            break;
        case "donneesTransit":
            NatalExterne=1;
            if (okUranien) tableauUranien(1);
            else tableauDominantes(1);
            break;
        //choix thème seul ou Transits
        case "selectTransits":
            // if (checkDom.checked) {
            //     let a=selectTransits.selectedIndex;
            //     selectTransits.item(a).selected=false;
            //     return;
            // }
            mix(); //theme.js
            break;
        //choixOpt mi-points(a+b)/2, paires(a/b=c/d), formule(a+b-c)
        case "selectMP":
            var a=selectMP.value;
            if (a=="optMipoint") {MP=0;margeDiv.hidden=true};
            if (a=="optPaire") {MP=1;margeDiv.hidden=false;lbCheck()};
            if (a=="optFormule") {MP=2;margeDiv.hidden=false;lbCheck()};
            uranienAspects(planeteDefaut,typeRef);
            break;
    //*** checkbox - autres
        case "checkMaisons":
            horizon.disabled=true;
            if (okUranien==0){
               // if (checkMaisons.checked==false);// asc=ascDef;//départ en balance 180deg. supprimé..
                //horizon
                if (!checkMaisons.checked && titreTheme.search("Zodia")==0  && !okTransits && choix1.item(0).checked){//natal
                    horizon.disabled=false;
                    calcPosMaisons(choixDate.value,choixHeure.value);//pour recalcul asc
                }
                //thème progressé
                else if (choix1.item(2).checked==true){
                    asc=posMaisonProgresse[0];
                    checkMaisonsNatal.checked=false;
                    titreTheme=titreTheme.replace(labelsDroite[14],labelsDroite[15]);
                }
                else if (checkMaintenant.checked==true || !posMaisonNatal.length) asc=posMaison[0];
                else if (!choix1.item(1).checked) asc=posMaisonNatal[0];//RS
                dessins();
            }
            else if (okUranien==1){//en uranien  maisons uniquement à roue 360
                if (checkMaisons.checked==true){
                    choixRoue.item(0).checked=choixRoue.item(1).checked=choixRoue.item(2).checked=false;
                    choixRoue.item(2).click();//roue 360
                }else dessins();//enlève les maisons
            }
            break;
        //checkDomification
        case "checkDom":
            if (okUranien) {checkDom.checked=false;return};
            if (!checkDom.checked) resetDoms();
            reaffiche();
            break;
        case "checkMaisonsNatal":
            if (okUranien) {checkMaisonsNatal.checked=false;return};
            if (okTransits) {checkMaisonsNatal.checked=true;return};
            if (checkMaisonsNatal.checked==true){
                recupPosMaisons("natal");
                titreTheme=titreTheme.replace(labelsDroite[15],labelsDroite[14]);
            }else{ 
                recupPosMaisons("progresse");
                titreTheme=titreTheme.replace(labelsDroite[14],labelsDroite[15]);
            }
            dessins();
            break;
        case "checkEquationTemps":
            //theme ou transits progressé
            if (tableau.hidden==true){
                if (choix1.item(2).checked==true){//theme progressé
                    choix1.item(2).checked=false;
                    choix1.item(2).click();
                }else if (okProgresse && okTransits) progresse();//transits progressés
            //tableaux transits progressés
            }else if (choix2.item(2).checked==true){//progressé/progressé
                //il faut dévalider le bouton avant de cliquer dessus
                choix2.item(2).checked=false;
                choix2.item(2).click();
            }else if (choix2.item(1).checked==true){//progressé/natal
                //il faut dévalider le bouton avant de cliquer dessus
                choix2.item(1).checked=false;
                choix2.item(1).click();
            }
            break;
        case "anneeTransit":
            for (var i=0;i<=2;i++){
                if (choix2.item(i).checked==true){
                    //il faut dévalider le bouton avant de cliquer dessus
                    choix2.item(i).checked=false;
                    choix2.item(i).click();
                    break;
                }
            }
            break;false
        case "checkAsMc":
            margeNoir();
            fixePhases=0;
            dessins();
            break;
        case "cacheGauche":
            cadre1.hidden= cacheGauche.checked ? false : true;
            if (tableau.hidden==true)dessins();
            break;
        case "cacheCentre":
            cadre2.hidden= cacheCentre.checked ? false : true;
            if (tableau.hidden==true)dessins();
            break;
        case "cacheTitre":
            titreCanvas.hidden= cacheTitre.checked ? false : true;
            //dessins();
            break;
        case "coefforbe":
            if (tableau.hidden==true)dessins();
            break;
        case "luminosite":
            if (okUranien)return;
            demarrage=0;
            lux=Number(luminosite.value);
            secteursSignes();
            dessins();//sinon, survol souris sur planètes et signes inopérant
            break;
        case "horizon":
            hrz=Number(horizon.value);
            dessins();
            break;
    }
}

//cacheInfosDateNaissance
function cIdN(x){
    if (!dateNatal) return;
    if (tableau.hidden==true) dateNatalBis=dateNatal;
    soleilNatalBis=soleilNatal;
    labelDateNaissance.textContent="natal "+dateMoyenne(dateNatalBis);
    labelDateNaissance.hidden=x;
    infoDateNaissance.hidden=x;    
}

function choix1Init(x){
    canvasCache(false);
    checkMaintenant.checked=false;
    checkEquationTemps.hidden=x;
    checkMaisonsNatal.hidden=x;
    labelMaisonsNatales.hidden=x;
    coeffOrbe.hidden=x;
    tableau.hidden=true;
    if (okUranien==0) margeDiv.hidden=false;
    if (okUranien) {
        selectTransits.value="";
        okMix=0;
    }
    choix1.item(0).checked==true ? cIdN(true) : cIdN(false);
    //tableau transits
    choix2.item(0).checked=false;
    choix2.item(1).checked=false;
    choix2.item(2).checked=false;
    checkDom.checked=false;
    if (okUranien==0 && choix1.item(0).checked) divTableaux.hidden=false;
    else divTableaux.hidden=true;
    divChoixDonnees.hidden=true;
    if (okProgresse) divLabelsProg.hidden=false;
    else  divLabelsProg.hidden=true;
    fixePhases=typeRef=okMix=0;
}
    
function choix2ProNat(x){
    checkMaintenant.checked=false;
    divLabelsProg.hidden=false;
    checkEquationTemps.hidden=false;
    checkMaisonsNatal.hidden=true;
    checkMaisonsNatal.checked=false;
    coeffOrbe.hidden=true;
    divCoeff.hidden=true;
    divMaisonsNatal.hidden=true;
    okTransits=1;//important, permet de mettre les positions planetes progressées en posPlanete[1] sinon pb de tableaux transits
    canvasCache(true);
    feuilleTransits(0,6);
    rechercheTransitsProgresses(dateNatalBis,choixHeure.value,x);    
}

function choixTypeDonneesAffiche(choix){
    var a=[tabResume,tabDominantes,tabChaine,tabUranien,divDynamique];
    for (i of a){i.hidden=true};
    if (choix) choix.hidden=false;
}

function LegendeElements(x0,x1){
    //suppression anciens carrés
    svgEfface(".rect_elements");
    var y=160,c;
    
    for (var i=0;i<=x1-x0;i++){
        //carrés de couleur90 ou 360
        c="hsla(" + 60*(i%(x1-x0+1)) + ",70%, 50%,0.30)"; //0.30:luminosité des carrés de la légende
        svgRectangle("rect_elements",centre[0]+(1.2*rayon),y,20,20,c);
        //texte légende
        svgTexte("rect_elements","rect"+i,labelsDominantes[x1-i],centre[0]+(1.24*rayon), y+24,0.8+"vw serif","bold");
        y-=40;
    }
}

function choixZodiaqueInit(choix){
    document.getElementById("divQuaternaire").hidden=!choix;
    document.getElementById("divChoixDonnees").hidden=choix;
    document.getElementById("bascule").hidden=!choix;
    divChoixResumes.hidden=!choix;
    //cache tableau et dévalide les options
    for (var i=0;i<=2;i++){
        choix2.item(i).checked=false;
    }
    var a=[tabUranien,labelRoue,divMP,divtabMP];
        for (i of a){i.hidden=choix};
    a=[tabResume,tabDominantes,tabChaine,margeDiv,divTableaux];
        for (i of a){i.hidden=!choix};
    a=[tableau,divDynamique,labelDateNaissance,divChoixDonnees,divLabelsProg];
        for (i of a){i.hidden=true};
    checkAsMc.checked=!choix;
    checkMaisons.checked=choix;
    choix1.item(0).checked=true;//natal sélectionné
    choixTableauDonnees.item(0).checked=true;
    horizon.disabled=true;
    selectTransits.value="optTheme";
    asc=posMaisonNatal[0];
    fixePhases=typeRef=NatalExterne=0;
    okProgresse=okTransits=okSynastrie=okArc=okRS=okMix=0;
    if (latNatal) [latitude,longitude,utc]=[latNatal,longNatal,utcNatal];
    
    //mise à 0 des positions externes
    for (i=0;i<=maxPl;i++){
        posPlanete[1][i]=0;
        retro[1][i]=0;
    }
    var date,heure,titre;
    if (!dateNatal || checkMaintenant.checked==true) date=choixDate.value,heure=choixHeure.value,titre="?";
    else date=dateNatal,heure=heureNatal,titre=titreNatal;
    requetes(date,heure,titre);
}

//couleur des secteurs signes
function couleurSecteurs(x){
    var x0,y0,x1,y1,f; 
    luminosite.value=lux;
    if (lux==0)lux=0.15;
    //suppression anciens secteurs signe
    svgEfface(".arc_signe");
    
    for (var i=0; i<=11;i++){
        x0=centre[0]+Math.cos(i*(Math.PI/6)+AS)*rayon;
        y0=centre[1]+Math.sin(i*(Math.PI/6)+AS)*rayon;
        x1=centre[0]+Math.cos((i+1)*Math.PI/6+AS)*rayon;
        y1=centre[1]+Math.sin((i+1)*Math.PI/6+AS)*rayon;
        f="hsla(" + 60*(i % x) + ",70%, 50%," + lux + ")";
        svgArc ("arc_signe",x0,y0,rayon,x1,y1,centre[0],centre[1],f,"transparent");
    }    
}

//1 bouton

//bascule thème/tableau
boutonBascule.onclick=function(){
   // canvasTarget.hidden=!canvasTarget.hidden;
  //  canvasInverse();
    if (tableau.hidden==false){
        canvasCache(false);
        tableau.hidden=true;
      //  okProgresse=0; //sinon affichage erronné des planètes rétrogrades, si "transits progressés" coché
    }else{
        canvasCache(true);
        tableau.hidden=false;
    }  
} 

//************************************** fin des choix ***************************************************

function canvasInverse(){
    var x=[tableau,margeDiv,canvasPhases];
    for (var i=0;i<x.length;i++){
        x[i].hidden=!x[i].hidden;
    }
}

//*************************** marge droite *******************************
//String.fromCharCode(j) : AS=K=75(code)=13(planete), MC=L=76=14, NN=M=77=10, Lilith=N=78=11, Mondial=79=12

function margeClic(e){
    if (okUranien) return;
    //fixePhases=0 ou 1
    fixePhases+=1;
    fixePhases=fixePhases%2;
    if (!fixePhases && !okTransits) reaffiche();//souris.js
    else margeSurvol(e);
}

function margeNoir() {
    for (var i of margePl){
        i.style.color="black";
    }
}

function margeSurvolOut(){
    if (!fixePhases && !okUranien){
        if(canvasPhases) canvasPhases.hidden=true;
        margeNoir();
        svg.style.display="";
    }
}

function margeSurvol(e){//e.target représente l'index de l'icône survolée
    if (okUranien || e.target.offsetTop<=5 || e.target.innerText=="") return;//5=au-dessus des icônes, ""=pas d'icône
    var x=0;
    margeNoir();
    //recherche planète (refPhase)
    for (var j=0;j<=maxPl;j++){
        if (j==12) continue;//NS
        if (planetesFonts[j]==e.target.innerText){
            x=refPhase=j;
            if (x>12 && x<15)x=j-1;
            margePl[x].style.color="red";//planete selectionnee en rouge
            break;
        }
        x+=1;
    }
    //mondial
    if (diversFonts[1]==e.target.innerText){
        x=14;
        refPhase=12;
        margePl[x].style.color="red";
    }
    //affichage
    if (refPhase!=undefined) affiPhases(refPhase);
}

function margeSet(){
    var x=0,y; //définir à 0 sinon bug
    efface(margeDiv);
    var margeListe= document.createElement('div');
    
    //icônes pour commuter interne/externe et pour remettre à zéro le bandeau en uranien
    if (okUranien){
        y=['arrow.png',swapIntExt,labelsUranien[17],'reset.png',effaceBandeau,labelsUranien[15]];
        for (var i=0;i<=5;i+=3){
            addIcon(margeListe,y[i],y[i+1],y[i+2]);
        }
    }
    
    margeListe.style.font= okUranien==0 ? 1.1+"vw Zodiac" : 0.8+"vw Zodiac";
    for (var i=0;i<=maxPl+1;i++){
        if (okUranien==1 && i==11) continue; //NS et mondial non affichés
        if (i==12) continue; //NS non affiché
        margePl[x]=document.createElement('p');
        margePl[x].setAttribute('class', 'main');
        if (i<=14){
            margePl[x].textContent = planetesFonts[i];
        }
        else if (i==15 && okUranien==0) { //mondial
            margePl[x].textContent = diversFonts[1];
        }
        else if (i>=15 && okUranien==1) { //uranien
            margePl[x].style.font=urFont(0.8,i)+"vw Zodiac";
            margePl[x].textContent = planetesFonts[i];
        }
        margeListe.appendChild(margePl[x]);
        //listeners
        if (okUranien==1){
            if (listeBlanche[i]==1){
                if (i>=13)y=i-2;//AS, MC et uraniennes
                else y=i;
                margePl[y].style.color="red";
            }
        }
        x+=1;
    }
    
    margeDiv.appendChild(margeListe);
    if (!okUranien){
        //icone info
        let c = new Image();
        c.src="../images/bulle.png";
        margeListe.append(c);
        survol(c,labelsDroite[45],-200,10);
        //message "mondial"
        survol(margePl[14],labelsDroite[27],-50,10);
        //listeners
        margeDiv.addEventListener("mouseover",margeSurvol,false);
        margeDiv.addEventListener("mouseout",margeSurvolOut,false);
        margeDiv.addEventListener("click",margeClic,false);
    }else{
        margeDiv.addEventListener("click",margeFormulesClic,false);
    }
}

function addIcon(m,i,l,d){
    var p=document.createElement('p');
    var im=document.createElement('input');
    im.setAttribute('type', 'image');
    im.setAttribute('src', '../images/'+i);
    im.setAttribute('alt', d);
    im.setAttribute('class', 'main');
    im.addEventListener("click",l,false);
    im.onmouseover=function(e){displayDivInfo(d,e.pageX-7*d.length,e.pageY+10)};
    im.onmouseout=function(){displayDivInfo()};
    p.appendChild(im);
    m.appendChild(p);
}

//pour formules uranien
function margeFormulesClic(e,z,lb){//z=0: remise à zéro,z>0: nbre de planetes à mettre en rouge
    if (okUranien==0) return;
    var x;
    for (var i=0;i<=maxPl;i++){
        //raz bandeau
        if (z==0){
            listeBlanche[i]=0; //listeBlanche de 0 à 23
            if (i<=22) margePl[i].style.color=""; // margePl de 0 à 22 (pas de NS)
        }
        //allumage sélectif
        else if (z){
            for (var j=0;j<z;j++){
                listeBlanche[lb[j]]=1;
                margePl[lb[j]].style.color="red";
            }
        }
        //clic sur bandeau
        else if (e.target.innerText==planetesFonts[i]){
            if (i>=13)x=i-2;//AS, MC et uraniennes
            else x=i;
            if (margePl[x].style.color>""){
                margePl[x].style.color="";
                listeBlanche[i]=0;
            }else{
                margePl[x].style.color="red";
                listeBlanche[i]=1;
            }
            break;
        }
    }
    if (!z) uranienAspects(planeteDefaut,typeRef);
}

function effaceBandeau(){
    margeFormulesClic(0,0);
}

function swapIntExt(){
    if (okTransits==0) return;
    uranienAspects(planeteDefaut,typeRef^1);//NOR
}

//test liste blanche pour compléter bandeau si manque de sélections pour paire ou formule
function lbCheck(){
    var l=0,lb=[],car=0;
    for (var x=0;x<listeBlanche.length;x++){
        if (listeBlanche[x]==1)l+=1;
        else{
            lb[car]=x;//emplacement libre
            car+=1;
        }
    }
    if (MP+1-l>0) margeFormulesClic(0,MP+1-l,lb);
}

//******************************* fin marge***************************
function cacheHorloge(x){
    document.getElementById("divHorloge").hidden=x; //true;
}

//fin de chargement de la page
window.onload=function() {
    traductions();
    choixEcoute();
    margeSet();
    demarrage=1;
    horizon.value="0";
    //recherche du navigateur et paramètres spécifiques
    navigateur="";
    var x=["firefox","chrome","android"];
    var ua = navigator.userAgent.toLowerCase();
    for (var i=0; i<x.length;i++){
        if (ua.indexOf(x[i]) > -1){navigateur=x[i]} //&& ua.indexOf("mobile");
    }
    android=0;
    if (navigateur=="android"){
        taille-=4;
        android=1;
        cacheHorloge(true);
    }
    fontTableau='0.8vw serif';
    if (navigateur=="chrome"){
        fontTableau='0.85vw serif'; //tableau résumé plus lisible
        titreCanvas.style.fontSize="0.9vw";
    }
    optTransits.label="plus...(2 max)";
   // if (navigateur=="firefox" || navigateur=="android") selectTransits.size=6;
    //version zodiaque
    version.textContent=chrome.runtime.getManifest().version;
    
    //reset positions planètes
    resetDoms();
    for (var i=0;i<=2;i++){
        posPlanete[i]=[];
        retro[i]=[];
        lLive[i]=[];
        hLive[i]=[];
    }
    //création table des notes
    for (var i=0;i<=2;i++){//instrument
        tableNotes[i]=[];
        for (var m=0;m<=1;m++){//dièse (rétrograde)
            tableNotes[i][m]=[];
            for (var j=0;j<=6;j++){//note
                tableNotes[i][m][j]=[];
                for (var k=3;k<=4;k++){//octave
                    tableNotes[i][m][j][k]=[];
                    for (var l=2;l<=3;l+=0.5){//durée
                        tableNotes[i][m][j][k][l]=[];
                    }
                }
            }
        }
    }
    creeTableaux();
    checkMaintenant.checked=false;//sinon pas d'affichage au démarrage
        //temporisation au démarrage sinon symboles planetes non chargés
    //utiliser setTimeout uniquement sous cette forme ou la suivante sinon message erreur relatif à la sécurité
    let timer = setTimeout(() => {
        console.log('Zodiaque démarré');
        checkMaintenant.click();
    }, 200);
    //autre syntaxe pour utiliser un timer
    /*let timer = setTimeout(function () {
    console.log('Bonjour :)');
    }, 1000);*/
}  

function resetDoms(){
    for (var i=0;i<=2;i++){
        posDom[i]=[];
    }
}

//ex tempo de x msec avec promise (non utilisé)
function temporise(x) {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve();
    }, x);
  });
}

window.onresize =function() {
    if (tableau.hidden==true) dessins();
}

//lien pour ouvrir la page des options
options.onclick=function(){
    var openingpage=browser.runtime.openOptionsPage();
    openingpage.then(console.log("page options ouverte"),onError);
} 

//atention, la version de zodiaque est liée à ce bouton (label for=... dans le html)
boutonTest.onclick=function(){
}

function logTabs(tabs) {
  for (let tab of tabs) {
    // tab.url requires the `tabs` permission
    console.log(tab.url);
  }
  browser.tabs.reload();//recharge la page active, celle des options pas zodiaque !
}

//pas utilisé
/*fileElem.addEventListener("change",() => {
    gereFichier();
},onError); */
